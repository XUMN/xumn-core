import {store} from "./CoreStore";
import {BindingParser, TBinding} from "./Util/BindingParser";
import {CoreConverter} from "./CoreConverter";
import {CoreConstruct} from "./CoreConstruct";


export class BindingSetting{
    static bindingStore(binding:TBinding){
        if(binding.bindingSource){
            binding.bindingValue = new CoreBinding().sourceValue(binding.bindingSource).method.obtain( binding );
        }
    }

}

export class CoreBinding {
    // Список доступных сторов со обладающих стандартными методами
    source = {
        "Content": store.content,
        "Session": store.session,
        "Failure": store.failure,
        "Process": store.process,
        "Filters": store.filters,
        "Routing": store.routing,
        "Dictionary": store.dictionary,
    } as { [key:string]:any };

    sourceValue = (bindingSource:string|undefined) =>{
        if(bindingSource){
            return this.source[bindingSource] ? this.source[bindingSource] : undefined;
        } else {
            console.log("Incorrect remove binding:",bindingSource)
        }
    };

    obtainValue = (value:string) => {
        const binding = BindingParser.bindingParsingValue(value);
        // Проверка на наличие добавчоного пуцти
        // Проверка запрос данных из стора или текущее значение
        BindingSetting.bindingStore(binding);
        // Проверка на наличие конструирования
        new CoreConstruct().launch(binding);
        // Проверка на наличие конвертации
        new CoreConverter().launch(binding);
        return binding;
    };

    refersValue = (expression:string)=>{
        const binding = BindingParser.bindingParsingValue(expression);
        return this.sourceValue(binding.bindingSource).method.refers( binding );
    };

    outputValue = (expression:string) => {
        const dict = {
            "undefined": () => "",
            "object": () => "",
            "boolean": () => "",
            "function": () => "",
            "symbol": () => "",
            "number": ( data:number ) => String(data),
            "string": ( data:string ) => data,
        }  as { [key:string]:Function };

        const binding = this.obtainValue(expression);
        binding.bindingValue = dict[typeof binding.bindingValue](binding.bindingValue);
        return binding;
    };

    recordBinding = (expression:string, value:any) => {
        const binding = BindingParser.bindingParsingExpression(expression);
        binding.bindingValue = value;
        this.sourceValue(binding.bindingSource).method.record( binding );


    };
    recordValue = (to:string, value:any) => {
        const binding = BindingParser.bindingParsingExpression(to);
        // Проверка значения на возможное выражение или объект
        if(typeof value === "string"){
            // Проверка выражение строка или биндинг
            binding.bindingValue = this.obtainValue(value).bindingValue;
        } else {
            binding.bindingValue = value;
        }
        // Проверка на наличие конструирования
        new CoreConstruct().launch(binding);
        // Проверка на наличие конвертации
        new CoreConverter().launch(binding);

        this.sourceValue(binding.bindingSource).method.record( binding );
    };

    removeValue = (expressionInto:string, expressionData?:string) => {
        const binding = BindingParser.bindingParsingExpression(expressionInto);
        // Проверка на удаление по ключу, или объекта в ключе
        if(expressionData){
            const data = this.obtainValue(expressionData);
            binding.bindingValue = data.bindingValue;
        }
        this.sourceValue(binding.bindingSource).method.remove( binding );
    };

    recordArrayValue = (expression:string, key:any, keyValue:any, value:any) => {
        const binding = BindingParser.bindingParsingExpression(expression);

        let testValue = this.obtainValue(keyValue).bindingValue;
        let bindingValue = this.obtainValue(expression).bindingValue;
        if (!Array.isArray(bindingValue)){
            this.recordValue(expression, []);
        }

        let objectValue = null;
        // Проверка значения на возможное выражение или объект
        if(typeof value === "string"){
            // Проверка выражение строка или биндинг
            objectValue = this.obtainValue(value).bindingValue;
        } else {
            objectValue = value;
        }

        let index = null;
        for(let idx in bindingValue){
            const val = bindingValue[idx];
            if (val === undefined){
                continue;
            }
            if (val.hasOwnProperty(key) && val[key] === testValue){
                index = idx;
            }
        }

        if (index !== null){
            delete bindingValue[index];
            bindingValue[index] = objectValue;
        } else {
            if (bindingValue === undefined){
                bindingValue = [];
            }
            bindingValue.push(objectValue);
        }
        binding.bindingValue = JSON.parse(JSON.stringify(bindingValue));

        // Проверка на наличие конструирования
        new CoreConstruct().launch(binding);

        // Проверка на наличие конвертации
        new CoreConverter().launch(binding);

        this.sourceValue(binding.bindingSource).method.record( binding );
    };

    removeArrayValue = (expression:string, key:string, keyValue:any) => {
        const binding = BindingParser.bindingParsingExpression(expression);

        let testValue = this.obtainValue(keyValue).bindingValue;
        let bindingValue = this.obtainValue(expression).bindingValue;
        if (!Array.isArray(bindingValue)){
            this.recordValue(expression, []);
        }

        let resultArray = [];
        for(const idx in bindingValue){
            const val = bindingValue[idx];
            if (val === undefined){
                continue;
            }
            if (!val.hasOwnProperty(key) || val[key] !== testValue){
                resultArray.push(val);
            }
        }
        binding.bindingValue = JSON.parse(JSON.stringify(resultArray));

        // Проверка на наличие конструирования
        new CoreConstruct().launch(binding);
        // Проверка на наличие конвертации
        new CoreConverter().launch(binding);
        this.sourceValue(binding.bindingSource).method.record( binding );
    };
}
