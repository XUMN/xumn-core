import {styleDictionary,triggerDefinition} from "../UI/StyleDefinition";

type TStyleFactory = {
    template:string;
}

export class ResourceFactory {
    protected style:string;
    protected collection:{[key:string]:any};
    constructor() {
        this.style = "";
        this.collection = {};
    }

    static getName = (name:string) => {
        name = name.replace(/^./, (match) => match.toLowerCase());
        name = name.replace(/[A-Z]/g, match => `-${match.toLowerCase()}`);
        return name;
    }

    static getValue = (list:HTMLCollection,type:string) =>{
        let result = "";
        for(const item of list){
            const property = item.getAttribute("Property");
            if(!property){continue}
            if(!styleDictionary[type][property]){continue}
            const value = item.getAttribute("Value");
            if(!value){continue}
            const key = ResourceFactory.getName(property);
            result += key + ":" + value + ";";
        }
        return result
    }

    static launch = (props:TStyleFactory) => {
        const result = {style: ""}
        const root = new DOMParser().parseFromString(props.template, "text/xml").documentElement;
        const item = root.querySelectorAll("Style");
        for(let i = 0; i < item.length; i++) {
            const type = item[i].getAttribute("TargetType");
            if(!type){continue}
            if(!styleDictionary[type]){continue}
            const targetName = item[i].getAttribute("TargetName");
            if(!targetName){continue}
            const {children} = item[i];
            if(children.length === 0){continue}
            result.style += "." + targetName;
            const trigger = item[i].getAttribute("Trigger");
            if(trigger){
                if(triggerDefinition[trigger]){
                    result.style += triggerDefinition[trigger];
                }
            }
            result.style = result.style + "{" + ResourceFactory.getValue(children,type) + "}";
        }

        return result.style;
    }
}