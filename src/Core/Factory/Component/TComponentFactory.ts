export type TComponentFactory = {
    fraction?: string;
    template: string;
    mixinProps?: any;
}