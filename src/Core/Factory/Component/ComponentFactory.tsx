import {createElement } from "react";
import {CoreUI} from "../../CoreUI";
import {TComponentFactory} from "./TComponentFactory";

const setLowercase = (value:string) => value.replace(/^./,  (match) => match.toLowerCase());

export function getAttributes(element:Element,) {
    const { attributes } = element;
    const result: { [name: string]: string | number | object[]} = {};
    for(const item of attributes){
        result[setLowercase(item.name)] = item.value;
    }
    return result;
}

export function getChildren (element:Element, callback:Function){
    const { children } = element;
    const result:Array<object> = [];
    for ( const [key, value] of Object.entries(children) ) {
        result.push(callback(value, key));
    }
    return result;
}

export function getProps (element: Element, key: number, callback:Function){
    const attributes = getAttributes(element);
    const template = new XMLSerializer().serializeToString(element);
    const children = getChildren(element,callback);
    return Object.assign( { key: key, template:template, children }, attributes);
}

export const ComponentFactory = (props:TComponentFactory) => {
    const {template,mixinProps} = props;
    const elementConverter = (element: Element, key: number) => {
        const component = CoreUI[element.tagName]?.default;
        if (component) {
            const props = getProps(element, key, elementConverter);
            if(mixinProps){
                props.mixinProps = mixinProps;
            }
            return createElement(component, {...props});
        } else {
            return null;
        }
    };
    const element = new DOMParser().parseFromString(template, "text/xml").documentElement;
    return elementConverter(element, 0);
};