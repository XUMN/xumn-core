import {parseDocumentElement} from "../Util/NodeXML";
import {CoreBinding} from "../CoreBinding";
import {AppConfigs} from "../../App/AppConfigs";
import {conditionFacade} from "../CoreCondition";

class RequestFactory {
    elementChildren = (o:any,rootElement:Element|null) => {
        if(rootElement){
            const key = rootElement.getAttribute("TargetName");
            if(key){
                const value = rootElement.getAttribute("Value");
                if(value){
                    o[key] = new CoreBinding().obtainValue(value).bindingValue;
                } else {
                    o[key] = {};
                    let n = o[key];
                    for(let i = 0; i < rootElement.children.length; i++){
                        const element = rootElement.children[i];
                        if( element ){
                            this.elementChildren(n,element)
                        }
                    }
                }
            } else {
                console.log("Incorrect request targetName ")
            }
        }
        return o
    };

    select = (element:Element|null) => {
        let o = {};
        if(element){
            const value = element.getAttribute("Value");
            if(value){
                o = new CoreBinding().obtainValue(value).bindingValue
            } else {
                for(let i = 0; i < element.children.length; i++){
                    this.elementChildren(o,element.children[i]);
                }
            }
        }
        return o
    };

    description = (template:Element | null) => {
        if(template){
            return {
                method: template.getAttribute("method"),
                headers: this.select( template.querySelector("Headers")),
                body: template.querySelector("Body")?JSON.stringify(this.select( template.querySelector("Body"))) : null
            }
        } else {
            return null;
        }
    };
    URI = (template:Element | null) => {
        return template ? AppConfigs.API.origin + template.getAttribute("API") : "";
    };
    binding = (template:Element | null) =>{
        const binding = template?.getAttribute("binding");
        return binding ? binding : "";
    };

    launch = (template:string) => {
        const request = parseDocumentElement(template).querySelector(":root > Request");
        propertyExtractor(request,":root > Request > Property");
        return {
            URI: () => this.URI(request),
            binding:() => this.binding(request),
            description:() => this.description( request )
        };
    }
}

const propertyExtractor = (element:Element | null, selector:string) => {
    if(element){
        const list = element.querySelectorAll(selector);
        for(const property of list){
            const propertyName = property?.getAttribute("Name");
            if(!propertyName) continue;
            const propertyValue = property?.getAttribute("Value");
            if(!propertyValue) continue;
            const condition: {[name:string]:string} = {}
            const conditionType = property?.getAttribute("ConditionType");
            const conditionValue = property?.getAttribute("ConditionValue");
            if(conditionType){
                condition.conditionType = conditionType;
            }
            if(conditionValue){
                condition.conditionValue = conditionValue;
            }
            if(conditionFacade(condition)){
                element.setAttribute(propertyName,propertyValue);
            }
        }
    }
}
export const requestParser = new RequestFactory();