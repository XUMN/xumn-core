import {CoreBinding} from "../CoreBinding";
import {recordValue} from "./ValueMethod";

export const updateLocation = (location:any) => {
    new CoreBinding().sourceValue("Routing").method.remove({bindingSource:"Routing",bindingPath: "location"});
    const search = new URLSearchParams(location.search);
    const binding = {
        bindingPath: "location",
        bindingSource: "Routing",
        bindingValue: {
            href:location.pathname + location.search,
            pathname:location.pathname,
            search:location.pathname.search,
            queryParam: {} as {[key:string]:string}
        }
    }

    for(const key of search.keys()) {
        const param = {bindingPath:key, bindingValue:search.get(key)}
        recordValue(binding.bindingValue.queryParam,param)

    }

    new CoreBinding().sourceValue(binding.bindingSource).method.record( binding );

}