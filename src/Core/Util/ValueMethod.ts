export const obtainValue = (o: any, path:string,) => {
    const a = path.split('.');
    if(!Array.isArray(a)){
        return undefined;
    }
    for (let i = 0; i < a.length; ++i) {
        const key = a[i];
        if (key in o) {
            o = o[key];
        } else {
            return undefined;
        }
    }
    return o;
};

type TRecordValue = {
    bindingPath:string;
    bindingValue:any;
    recordProperty?:string;
}

export const recordValue = (data: any, param:TRecordValue ) => {

    const prop:Array<string> = param.bindingPath.split(".");



    prop?.map( (key, i) => {
        if(++i === prop.length) {
            if (data === undefined){
                data = [];
            }
            data[key] = param.bindingValue;
        }
        data[key] = data.hasOwnProperty(key) ? data[key] : {};
        data = data[key];
        return null;
    });

};


export const removeValue = (o: any, path:string, value?:any ) =>{
    const prop:Array<string> = path.split(".");


    prop?.map( (e, i) => {
        if(++i === prop.length){
            if(Array.isArray(o[e])){
                if(value){
                    const index = o[e].indexOf(value, 0);
                    if (index >= 0) {
                        o[e].splice(index, 1);
                    }
                } else {
                    delete o[e]
                }
            } else {
                delete o[e]
            }
        } else {
            if(o.hasOwnProperty(e)){
                o = o[e];
            }
        }
    });

};

export const refersValue = ( bindingValue: any, bindingPath:string ) =>{
    const result:{[key:string]:any} = {};
    const value = obtainValue(bindingValue, bindingPath);
    const getReference = (o:any, path?:string) => {
        for(const key in o){
            if(o.hasOwnProperty(key)){
                if(typeof o[key] === "object"){
                    path ? getReference(o[key], path + "." + key): getReference(o[key], key);

                } else {
                    if(path){
                        if(Array.isArray(o)){
                            result[path] = [];
                            o.map(e => result[path].push( e ));
                        } else {
                            result[path + "." + key] = o[key];
                        }
                    } else {
                        result[key] = o[key];
                    }
                }
            }
        }
    };
    getReference(value,bindingPath);
    return result;
};