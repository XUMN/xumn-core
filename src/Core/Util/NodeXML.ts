import {CoreBinding} from "../CoreBinding";

export const parseXML = ( template:string ) => new DOMParser().parseFromString(template, "text/xml");

export const parseDocumentElement = ( template:string ) => parseXML(template).documentElement;

export const cloneElement = ( element:Element  ) => new XMLSerializer().serializeToString(element.cloneNode(true));

export const cloneDocumentElement = ( element:Element ) => parseXML(new XMLSerializer().serializeToString(element)).documentElement;

export const fragment = ( pattern:string ) => {
    const template = parseXML(pattern);
    template.documentElement.removeAttribute("type");
    return template.documentElement;
};

export const replaceRootElement= ( template:string ) => {
    const fragment = new DOMParser().parseFromString("<Template><Slot/></Template>", "text/xml");
    const includes = new DOMParser().parseFromString(template, "text/xml");
    fragment.querySelector("Slot")?.replaceWith(...includes.documentElement.children);
    return fragment;
};

const setLowercase = (value:string) => value.replace(/^./,  (match) => match.toLowerCase());

export const getElementAttr = (element:Element, mode?:"lowercase") => {
    let prop: { [name: string]: any } = {};
    const { attributes } = element;
    for(const item of attributes){
        prop[item.name] = item.value;
    }
    if(mode){
        const result:{[name:string]:any} = {}
        Object.keys(prop).map(e=> result[setLowercase(e)] = prop[e]);
        prop = result;
    }
    return prop;
};


export const getChildrenAttribute = (template:string, selector:string, mode?:"lowercase" )=> {
    const prop:any[] = [];
    const XML = parseXML(template);
    const collection = XML.querySelectorAll( selector );
    for(const item of Array.from(collection)){
        prop.push(getElementAttr(item,mode));
    }
    return prop;
};

const allSetter = (root:Element, elementName:string, data:any  ) => {
    const props = getElementAttr(root);
    if( root.tagName === elementName){
        if(props.targetName){
            if(props.value){
                data[props.targetName] = new CoreBinding().obtainValue(props.value).bindingValue;
            } else {
                data[props.targetName] = {};
                for(let i = 0; i < root.children.length; i++){
                    const element = root.children[i];
                    if( element ){
                        allSetter(root.children[i], elementName, data[props.targetName]);
                    }
                }
            }
        }
    }
};

export const collectAllChildrenAttr = (template: string, selector: string) => {
    const result: any = {};
    const element = parseXML(template).documentElement.children;
    for(let i = 0; i < element.length; i++){
        allSetter(element[i], "Setter", result);
    }
    return result;
};


type Ts = {
    bindingSource: string,
    bindingPath: string,
    bindingIndex: number,
    element: Element,
    attribute: string
}

export const recordPartBindingAttribute = (props: Ts) => {
    //Получаем все вложенные циклы
    let availableLoops = [];
    const loops = props.element.querySelectorAll(`Loop`);
    for(let i = 0; i < loops.length; i++){
        if (loops[i]?.parentElement?.closest("Loop") === null){
            availableLoops.push(loops[i]);
        }
    }

    const items = props.element.querySelectorAll(`[${props.attribute}]`);
    for(let i = 0; i < items.length; i++){
        let closestLoop = items[i].closest("Loop");
        if (closestLoop === null || (closestLoop === items[i] && availableLoops.includes(closestLoop))) {
            let value = items[i].getAttribute(props.attribute);
            if(value?.includes( "Binding Value=Index")){
                items[i].setAttribute("Value",value.replace("Index",String(props.bindingIndex)))
            }
            if (value?.includes("Binding Path=")) {
                const currentPath = `Path=${props.bindingPath}.${props.bindingIndex}.`;
                value = value.replace("Binding ", "Binding Source=" + props.bindingSource + ",");
                value = value.replace("Path=", currentPath);
                items[i].setAttribute(props.attribute, value);
            }
            if (value?.includes("Binding Index")) {
                const currentPath = `Path=${props.bindingPath}.${props.bindingIndex}`;
                value = value.replace("Binding ", "Binding Source=" + props.bindingSource + ",");
                value = value.replace("Index", currentPath);
                items[i].setAttribute(props.attribute, value);
            }
        }
    }
}