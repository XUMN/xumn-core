export type TBinding = {
    [key:string]:string|undefined
}

export const BindingParser = {
    // Очистка выражения от вспомогательных символов
    normalizeExpression: (expression:string) => expression
        .replace("Binding", "")
        .replace("{", "")
        .replace("}", "")
        .replace(/\s+/g,""),
    // Извлечение свойства из выражения биндинга
    bindingProperty:(expression:string, propertyName:string)=>{
        let property: string | undefined = undefined;
        for(const e of expression.split(",")){
            if(e.includes(propertyName)) {
                const value = e.split("=").pop();
                property = value ? value : undefined;
            }
        }
        return property;
    },
    // Результат биндинга, парсинг выражения
    bindingParsingExpression: (expression:string) => {
        const result:TBinding = {};
        expression = BindingParser.normalizeExpression(expression);
        result.bindingValue = BindingParser.bindingProperty(expression,"Value="); // Итоговое значение биндинга
        result.bindingSource = BindingParser.bindingProperty(expression,"Source="); // Стор из которого извлекается значение
        result.bindingPath = BindingParser.bindingProperty(expression,"Path="); // Путь в сторе из готорого извлекается значение
        result.bindingConverter = BindingParser.bindingProperty(expression,"Converter="); // Тип конвертера данных
        result.converterProperty = BindingParser.bindingProperty(expression,"ConverterProperty="); // Свойство конвертера данных
        result.bindingConstruct = BindingParser.bindingProperty(expression,"Construct="); // Тип конструктора данных
        result.constructProperty = BindingParser.bindingProperty(expression,"ConstructProperty="); // Свойство конструктора данных
        result.recordProperty = BindingParser.bindingProperty(expression,"RecordProperty="); // Флаг проверки записи в массив
        //Удаление несуществующих свойств результата биндинга
        for ( const [key, value] of Object.entries(result) ) {
            if(!value){
                delete result[key]
            }
        }
        return result;
    },

    // Результат единичного выражения биндинга, возвращает строковое значение или реультат парсинга выражения биндинга
    bindingParsingValue: (expression:string) => {
        let result:{[key:string]:any} = {};
        expression.includes("Binding") ? result = BindingParser.bindingParsingExpression(expression) : result.bindingValue = expression;
        return result;
    },
    // Результат мульти выражения биндинга, возвращает коллекцию выражений биндинга
    bindingCollection: (expression:string) => {
        const result: Array<string> = [];
        for(const item of expression.split(/[{}]/)){
            if(item){
                result.push("{" + item + "}");
            }
        }
        return result;
    }
};