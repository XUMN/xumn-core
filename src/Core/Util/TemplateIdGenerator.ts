import {CoreBinding} from "../CoreBinding";

export class Source {
    static setRoute(pathname:string,pattern:string|undefined,source:string){
        if(pattern){
            const slug: Array<string> = [];
            const name: Array<string> = [];
            const dict: {[name:string]:string} = {}
            pathname.split("/")?.map(e => e ? slug.push(e) : null);
            pattern.split("/")?.map(e => e ? name.push(e) : null);
            name.map( (key,i) => key?.includes(":") ? dict[key] = slug[i] : null);
            Object.keys(dict).map( (key) => source = source.replace(key,dict[key]));
        }
        return source;
    }
    static setValue(template:string,source:string){
        const root = new DOMParser().parseFromString(template, "text/xml");
        for(const element of root.querySelectorAll(":root > Directory")){
            const to = element.getAttribute("DirectoryName");
            if(!to){continue}
            const value = element.getAttribute("Value");
            if(!value){continue}
            source = source.replace(to,new CoreBinding().obtainValue(value).bindingValue);
        }
        return source;
    }
    static getSource(template:string,routePattern:string | undefined, pathname:string, source:string){
        source = Source.setRoute(pathname,routePattern,source)
        source = Source.setValue(template,source)
        return source;
    }
}