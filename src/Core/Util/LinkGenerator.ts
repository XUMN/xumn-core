import {getChildrenAttribute} from "./NodeXML";
import {CoreBinding} from "../CoreBinding";
import {BindingParser} from "./BindingParser";

export const queryGenerator = (template:string,URI:URL) => {
    const root = new DOMParser().parseFromString(template, "text/xml");
    const value = root.querySelector(":root > Query")?.getAttribute("Value");
    if(value){
        const bindingPath = BindingParser.bindingParsingValue(value).bindingPath;
        const getParamName = (key:string) => key.replace(bindingPath + ".","");
        const setParam = (param:string,value:string) => URI.searchParams.set(param,value);
        const refers = new CoreBinding().refersValue(value);
        if(refers){
            for ( const [key, value] of Object.entries(refers) ) {
                const paramName = getParamName(key);
                setParam(paramName,String(value));
            }
        }
    }
}

export const linkGenerator = (template:string,to:string) => {
    const pathname = new CoreBinding().outputValue(to).bindingValue;
    const URI = new URL(window.location.origin + pathname);
    queryGenerator(template,URI);
    const setter = getChildrenAttribute(template,':root > Query > Param',"lowercase");
    for(const element of setter){
        const value = new CoreBinding().outputValue(element.value).bindingValue;
        if(value){
            URI.searchParams.set(element.to,value);
        }
    }
    return URI.pathname + URI.search;
};