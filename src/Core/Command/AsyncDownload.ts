import {AppConfigs} from "../../App/AppConfigs";
import {CoreBinding} from "../CoreBinding";


export const asyncDownload = async (props:any) => {
    const fileId = new CoreBinding().obtainValue(props.fileId).bindingValue;
    if(fileId) {
        window.open( AppConfigs.API.origin + "/download/" + fileId, '_blank');
    }
};