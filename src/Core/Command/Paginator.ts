import {CoreBinding} from "../CoreBinding";

export class Paginator {
    static getURI = (value:string) => new URL(window.location.origin + new CoreBinding().obtainValue(value).bindingValue);
    static getPage (props:any) {
        const result = {
            URI: Paginator.getURI(props.link),
            current: new CoreBinding().obtainValue(props.page).bindingValue,
            prev: new CoreBinding().obtainValue(props.page).bindingValue,
            next: new CoreBinding().obtainValue(props.page).bindingValue,
            rows: new CoreBinding().obtainValue(props.rows).bindingValue,
            count: new CoreBinding().obtainValue(props.count).bindingValue,
            max: 0
        }
        result.max = Math.floor(result.count / result.rows )
        return result;
    }
    static setBack(props:any){
        const page = Paginator.getPage(props);
        if(page.current > 2){
            const back = 0;
            page.URI.searchParams.set(props.pageParamName,String( back ));
            let visibleValue = JSON.parse(JSON.stringify(back));
            const value = {
                "status":"active",
                "number": back,
                "visibleValue": ++visibleValue,
                "link": page.URI.pathname + page.URI.search
            }
            new CoreBinding().recordValue(props.back,value);
        } else {
            new CoreBinding().removeValue(props.back);
        }
    }
    static setPrev(props:any){
        const page = Paginator.getPage(props);
        if(page.current > 0){
            let prev = new CoreBinding().obtainValue(props.page).bindingValue;
            --prev;
            prev = prev >= 0 ? prev : page.current;
            page.URI.searchParams.set(props.pageParamName,String( prev ));
            let visibleValue = JSON.parse(JSON.stringify(prev));
            const value = {
                "status":"active",
                "number": prev,
                "visibleValue": ++visibleValue,
                "link": page.URI.pathname + page.URI.search
            }
            new CoreBinding().recordValue(props.prev,value);
        } else {
            new CoreBinding().removeValue(props.prev);
        }
    }
    static setNext(props:any) {
        const page = Paginator.getPage(props);
        if(page.current !== page.max){
            let next = new CoreBinding().obtainValue(props.page).bindingValue;
            ++next;
            next = next >= Math.ceil(page.count / page.rows) ? page.current: next;
            page.URI.searchParams.set(props.pageParamName,String( next ));
            let visibleValue = JSON.parse(JSON.stringify(next));
            const value = {
                "status":"active",
                "number": next,
                "visibleValue": ++visibleValue,
                "link": page.URI.pathname + page.URI.search
            }
            new CoreBinding().recordValue(props.next,value);
        } else {
            new CoreBinding().removeValue(props.next);
        }
    }
    static setItem(props:any,i:number,listLink:Array<any>) {
        const URI = Paginator.getURI(props.link);
        URI.searchParams.set(props.pageParamName,String( i ));
        let visibleValue = JSON.parse(JSON.stringify(i));
        const value = {
            "number": i,
            "visibleValue": ++visibleValue,
            "link": URI.pathname + URI.search
        }
        listLink.push(value);
    }
    static setList(props:any) {
        const listLink:Array<any> = []
        const page = Paginator.getPage(props);
        if(page.current <= 2 ){
            for(let i = 0; i <=4; i++) {
                if(i <= page.max ) {
                    Paginator.setItem(props,i,listLink)
                }
            }
        }
        if( page.current > 2 && page.current < page.max - 2){
            for(let i = page.current - 2; i <= page.current + 2; i++) {
                if(i <= page.max ) {
                    Paginator.setItem(props,i,listLink)
                }
            }
        }
        if( page.current > 2 && page.current >= page.max - 2) {
            for(let i = page.max - 4; i <= page.max; i++) {
                if(i <= page.max ) {
                    Paginator.setItem(props,i,listLink)
                }
            }
        }
        new CoreBinding().recordValue(props.listLink,listLink);
    }
    static update(props:any){
        const getURI = () => new URL(window.location.origin + new CoreBinding().obtainValue(props.link).bindingValue);
        Paginator.setBack(props);
        Paginator.setPrev(props);
        Paginator.setNext(props);
        Paginator.setList(props);
        const rowsList =  new CoreBinding().obtainValue(props.rowsList).bindingValue;
        if(Array.isArray(rowsList)) {
            for(const item of rowsList){
                const URI = getURI();
                URI.searchParams.set(props.pageParamName,String( 0) );
                URI.searchParams.set(props.rowsParamName,String( item.number) );
                item["link"] = URI.pathname + URI.search
            }
        }
    }
}