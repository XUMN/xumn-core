import {action, makeObservable, observable} from "mobx";
import {store} from "../../CoreStore";
import {obtainCoreRequest} from "../../CoreRequest";
import {AppConfigs} from "../../../App/AppConfigs";
import {TemplateRequest} from "../../Request/Template/TemplateRequest";
import {templateBuilder} from "../../Builder/TemplateBuilder/TemplateBuilder";

export class TemplateService {
    public status: "process" | "success" | "failure";
    public confirm: boolean;
    private service:  TemplateRequest;
    constructor() {
        makeObservable(this, {
            status: observable,
            launch: action,
        })
        this.service = new TemplateRequest();
        this.status = "process";
        this.confirm = true;

    }

    private getTemplateResource = async (template:Document) =>{
        const collection = template.querySelectorAll("Template\\.Resource Resource");
        this.confirm = false;
        for(const element of collection){
            const id = element.getAttribute("Source");
            const processNotice = element.getAttribute("ProcessNotice");
            store.process.launch({notice:processNotice})
            if(id){
                await new TemplateService().launch({source:id})
            }
        }
        this.confirm = true;
    }

    public launch = async (param:any) => {
        let props:any ={};
        if(param.source){
            props = Object.assign({}, param);
        } else {
            props = Object.assign({}, param, {source:param.templateIdentifier});
        }
        this.status = "process";
        if(store.template.method.obtain(props.source)){
            this.status = "success";
        } else {
            store.process.launch({notice:props.processNotice})
            const request = obtainCoreRequest(AppConfigs.templateRequest);
            if(request){
                try {
                    const requestWithParam = {
                        URI:request.URI(props.source),
                        description:request.description,
                        mimeType:request.mimeType,
                    };
                    await this.service.obtain(requestWithParam);
                    if(this.service.status === "success"){
                        if(this.service.response){
                            //const template = this.service.response.replace(/>\s+?</g, "><").replace(/\s+/g, ' ');

                            const templateDocument = new DOMParser().parseFromString(this.service.response,"text/xml");
                            await this.getTemplateResource(templateDocument);
                            templateBuilder(templateDocument);
                            const template = new XMLSerializer().serializeToString(templateDocument);
                            store.template.method.record({id: props.source, template: template});

                            if(this.confirm){

                                this.status = "success";
                                store.process.method.finish();
                            } else {
                                this.status = "process";
                            }
                        }
                    }
                } catch ( error ) {
                    store.process.method.finish();
                    this.status = "failure";
                }
            } else {
                store.process.method.finish();
                this.status = "failure";
            }
        }
    }
}