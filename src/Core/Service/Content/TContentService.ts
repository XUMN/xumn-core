export type TContentService = {
    providerType:string;
    template:string;
    processNotice?: string;
    successNotice?: string;
    failureNotice?: string;
    processStatusBinding?: string;
}