import {observable,action,makeObservable} from "mobx";
import {RequestData} from "../../Request/Data/RequestData";
import {CoreBinding} from "../../CoreBinding";
import {BindingParser} from "../../Util/BindingParser";
import {requestParser} from "../../Factory/RequestFactory";
import {CoreConverter} from "../../CoreConverter";
import {store} from "../../CoreStore";
import {getChildrenAttribute} from "../../Util/NodeXML";
import {conditionFacade} from "../../CoreCondition";
import {TContentService} from "./TContentService";

export class ContentService {
    public status: "process" | "success" | "failure";
    protected service:RequestData;
    constructor() {
        makeObservable(this, {
            status: observable,
            process: action,
            asynchronousProcess: action,
            supplyContent: action,
        })
        this.status = "process";
        this.service = new RequestData();
    }
    sleep = (m:number) => new Promise(r => setTimeout(r, m));

    public process = async (props: TContentService) => {

        if(props.processStatusBinding){
            new CoreBinding().recordValue(props.processStatusBinding,"Process");
        } else {
            store.process.launch({notice:props.processNotice})
        }
        const requestData = requestParser.launch(props.template);
        const requestProp = {
            URI:requestData.URI(),
            description:requestData.description(),
            processNotice:{
                name: props.processNotice,
                notification: props.processNotice
            }
        };
        await this.service.obtain(requestProp);
        switch ( this.service.status ) {
            case "success":
                const into = BindingParser.bindingParsingExpression(requestData.binding());
                into.bindingValue = this.service.response;
                new CoreConverter().launch(into);
                new CoreBinding().sourceValue(into.bindingSource).method.record( into );
                this.status = "success";
                if(props.processStatusBinding){
                    new CoreBinding().removeValue(props.processStatusBinding);
                }
                store.process.method.finish();
                break;
            case "process":
                this.status = "failure";
                if(props.processStatusBinding){
                    new CoreBinding().removeValue(props.processStatusBinding);
                }
                store.process.method.finish();
                break;
            case "failure":
                this.status = "failure";
                if(props.processStatusBinding){
                    new CoreBinding().removeValue(props.processStatusBinding);
                }
                store.process.method.finish();
                store.failure.method.launch(this.service.response);
                break;
            default:
                if(props.processStatusBinding){
                    new CoreBinding().removeValue(props.processStatusBinding);
                }
                store.process.method.finish();
                return {error:"unknown"};
        }
    };

    public asynchronousProcess = async (props: TContentService) => {
        const requestData = requestParser.launch(props.template);
        const requestProp = {
            URI:requestData.URI(),
            description:requestData.description(),
            processNotice:{
                name: props.processNotice,
                notification: props.processNotice
            }
        };
        await this.service.obtain(requestProp);

        switch ( this.service.status ) {
            case "success":
                const binding = BindingParser.bindingParsingExpression(requestData.binding());
                binding.bindingValue = this.service.response;
                new CoreConverter().launch(binding);
                new CoreBinding().sourceValue(binding.bindingSource).method.record( binding );
                const condition = {status: true } as { [key:string]: boolean };
                getChildrenAttribute(props.template,":root > Request > Condition").map( (props:any) => condition.status = conditionFacade(props) ?  condition.status : false );
                if( condition.status ){
                    this.status = "success";
                } else {
                    await this.sleep( 500 );
                    await this.asynchronousProcess(props);
                }
                break;
            case "process":
                break;
            case "failure":
                store.failure.method.launch(this.service.response);
                break;
            default:
                return {error:"unknown"};
        }
    };

    public supplyContent = async (props: TContentService) => {
        this.status = "process";
        const binding = requestParser.launch(props.template).binding();
        if(props.providerType === "Regular"){
            new CoreBinding().obtainValue(binding).bindingValue ? this.status = "success" : await this.process(props);
        } else if(props.providerType === "Asynchronous"){
            await this.asynchronousProcess(props);
        } else {
            new CoreBinding().removeValue(binding)
            await this.process(props);
        }
    }
}

export class TemplateContent {
    public status: "process" | "success" | "failure";
    constructor() {

        this.status = "process";
    }
    async launch(template: string){
        this.status = "process";
        const XML = new DOMParser().parseFromString(template, "text/xml");
        const collection = XML.querySelectorAll(":root > Template\\.Content > Provider")
        for (const element of collection ){
            this.status = "process";
            const request = element.querySelector("Request");
            if(!request) {continue}
            const providerType = element.getAttribute("ProviderType");
            if(!providerType) {continue}
            const value = {
                providerType:providerType,
                template:new XMLSerializer().serializeToString(element),
            }
            await new ContentService().supplyContent(value);
        }
        this.status = "success";
    }
}
export const contentService:ContentService = new ContentService();