export type TCommandService = {
    into:string;
    providerType:string;
    template:string;
    processNotice?: string;
    successNotice?: string;
    failureNotice?: string;
}