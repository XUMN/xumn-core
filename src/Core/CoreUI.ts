
export type TCoreUI = { [id: string]: any }

export const CoreUI = {
    "Application": require("./UI/Application/Application"),
    "Navigation": require("./UI/Layout/Navigation/NavigationLayout"),
    "Main": require("./UI/Layout/Main/MainLayout"),
    "Header": require("./UI/Layout/Header/HeaderLayout"),
    "Footer": require("./UI/Layout/Footer/FooterLayout"),
    "SideBar": require("./UI/Layout/SideBar/SideBar"),

    "Router": require("./UI/Routing/Router/RouterTest"),
    "Route": require("./UI/Routing/Route/RouterRoute"),
    "RouteSwitch": require("./UI/Routing/Switch/RouteSwitch"),
    "RouteButton": require("./UI/Routing/Button/RouteButton"),
    "RouteRedirect": require("./UI/Routing/Redirect/RouteRedirect"),
    "Link": require("./UI/Routing/Link/RouteLink"),

    "Template": require("./UI/Template/Template"),
    "Slot": require("./UI/Slot/Slot"),
    "Dialog": require("./UI/Dialog/Dialog"),


    "Protection": require("./UI/Router/Protection"),
    "Provider": require("./UI/Provider/Content/ContentProvider"),

    "Condition": require("./UI/Condition/Condition"),
    "Loop": require("./UI/Loop/Loop"),
    "Failure": require("./UI/Failure/Failure"),
    "Process": require("./UI/Process/Process"),
    "Modal": require("./UI/Modal/Modal"),
    "ScrollView": require("./UI/ScrollView/ScrollView"),
    "ResourceDictionary": require("./UI/ResourceDictionary/ResourceDictionary"),
    "TextArea": require("./UI/Area/TextArea"),
    "ComboBox": require("./UI/Combo/Box/ComboBox"),

    "Switch": require("./UI/Switch/Switch"),

    "OutsideClickObserver": require("./UI/Observer/OutsideClick/OutsideClickObserver"),
    "QueryParamObserver": require("./UI/Button/QueryParamObserver"),

    "Menu": require("./UI/Menu/Menu"),
    "MenuToggle": require("./UI/Menu/Toggle/MenuToggle"),
    "MenuGroup": require("./UI/Menu/Group/MenuGroup"),
    "MenuItem": require("./UI/Menu/Item/MenuItem"),

    "Select": require("./UI/Forms/Select/Select"),
    "OptionGroup": require("./UI/Forms/OptionGroup/OptionGroup"),
    "Option": require("./UI/Forms/Option/Option"),

    "InputList": require("./UI/Forms/Input/List/InputList"),

    "Text": require("./UI/Text/Text"),
    "Span": require("./UI/Text/Span/TextSpan"),
    "Code": require("./UI/Text/Code/TextCode"),
    "Time": require("./UI/Text/Time/TextTime"),

    "Date": require("./UI/Text/Time/TextTime"),
    "Image": require("./UI/Image/Image"),

    "Status": require("./UI/Status/Status"),
    "Auto": require("./UI/Button/Auto"),
    "AutoRun": require("./UI/Button/AutoRun"),
    "AutoRunMulti": require("./UI/Button/AutoRunMulti"),
    "Button": require("./UI/Forms/Button/Button"),

    "InputText": require("./UI/Forms/Input/Text/InputText"),
    "InputButton": require("./UI/Forms/Input/Button/InputButton"),
    "InputImage": require("./UI/Forms/Input/Image/InputImage"),
    "InputDate": require("./UI/Forms/Input/Date/InputDate"),
    "InputTime": require("./UI/Input/Time/InputTime"),
    "InputSearch": require("./UI/Input/Search/InputSearch"),

    "InputPassword": require("./UI/Forms/Input/Password/InputPassword"),
    "CheckBox": require("./UI/CheckBox/CheckBox"),

    "AutoComplete": require("./UI/Forms/AutoComplete/AutoComplete"),
    "AutoCompleteGroup": require("./UI/Forms/AutoComplete/Group/AutoCompleteGroup"),

    "Group": require("./UI/Layout/Group/Group"),
    "Block": require("./UI/Layout/Block/Block"),

    "PositionLayout": require("./UI/Layout/Position/PositionLayout"),
    "StackLayout": require("./UI/Layout/Stack/StackLayout"),

    "Flex": require("./UI/Layout/Flex/FlexLayout"),
    "Grid": require("./UI/Layout/Grid/GridLayout"),
    "GridCell": require("./UI/Layout/Grid/Cell/GridCell"),
    "GridRow": require("./UI/Layout/Grid/Row/GridRow"),

    "Column": require("./UI/Layout/Column/ColumnLayout"),

    "Details": require("./UI/Details/Details"),
    "Summary": require("./UI/Details/Summary"),

    "Table": require("./UI/Layout/Table/TableLayout"),
    "TableHead": require("./UI/Layout/Table/Head/TableHead"),
    "TableBody": require("./UI/Layout/Table/Body/TableBody"),
    "TableFoot": require("./UI/Layout/Table/Foot/TableFoot"),

    "Row": require("./UI/Layout/Table/Row/TableRow"),
    "Cell": require("./UI/Layout/Table/Cell/TableCell"),

    "TableBodyLink": require("./UI/Layout/Table/Body/Link/TableBodyLink"),
    "TableCell": require("./UI/Layout/Table/Cell/TableCell"),
    "Paginator": require("./UI/Paginator/Paginator"),
    "Range": require("./UI/Paginator/Range/Range"),

} as TCoreUI;

export const launchCoreUI = ( AppUI: TCoreUI ) => {
    for(const key in AppUI ){
        if(AppUI.hasOwnProperty(key)){
            CoreUI[key] = AppUI[key];
        }
    }
};