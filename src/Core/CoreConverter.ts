import {obtainValue, recordValue} from "./Util/ValueMethod";
import {store} from "./CoreStore";

export const decodeString = (str: string) => {
    const decode = escape(atob( str ));
    return decodeURIComponent( decode );
};

export const encodeString = (str: string) => {
    const encode = unescape(encodeURIComponent( str ));
    return btoa( encode);
};

export class CoreConverter {
    encode = (props:any) => {
        try {
            props.bindingValue = encodeString(JSON.stringify(props.bindingValue));
        } catch (error){
            console.log( error )
        }
    };
    decode = async (props:any) =>{
        if(props.converterProperty){
            try {
                let value = obtainValue(props.bindingValue,props.converterProperty);
                value =  decodeString(value);
                recordValue(props.bindingValue,{
                    bindingPath:props.converterProperty,
                    bindingValue: JSON.parse(value),
                })
            } catch (error){
                console.log( error )
            }
        }
    };

    dictionaryMapper = (props:any) => {
        const dict = store.dictionary.method.obtain({bindingPath: props.converterProperty});
        if(Array.isArray(dict)){
            const item = dict.find(e => e.key === props.bindingValue);
            if(item){
                props.bindingValue = item.value;
            } else {
                props.bindingValue = undefined;

            }
        } else {
            props.bindingValue = undefined;
        }
    };
    boolean = (props:any) => {
        props.bindingValue = JSON.parse(props.bindingValue);
    };
    number = (props:any) => {
        props.bindingValue = Number(props.bindingValue);
    };
    object = (props:any) => {
        props.bindingValue = {};
    };
    parse = (props:any) => {
        props.bindingValue = JSON.parse(props.bindingValue);
    };
    clone = (props:any) => {
        props.bindingValue = JSON.parse(JSON.stringify(props.bindingValue));
    };
    keyName = (props:any) => {
        if (Array.isArray(props.bindingValue)) {
            const e = props.bindingValue.find((e:any) => e.hasOwnProperty(props.converterProperty));
            props.bindingValue = e ?  e[props.converterProperty] : undefined;
        } else {
            props.bindingValue = undefined;
        }
    };
    increment = (props:any) => {
        let number = Number(props.bindingValue);
        props.bindingValue = ++number;
    }
    public method = {
        "Encode": (props:any) => this.encode(props),
        "Decode": (props:any) => this.decode(props),
        "DictionaryMapper": (props:any) => this.dictionaryMapper(props),
        "Boolean": (props:any) => this.boolean(props),
        "Number": (props:any) => this.number(props),
        "Object": (props:any) => this.object(props),
        "Parse": (props:any) => this.parse(props),
        "KeyName": (props:any) => this.keyName(props),
        "Clone": (props:any) => this.clone(props),
"Increment": (props:any) => this.increment(props),
    } as { [key:string]:Function };

    launch = (props:any) =>{
        if(props.bindingConverter){
            if(this.method[props.bindingConverter]){
                this.method[props.bindingConverter](props)
            }
        }
    }
}