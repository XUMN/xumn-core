import {BindingParser} from "./Util/BindingParser";
import {CoreBinding} from "./CoreBinding";
import {getChildrenAttribute, parseXML} from "./Util/NodeXML";
import {obtainCoreCommand} from "./CoreCommand";


export const objectHasOutputValue = (value:string) => {
    let result: boolean = false;
    const refers = new CoreBinding().refersValue(value);
    if(refers){
        for ( const key in refers ) {
            if(refers[key]){
                if(refers[key] !== ""){
                    result = true;
                }
            }
        }
    }
    return result;
};

export const objectNotHasOutputValue = (value:string) => {
    let result: boolean = true;
    const refers = new CoreBinding().refersValue(value);
    if(refers){
        for ( const key in refers ) {
            if(refers[key]){
                if(refers[key] !== ""){
                    result = false;
                }
            }
        }
    }
    return result;
};




export const isOutput = (value:string) => {
    let result: boolean = true;
    BindingParser.bindingCollection(value).map(e => new CoreBinding().outputValue(e).bindingValue === "" ? result = false : null);
    return result;
};

export const notOutput = (value:string) => {
    let result:boolean = false;
    BindingParser.bindingCollection(value).map(e => new CoreBinding().outputValue(e).bindingValue === "" ? result = true : null );
    return result;
};


export const notEmpty = (value:string) => {
    const data = new CoreBinding().obtainValue(value).bindingValue;
    return Array.isArray(data) ? data.length !== 0 : false;
};

export const isEmpty = (value:string) => {
    const data = new CoreBinding().obtainValue(value).bindingValue;
    return Array.isArray(data) ? data.length === 0 : false;
};

export const isValue = (value:any) => {
    let result:boolean = true;
    BindingParser.bindingCollection(value).map(e => new CoreBinding().obtainValue(e).bindingValue ? null : result = false );
    return result;
};

export const notValue = (value:string) => {
    let result:boolean = true;
    BindingParser.bindingCollection(value).map(e => new CoreBinding().obtainValue(e).bindingValue ? result = false : null );
    return result;
};

export const isEqual = (value:string) => {
    let result:boolean = true;
    const collection: Array<any> = [];
    BindingParser.bindingCollection(value).map(e => collection.push(new CoreBinding().obtainValue(e).bindingValue));
    collection.map(e => e !== collection[0] ? result = false : null);
    return result;
};

export const notEqual = (value:string) => {
    //let result:boolean = true;
    const collection: Array<any> = [];
    BindingParser.bindingCollection(value).map(e => collection.push(new CoreBinding().obtainValue(e).bindingValue));
    if (collection.length !== 2){
        return false;
    }
    return collection[0] !== collection[1];
};










export const notEmptyAll = (value:string) => {
    const data = new CoreBinding().obtainValue(value).bindingValue;
    return Array.isArray(data) ? data.length !== 0 : false;
};

export const emptyAll = (value:string) => {
    const data = new CoreBinding().obtainValue(value).bindingValue;
    return Array.isArray(data) ? data.length === 0 : true
};

export const hasAllOutput = (value:string) => {
    let result: boolean = true;
    BindingParser.bindingCollection(value).map(e => new CoreBinding().outputValue(e).bindingValue === "" ? result = false : null);
    return result;
};

export const notHasAllOutput = (value:string) => {
    let result:boolean = false;
    BindingParser.bindingCollection(value).map(e => new CoreBinding().outputValue(e).bindingValue === "" ? result = true : null );
    return result;
};


export const hasAll = (value:string) => {
    let result:boolean = true;
    BindingParser.bindingCollection(value).map(e => new CoreBinding().obtainValue(e).bindingValue === undefined ? result = false : null );
    return result;
};

export const hasAny = (value:string) => {
    let result:boolean = false;
    BindingParser.bindingCollection(value).map(e => new CoreBinding().obtainValue(e).bindingValue !== undefined ? result = true : null );
    return result;
};

export const notHasAny = (value:string) => {
    let result:boolean = false;
    BindingParser.bindingCollection(value).map(e => new CoreBinding().obtainValue(e).bindingValue === undefined ? result = true : null );
    return result;
};

export const notHasAll = (value:string) => {
    let result:boolean = true;
    BindingParser.bindingCollection(value).map(e => new CoreBinding().obtainValue(e).bindingValue !== undefined ? result = false : null);
    return result;
};



export const hasOnly = (value:string, source:string) => {
    const refers = new CoreBinding().refersValue(source);
    if(typeof refers === "object"){
        let result:boolean = true;
        const collection: {[key:string]:any} = {};
        BindingParser.bindingCollection(value).map(e => collection[new CoreBinding().obtainValue(e).bindingPath] = new CoreBinding().obtainValue(e).bindingValue);
        Object.keys(refers).map(key=> collection.hasOwnProperty(key) ?  null : result = false);
        return result;
    } else {
        return false
    }
};

export const notHasOnly = (value:string, source:string) => {
    const refers = new CoreBinding().refersValue(source);
    if(typeof refers === "object"){
        let result:boolean = false;
        const collection: {[key:string]:any} = {};
        BindingParser.bindingCollection(value).map(e => collection[new CoreBinding().obtainValue(e).bindingPath] = new CoreBinding().obtainValue(e).bindingValue);
        Object.keys(refers).map(key=> collection.hasOwnProperty(key) ?  null : result = true);
        return result;
    } else {
        return false
    }
};

export const equalAll = (value:string) => {
    let result:boolean = true;
    const collection: Array<any> = [];
    BindingParser.bindingCollection(value).map(e => collection.push(new CoreBinding().obtainValue(e).bindingValue));
    collection.map(e => e !== collection[0] ? result = false : null);
    return result;
};


export const notEqualAny = (value:string) => {
    let result:boolean = true;
    const collection: Array<any> = [];
    BindingParser.bindingCollection(value).map(e => collection.push(new CoreBinding().obtainValue(e).bindingValue));

    collection.map((e, i) => i !== 0 && e === collection[0] ? result = false : null);
    return result;
};

export const equalAny = (value:string) => {
    let result:boolean = false;
    const collection: Array<any> = [];
    BindingParser.bindingCollection(value).map(e => collection.push(new CoreBinding().obtainValue(e).bindingValue));

    collection.map((e, i) => i !== 0 && e === collection[0] ? result = true : null);
    return result;
};

export const notEqualAll = (value:string) => {
    //let result:boolean = true;
    const collection: Array<any> = [];
    BindingParser.bindingCollection(value).map(e => collection.push(new CoreBinding().obtainValue(e).bindingValue));
    if (collection.length !== 2){
        return false;
    }
    return collection[0] !== collection[1];
};

export const isEmptyAll = (value:string) => {
    //let result:boolean = true;
    const collection: Array<any> = [];
    BindingParser.bindingCollection(value).map(e => collection.push(new CoreBinding().obtainValue(e).bindingValue));
    if (collection.length === 0){
        return true;
    }
    return collection[0] === undefined || collection[0] === null
        || collection[0] === '' || collection[0] === 'undefined' || collection[0] === 'null';
};

export const isEmptyAny = (value:string) => {
    const collection: Array<any> = [];
    BindingParser.bindingCollection(value).map(e => collection.push(new CoreBinding().obtainValue(e).bindingValue));
    if (collection.length === 0){
        return true;
    }
    for (let el of collection) {
        if (el === undefined || el === null || el === '' || el === 'undefined' || el === 'null'){
            return true;
        }
    }
    return false;
};

export const isNotEmptyAll = (value:string) => {
    //let result:boolean = true;
    const collection: Array<any> = [];
    BindingParser.bindingCollection(value).map(e => collection.push(new CoreBinding().obtainValue(e).bindingValue));
    if (collection.length === 0){
        return true;
    }
    return collection[0] !== undefined && collection[0] !== null
        && collection[0] !== '' && collection[0] !== 'undefined' && collection[0] !== 'null';
};



export type TCondition = {
    conditionType: string;
    conditionValue: string;
    conditionSource?: string;
    valueType?: string;
}

export const conditionCatalog:{[key:string]:Function} = {
    "ObjectHasOutputValue": (props: TCondition) => objectHasOutputValue(props.conditionValue),
    "ObjectNotHasOutputValue": (props: TCondition) => objectNotHasOutputValue(props.conditionValue),

    "IsOutput": (props: TCondition) => isOutput(props.conditionValue),
    "NotOutput": (props: TCondition) => notOutput(props.conditionValue),

    "IsValue": (props: TCondition) => isValue(props.conditionValue),
    "NotValue": (props: TCondition) => notValue(props.conditionValue),
    "IsEmpty": (props: TCondition) => isEmpty(props.conditionValue),
    "NotEqualAny": (props: TCondition) => notEqualAny(props.conditionValue),


    "NotEmpty": (props: TCondition) => notEmpty(props.conditionValue),
    "IsEqual": (props: TCondition) => isEqual(props.conditionValue),
    "NotEqual": (props: TCondition) => notEqual(props.conditionValue),

    "HasAllOutput": (props: TCondition) => hasAllOutput(props.conditionValue),
    "NotHasAllOutput": (props: TCondition) => notHasAllOutput(props.conditionValue),
    "HasAll": (props: TCondition) => hasAll(props.conditionValue),
    "NotHasAll": (props: TCondition) => notHasAll(props.conditionValue),
    "HasAny": (props: TCondition) => hasAny(props.conditionValue),
    "NotHasAny": (props: TCondition) => notHasAny(props.conditionValue),
    "HasOnly": (props: TCondition) => props.conditionSource ? hasOnly(props.conditionValue, props.conditionSource) : false,
    "NotHasOnly": (props: TCondition) => props.conditionSource ? notHasOnly(props.conditionValue, props.conditionSource) : false,
    "EmptyAll": (props: TCondition) => emptyAll(props.conditionValue),


    "NotEmptyAll": (props: TCondition) => notEmptyAll(props.conditionValue),
    "EqualAll": (props: TCondition) => equalAll(props.conditionValue),
    "EqualAny": (props: TCondition) => equalAny(props.conditionValue),
    "NotEqualAll": (props: TCondition) => notEqualAll(props.conditionValue),
    "IsEmptyAll": (props: TCondition) => isEmptyAll(props.conditionValue),
    "IsEmptyAny": (props: TCondition) => isEmptyAny(props.conditionValue),
    "IsNotEmptyAll": (props: TCondition) => isNotEmptyAll(props.conditionValue),
};

export class CoreAction {
    static implement = (template: string, selector: string) => {
        const element = parseXML(template).querySelectorAll(selector);
        getChildrenAttribute(template, selector, "lowercase").map((command, i) => {
            if (obtainCoreCommand(command.commandName)) {
                command.template = new XMLSerializer().serializeToString(element[i]);
                return obtainCoreCommand(command.commandName).perform(command);
            } else {
                return console.warn("Unknown command Name:", command.commandName);
            }
        });
    };
}

const t = (template:string) => {
    let result = String();

    const root = new DOMParser().parseFromString(template, "text/xml").documentElement;
    const element = root.querySelectorAll(':root > Setter[Property="ConditionValue"]');



    Array.from(element).map(e=>{
        return result += e.getAttribute("Value");
    });

    return result;
}
export const conditionFacade = (props: any, trigger?:any) => {


    const condition: {[name:string]:string} = {}
    if(props.conditionType){
        condition.conditionType = props.conditionType;
        if(props.conditionValue){
            condition.conditionValue = props.conditionValue;
        } else if(props.value) {
            condition.conditionValue = props.value;
        } else {
            if(props.template){
                condition.conditionValue = t(props.template);
            }
        }
        if(props.conditionSource) {
            condition.conditionSource = props.conditionSource;
        }
    }

    if (condition.hasOwnProperty("conditionValue")) {
        return conditionCatalog.hasOwnProperty(condition.conditionType) ? conditionCatalog[condition.conditionType](condition) : false;
    } else {
        return true;
    }

}

