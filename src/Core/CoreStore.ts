import {SessionStore} from "./Store/Session/SessionStore";
import {ContentStore} from "./Store/Content/ContentStore";
import {FailureStore} from "./Store/Failure/FailureStore";
import {ProcessStore} from "./Store/Process/ProcessStore";
import {FiltersStore} from "./Store/Filters/FiltersStore";
import {TemplateStore} from "./Store/Template/TemplateStore";
import {RoutingStore} from "./Store/Routing/RoutingStore";
import {ModalStore} from "./Store/Modal/ModalStore";
import {DictionaryStore} from "./Store/Dictionary/DictionaryStore";

export class CoreStore {
    readonly modal: ModalStore;
    readonly session: SessionStore;
    readonly process: ProcessStore;
    readonly failure: FailureStore;
    readonly filters: FiltersStore;
    readonly routing: RoutingStore;
    readonly content: ContentStore;
    readonly dictionary: DictionaryStore;
    readonly template: TemplateStore;
    constructor() {
        this.modal = new ModalStore();
        this.session = new SessionStore();
        this.process = new ProcessStore();
        this.failure = new FailureStore();
        this.filters = new FiltersStore();
        this.routing = new RoutingStore();
        this.content = new ContentStore();
        this.dictionary = new DictionaryStore();
        this.template = new TemplateStore();
    }
}

export const store:CoreStore = new CoreStore();