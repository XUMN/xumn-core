import {action, observable,makeObservable} from "mobx";
import {obtainValue, recordValue, refersValue, removeValue} from "../../Util/ValueMethod";
import {TProcessStore} from "./TProcessStore"

export class ProcessStore {
    public state: boolean;
    public props: TProcessStore | any;
    constructor( ) {
        makeObservable(this, {
            props: observable,
            state: observable,
            recordValueByPath: action,
            removeValueByPath: action,
            launch: action,
            finish: action,
        })
        this.state = false;
        this.props = {
            notice: null,
            status:false
        };
    }

    public launch(props?:TProcessStore){
        this.props.notice = props?.notice;
        this.props.status = true;
        document.querySelector("body")?.setAttribute("data-process", "true");
    }

    public finish(){
        delete this.props.notice;
        this.props.status = false;
        document.querySelector("body")?.removeAttribute("data-process");
    }

    public obtainValueByPath = (param:any) => obtainValue(this.props, param.bindingPath);
    public refersValueByPath = (param:any) => refersValue(this.props, param.bindingPath);
    public recordValueByPath = (param:any) => recordValue(this.props, param);
    public removeValueByPath = (param:any) => removeValue(this.props, param.bindingPath, param.bindingValue);

    public method = {
        status: () => this.state,
        launch: (param:TProcessStore) => this.launch(param),
        finish: () => this.finish(),
        obtain: (param:any) => this.obtainValueByPath(param),
        refers: (param:any) => this.refersValueByPath(param),
        record: (param:any) => this.recordValueByPath(param),
        remove: (param:any) => this.removeValueByPath(param),
    }
}