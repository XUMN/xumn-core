export class SessionStorage {
    public storage: string;
    public event: string;
    constructor( store:string, event:string ) {
        this.storage = store;
        this.event = event;
    }
    private signal = () => {
        const event = new Event( this.event, { "bubbles": true, "cancelable": false } );
        document.dispatchEvent( event );
    };

    public record = ( content: string ) => {
        localStorage[this.storage] = content;
        this.signal();
    };

    public remove = () => {
        localStorage.removeItem(this.storage);
        this.signal();
    };

    public obtain = () =>{
        return localStorage[this.storage] ? localStorage[this.storage] : null;
    };
}

export default SessionStorage;
