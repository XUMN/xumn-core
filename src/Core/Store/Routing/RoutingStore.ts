import {observable, action, makeObservable} from "mobx";
import {obtainValue, recordValue, refersValue, removeValue} from "../../Util/ValueMethod";

export class RoutingStore {
    props: { [id: string]: any };

    constructor() {
        makeObservable(this, {
            props: observable,
            recordValueByPath: action,
            removeValueByPath: action,
            revoke: action,
        })
        this.props = {};
    }

    private obtainValueByPath = (param:any) => obtainValue(this.props, param.bindingPath);
    private refersValueByPath = (props:any) => refersValue(this.props, props.bindingPath);
    recordValueByPath = (param:any) => recordValue(this.props, param);

    removeValueByPath = (param:any) => removeValue(this.props, param.bindingPath, param.bindingValue);
    revoke = () => this.props = {};

    public method = {
        obtain: (param:any) => this.obtainValueByPath(param),
        refers: (param:any) => this.refersValueByPath(param),
        record: (param:any) => this.recordValueByPath(param),
        remove: (param:any) => this.removeValueByPath(param),
        revoke: () => this.revoke(),
    }
}