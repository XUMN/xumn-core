import {action, observable,makeObservable} from "mobx";
import {TModalStore} from "./TModalStore";

export class ModalStore {
    public state: boolean;
    public source: string;
    public triggerTemplate: string;

    constructor( ) {
        makeObservable(this, {
            state: observable,
            source: observable,
            triggerTemplate: observable,
            launch: action,
            finish: action,
        })
        this.source = "";
        this.triggerTemplate = "";
        this.state = false;
        window.addEventListener("MutationHistory", () => this.finish(), false);
    }

    public launch(props:TModalStore){
        this.source = props.source;
        this.state = true;
        document.querySelector("body")?.setAttribute("data-modal", "true");
    }

    public finish(){
        this.state = false;
        this.source = "";
        document.querySelector("body")?.removeAttribute("data-modal");
    }

    public method = {
        templateIdentifier: () => this.source,
        status: () => this.state,
        launch: (props:any) => this.launch(props),
        finish: () => this.finish(),
    }
}