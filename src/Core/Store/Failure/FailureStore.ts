import {action, observable, makeObservable} from "mobx";
import {
    obtainValue,
    recordValue,
    refersValue,
    removeValue
} from "../../Util/ValueMethod";
import {TFailureStore} from "./TFailureStore";

export class FailureStore {
    public state: boolean;
    public props: TFailureStore | any;
    constructor( ) {
        makeObservable(this, {
            state: observable,
            props: observable,
            launch: action,
            finish: action,
            recordValueByPath: action,
            removeValueByPath: action,

        })
        this.state = false;
        this.props = {};
    }
    public launch(props:any){
        this.props.status = props;
        console.log( JSON.stringify(props))
        this.state = true;
    }
    public finish(){
        this.props = {};
        this.state = false;
    }

    public obtainValueByPath = (param:any) => obtainValue(this.props, param.bindingPath);
    public refersValueByPath = (param:any) => refersValue(this.props, param.bindingPath);
    public recordValueByPath = (param:any) => recordValue(this.props, param);
    public removeValueByPath = (param:any) => removeValue(this.props, param.bindingPath, param.bindingValue);

    public method = {
        status: () => this.state,
        finish: () => this.finish(),
        launch: (param:any) => this.launch(param),
        obtain: (param:any) => this.obtainValueByPath(param),
        refers: (param:any) => this.refersValueByPath(param),
        record: (param:any) => this.recordValueByPath(param),
        remove: (param:any) => this.removeValueByPath(param),
    }
}
