export type TContentStore = {
    bindingPath: string;
    data: string;
}
