import {action, observable} from "mobx";

export class SidebarStore {
    @observable public state: boolean;
    constructor( ) {
        this.state = true;
    }

    @action public finish(){
        this.state = false;
    }

    @action public toggle(){
        this.state = !this.state;
    }

    public method = {
        status: () => this.state,
        toggle: () => this.toggle(),
    }
}