import {observable, action, makeObservable} from "mobx";
import {recordValue, removeValue, obtainValue, refersValue} from "../../Util/ValueMethod";

export class FiltersStore {
    public props: {
        [id: string]: any;
    };

    constructor() {
        makeObservable(this, {
            props: observable,
            recordValueByPath: action,
            removeValueByPath: action,
            revoke: action,
        })
        this.props = {};
    }

    public obtainValueByPath = (param:any) => obtainValue(this.props, param.bindingPath);
    public refersValueByPath = (param:any) => refersValue(this.props, param.bindingPath);
    public recordValueByPath = (param:any) => recordValue(this.props, param);
    public removeValueByPath = (param:any) => removeValue(this.props, param.bindingPath, param.bindingValue);
    public revoke = () => this.props = {};

    public method = {
        obtain: (param:any) => this.obtainValueByPath(param),
        refers: (param:any) => this.refersValueByPath(param),
        record: (param:any) => this.recordValueByPath(param),
        remove: (param:any) => this.removeValueByPath(param),
        revoke: () => this.revoke(),
    }
}