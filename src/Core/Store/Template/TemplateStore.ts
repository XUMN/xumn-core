import {observable,action,makeObservable} from "mobx";
import {TTemplateStore} from "./TTemplateStore";

export class TemplateStore {
    public readonly props: {
        [id: string]: string;
    };

    constructor() {
        makeObservable(this, {
            props: observable,
            recordByName: action,
        })
        this.props = {};
    }

    public recordByName = ( param:TTemplateStore ) => this.props[param.id] = param.template;

    public obtainByName = (id:string) => this.props[id] ? this.props[id] : undefined;

    public method = {
        obtain:(id:string) => this.obtainByName(id),
        record:(param:TTemplateStore) => this.recordByName(param)
    }
}