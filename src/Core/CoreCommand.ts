import {store} from "./CoreStore";
import {CoreBinding} from "./CoreBinding";

import {ContentService} from "./Service/Content/ContentService";
import {getChildrenAttribute} from "./Util/NodeXML";
import {linkGenerator} from "./Util/LinkGenerator";
import {downloadFromData} from "./Command/DownloadFromData";
import {asyncDownload} from "./Command/AsyncDownload";
import {BindingParser} from "./Util/BindingParser";
import {Paginator} from "./Command/Paginator";

export type TFacadeCommand = { [name:string]:Function };

export const catalogCoreCommand = {
    authorization: () =>{
        return {
            perform:() => store.session.method.launch(),
        };
    },
    authorizationSso: () =>{
        return {
            perform:() => {
                let basePath = '';
                if (window.location.host === 'etrn-x5-pp.esphere.ru'){
                    basePath = "https://lkk-preprod.esphere.ru/auth/UI/Login";
                } else if (window.location.host === 'logistics.esphere.ru'){
                    basePath = "https://lkk.esphere.ru/auth/UI/Login";
                } else {
                    basePath = "https://lkk-test.esphere.ru/auth/UI/Login";
                }
                let gotoPath = window.location.protocol + "//" + window.location.host;
                window.location.replace(basePath + "?realm=lkk_sfera&goto=" + gotoPath + "/auth/login/sso");
            },
        };
    },
    goToHome: () =>{
        return {
            perform:() => {
                let gotoPath = window.location.protocol + "//" + window.location.host + "/";
                window.location.replace(gotoPath);
            },
        };
    },


    goToCourier: () =>{
        return {
            perform:() => {
                let basePath = '';
                if (window.location.host === 'etrn-x5-pp.esphere.ru'){
                    basePath = "https://courier-demo.esphere.ru";
                } else if (window.location.host === 'logistics.esphere.ru'){
                    basePath = "https://courier.esphere.ru";
                } else {
                    basePath = "https://courier-demo.esphere.ru";
                }
                window.open(basePath, '_blank');
            },
        };
    },
    goToLkk: () =>{
        return {
            perform:() => {
                let basePath = '';
                if (window.location.host === 'etrn-x5-pp.esphere.ru'){
                    basePath = "https://lkk-preprod.esphere.ru";
                } else if (window.location.host === 'logistics.esphere.ru'){
                    basePath = "https://lkk.esphere.ru";
                } else {
                    basePath = "https://lkk-test.esphere.ru";
                }
                window.open(basePath, 'blank');
            },
        };
    },
    logout: () =>{
        return {
            perform:() => {
                store.content.method.revoke();
                store.session.method.revoke();
            },
        };
    },
    request: () => {
        return {
            perform:(props:any) => {
                return new ContentService().supplyContent(props)
            }
        };
    },

    recordBinding: () => {
        return {
            perform:(props:any) => new CoreBinding().recordBinding(props.into, props.value)
        }
    },


    copyValue: () => {
        return {
            perform:(props:any) => {
                const value = JSON.parse(JSON.stringify(new CoreBinding().obtainValue(props.value).bindingValue));
                new CoreBinding().recordValue(props.to, value)
            }
        }
    },
    delValue: () => {
        return {
            perform:(props:any) => new CoreBinding().removeValue(props.from)
        }
    },
    setValue: () => {
        return {
            perform:(props:any) => {
                new CoreBinding().recordValue(props.to, props.value)
            }

        }
    },

    recordValue: () => {
        return {

            perform:(props:any) => new CoreBinding().recordValue(props.into, props.value)

        }
    },
    recordValueByBinding: () => {
        return {
            perform:(props:any) => new CoreBinding().recordValue(new CoreBinding().obtainValue(props.into).bindingValue, props.value)
        }
    },
    removeValue: () => {
        return {
            perform:(props:any) => new CoreBinding().removeValue(props.into)
        }
    },
    recordArrayValue: () => {
        return {
            perform:(props:any) => new CoreBinding().recordArrayValue(props.into, props.key, props.keyValue, props.value)
        }
    },
    removeArrayValue: () => {
        return {
            perform:(props:any) => new CoreBinding().removeArrayValue(props.into, props.key, props.keyValue)
        }
    },
    recordSetterValue: () => {
        return {
            perform:(props:any) => {
                for(const item of getChildrenAttribute(props.template, ":root > Setter")){
                    new CoreBinding().recordValue(item.into,item.value)
                }
            }
        }
    },
    recordCollection: () => {
        return {
            perform: (props: any) => {
                getChildrenAttribute(props.template, ":root > Setter","lowercase").map((e: any) => {
                    const value = {} as { [name: string]: any };
                    Object.keys(e).map(key => value[key] = e[key]);
                    return new CoreBinding().recordArrayValue(props.into, 'key', value.key, value);
                })
            }
        }
    },



    recordParam: () => {
        return {
            perform:(props:any) => {
                const binding = new CoreBinding().obtainValue(props.value);
                let value = binding.bindingValue;
                value = typeof value === "string" ? value : JSON.stringify(value);
                const URI =  new URL(window.location.href);
                URI.searchParams.set(props.param,value);
                window.history.replaceState("","",URI.href);
            }
        }
    },




    setPaginator: () => {
        return {
            perform: (props: any) => {
                return Paginator.update(props);
            }
        }
    },
    setPrevPage: () => {
        return {
            perform:(props:any) => {
                const paging: {[name:string]:number} = {
                    page: new CoreBinding().obtainValue(props.page).bindingValue,
                };
                paging.page = --paging.page >= 0 ? paging.page : ++paging.page;
                new CoreBinding().recordValue(props.to, paging.page)
            }
        }
    },
    setNextPage: () => {
        return {
            perform:(props:any) => {
                const paging: {[name:string]:number} = {
                    page: new CoreBinding().obtainValue(props.page).bindingValue,
                    rows: new CoreBinding().obtainValue(props.rows).bindingValue,
                    count: new CoreBinding().obtainValue(props.count).bindingValue
                };
                paging.page = ++paging.page < Math.ceil(paging.count / paging.rows) ? paging.page : --paging.page;
                new CoreBinding().recordValue(props.to, paging.page)
            }
        }
    },


    goToHref: () => {
        return {
            perform: (props: any) => window.location.href = props.href
        }
    },

    goToSphere: () => {
        return {
            perform: (props: any) => {
                const folderId = new CoreBinding().obtainValue(props.folderId).bindingValue;
                const documentId = new CoreBinding().obtainValue(props.documentId).bindingValue;
                let url = 'https://courier-demo.esphere.ru/documents/' + folderId + '/' + documentId;
                window.open(url, '_blank');
                // window.location.href = url;
            }
        }
    },


    back: () => {
        return {
            perform:() => window.history.back()
        }
    },
    launchModal: () => {
        const perform =  (props: any) => {
            store.modal.method.launch(props)
        };
        return {
            perform:(props:any ) => perform(props)
        }
    },
    finishModal: () => {
        return {
            perform:() => store.modal.method.finish()
        }
    },
    finishFailure: () => {
        return {
            perform:() => store.failure.method.finish()
        }
    },

    ddd:()=>{
        return{
            perform:(props:any) => {
                window.history.pushState({},linkGenerator(props.template, props.to))
            }
        }

    },
    generateQueryParam: () => {
        return {
            perform:(props:any) => {
                const URI =  new URL(window.location.origin + window.location.pathname);
                const bindingPath = BindingParser.bindingParsingValue(props.value).bindingPath;
                const refers = new CoreBinding().refersValue(props.value);
                const getParamName = (key:string) => key.replace(bindingPath + ".","");
                const setParam = (param:string,value:string) => URI.searchParams.set(param,value);
                if(refers){
                    for ( const [key, value] of Object.entries(refers) ) {
                        const paramName = getParamName(key);
                        setParam(paramName,String(value));
                    }
                }
                new CoreBinding().recordValue(props.to,URI.pathname + URI.search);
            }
        }
    },
    revokeFilters: () => {
        return {
            perform:() => store.filters.method.revoke()
        }
    },
    downloadFromData: () => {
        return {
            perform:(props:any) => downloadFromData(props)
        }
    },
    download: () => {
        return {
            perform:(props:any) => asyncDownload(props)
        }
    },
};

export const facadeCoreCommand = {
    "Authorization": catalogCoreCommand.authorization,
    "AuthorizationSso": catalogCoreCommand.authorizationSso,
    "GoToCourier": catalogCoreCommand.goToCourier,
    "GoToLkk": catalogCoreCommand.goToLkk,
    "GoToHome": catalogCoreCommand.goToHome,
    "Logout": catalogCoreCommand.logout,
    "RecordBinding": catalogCoreCommand.recordBinding,

    "SetValue": catalogCoreCommand.setValue,
    "DelValue": catalogCoreCommand.delValue,



    "RecordValue": catalogCoreCommand.recordValue,
    "RemoveValue": catalogCoreCommand.removeValue,
    "RecordArrayValue": catalogCoreCommand.recordArrayValue,
    "RemoveArrayValue": catalogCoreCommand.removeArrayValue,
    "RecordSetterValue": catalogCoreCommand.recordSetterValue,
    "RecordCollection": catalogCoreCommand.recordCollection,
    "Request": catalogCoreCommand.request,

    "GoToHref": catalogCoreCommand.goToHref,
    "GoToSphere" : catalogCoreCommand.goToSphere,

    "CopyValue": catalogCoreCommand.copyValue,



    "RecordValueByBinding":catalogCoreCommand.recordValueByBinding,
    "Back": catalogCoreCommand.back,
    "RecordParam": catalogCoreCommand.recordParam,


    "SetPaginator": catalogCoreCommand.setPaginator,


    "SetPrevPage": catalogCoreCommand.setPrevPage,
    "SetNextPage": catalogCoreCommand.setNextPage,

    "LaunchModal": catalogCoreCommand.launchModal,
    "ChangeModal": catalogCoreCommand.launchModal,
    "FinishModal": catalogCoreCommand.finishModal,
    "FinishFailure": catalogCoreCommand.finishFailure,
    "GenerateQueryParam": catalogCoreCommand.generateQueryParam,

    "RevokeFilters": catalogCoreCommand.revokeFilters,
    "DownloadFromData": catalogCoreCommand.downloadFromData,
    "Download": catalogCoreCommand.download,
} as TFacadeCommand;

export const updateCoreCommand = ( facadeAppCommand: TFacadeCommand ) => {
    for(const key in facadeAppCommand ){
        if(facadeAppCommand.hasOwnProperty(key)){
            facadeCoreCommand[key] = facadeAppCommand[key];
        }
    }
};

export const obtainCoreCommand = (command: string) => {
    return facadeCoreCommand.hasOwnProperty(command) ? facadeCoreCommand[command]() : undefined;
};