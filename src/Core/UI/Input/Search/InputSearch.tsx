import React from 'react';
import Input from "../Input";
import {TInput} from "../TInput";

export const InputSearch = (props: TInput) =>
    <div data-component="InputSearchBox">
        <Input {...props} component="InputSearch" inputType={"text"}/>
        <svg role="img" width="32" height="32" viewBox="0 0 32 32">
            <g strokeWidth="1.25" strokeLinecap="round" fill="none">
                <circle cx="18" cy="14" r="6"/>
                <line x1="8" y1="24" x2="14" y2="18" strokeLinecap="butt"/>
            </g>
        </svg>
    </div>;

export default InputSearch;