export type TInputTime = {
    style?: string,
    placeholder?: string;
    srcInput: string;
    required?: "true";
    value: string

}