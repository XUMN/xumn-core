import React from 'react';
import Input from "../Input"
import {TInputTime} from "./TInputTime"

export const InputTime = (props: TInputTime) =>
    <Input {...props} component="InputTime" inputType={"time"}/>;

export default InputTime;