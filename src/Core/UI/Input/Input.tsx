import React, {FormEvent, FunctionComponent} from 'react';
import {observer} from "mobx-react-lite";
import {CoreBinding} from "../../CoreBinding";
import {conditionFacade} from "../../CoreCondition";
import {TInput} from "./TInput";

export const Input:FunctionComponent<TInput> = observer((props) => {

    return conditionFacade(props) ?
        <input data-component={props.component}
               data-style={props.style}
               autoComplete={props.autoComplete}
               type={props.inputType}
               placeholder={props.placeholder}
               required={!!props.required}
               maxLength={props.maxLength ? Number(props.maxLength) : undefined}
               pattern={props.pattern}
               value={new CoreBinding().outputValue(props.value).bindingValue}
               onChange={(e: FormEvent<HTMLInputElement>) => new CoreBinding().recordValue(props.value, e.currentTarget.value)}
        />
        : null
});

export default Input;