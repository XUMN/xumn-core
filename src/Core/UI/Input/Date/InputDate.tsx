import React from 'react';
import Input from "../Input"
import {TInputDate} from "./TInputDate"
//Default
export const InputDate = (props: TInputDate) =>
    <Input {...props} component="InputDate" inputType={"date"}/>;

export default InputDate;