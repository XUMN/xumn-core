export type TInput = {
    style?: string,
    inputType: string;
    component: string
    autoComplete?: string;
    placeholder?: string;
    required?: "true";
    value: string;
    maxLength?: string | undefined;
    pattern?: string | undefined;
}