import React, {FunctionComponent} from 'react';
import {observer} from "mobx-react-lite";
import {conditionFacade} from "../../../CoreCondition";
import {TColumnLayout} from "./TColumnLayout";

export const ColumnLayout: FunctionComponent<TColumnLayout> = observer((props) => {
        return conditionFacade(props) ?
            <div role="presentation" data-component="Column" data-style={props.style} children={props.children}/>
            : null
    }
);

export default ColumnLayout;