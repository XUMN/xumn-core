export type TMainLayout = {
    style?: string;
    template: string;
}
