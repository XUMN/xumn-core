import React, {FunctionComponent} from 'react';
import {CoreStyle} from "../../../CoreStyle";
import {TMainLayout} from "./TMainLayout";
import "./base.css";

export const MainLayout: FunctionComponent<TMainLayout> = (props) =>
    <main role="main"
          className={props.style}
          style={CoreStyle.getStyle("Main",props.template,':root > Main\\.Style')}
          children={props.children}/>;

export default MainLayout;