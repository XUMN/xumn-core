import React,{FunctionComponent} from "react";
import {observer} from "mobx-react-lite";
import {conditionFacade} from "../../../../CoreCondition";
import {TGridCell} from "./TGridCell";
import {CoreStyle} from "../../../../CoreStyle";
import "./base.css";

export const GridCell: FunctionComponent<TGridCell> = observer((props) => {
    return conditionFacade(props) ?
        <div role="gridcell"
             className={ props.style }
             style={CoreStyle.getStyle("GridCell",props.template,':root > GridCell\\.Style')}
             children={props.children}/>
        : null
});

export default GridCell;