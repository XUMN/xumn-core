import React,{FunctionComponent} from "react";
import {observer} from "mobx-react-lite";
import {conditionFacade} from "../../../CoreCondition";
import {CoreStyle} from "../../../CoreStyle";
import {TGridLayout} from "./TGridLayout";
import "./base.css";

export const GridLayout: FunctionComponent<TGridLayout> = observer((props) => {

    return conditionFacade(props) ?
        <div role="grid"
             className={props.style}
             style={CoreStyle.getStyle("Grid",props.template,':root > Grid\\.Style')}
             children={props.children}/>
        : null
});

export default GridLayout;