export type TGridLayout = {
    style?: string;
    conditionType?: string;
    conditionValue?: string;
    template:string;
}