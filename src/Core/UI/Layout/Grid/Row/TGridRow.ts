export type TGridRow = {
    style?: string;
    conditionType?: string;
    conditionValue?: string;
    gridColumn?: string;
    template: string;
}