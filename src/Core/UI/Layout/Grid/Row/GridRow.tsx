import React,{FunctionComponent} from "react";
import {observer} from "mobx-react-lite";
import {conditionFacade} from "../../../../CoreCondition";
import {TGridRow} from "./TGridRow";
import {CoreStyle} from "../../../../CoreStyle";
import "./base.css";

export const GridRow: FunctionComponent<TGridRow> = observer((props) => {
    return conditionFacade(props) ?
        <div data-role="grid-row"
             role="row"
             className={ props.style }
             style={CoreStyle.getStyle("GridRow",props.template,':root > GridRow\\.Style')}
             children={props.children}/>
        : null
});

export default GridRow;