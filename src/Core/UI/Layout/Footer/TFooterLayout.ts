export type TFooterLayout = {
    style?: string;
    template: string;
}