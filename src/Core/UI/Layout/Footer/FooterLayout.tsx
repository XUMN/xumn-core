import React, {FunctionComponent} from 'react';
import {TFooterLayout} from "./TFooterLayout";
import {CoreStyle} from "../../../CoreStyle";
import "./base.css";

export const FooterLayout: FunctionComponent<TFooterLayout> = (props) =>
    <footer role="contentinfo"
            className={props.style}
            style={CoreStyle.getStyle("Footer",props.template,':root > Footer\\.Style')}
            children={props.children}/>;

export default FooterLayout;
