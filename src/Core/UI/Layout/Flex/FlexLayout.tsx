import React, {FunctionComponent} from 'react';
import {observer} from "mobx-react-lite";
import {conditionFacade} from "../../../CoreCondition";
import {CoreStyle} from "../../../CoreStyle";
import {TFlexLayout} from "./TFlexLayout";
import "./base.css";

export const FlexLayout: FunctionComponent<TFlexLayout> = observer((props) => {
        return conditionFacade(props) ?
            <div data-role="flex"
                 className={props.style}
                 style={CoreStyle.getStyle("Flex",props.template,':root > Flex\\.Style')}
                 children={props.children}/>
            : null
    }
);

export default FlexLayout;