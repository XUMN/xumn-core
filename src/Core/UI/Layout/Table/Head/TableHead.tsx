import React,{FunctionComponent} from "react";
import {observer} from "mobx-react-lite";
import {CoreStyle} from "../../../../CoreStyle";
import {conditionFacade} from "../../../../CoreCondition";
import {TTableHead} from "./TTableHead";
import "./base.css";

export const TableHead: FunctionComponent<TTableHead> = observer((props) => {
    return conditionFacade(props) ?
        <thead className={props.style}
               style={CoreStyle.getStyle("TableHead",props.template, ':root > Style')}
               children={props.children}/>
        :
        null
});

export default TableHead;