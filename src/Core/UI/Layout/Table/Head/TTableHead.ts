export type TTableHead = {
    style?: string;
    conditionType?: string;
    conditionValue?: string;
    template: string;
}