import React,{FunctionComponent} from "react";
import {observer} from "mobx-react-lite";
import {CoreStyle} from "../../../../CoreStyle";
import {conditionFacade} from "../../../../CoreCondition";
import {TTableFoot} from "./TTableFoot";
import "./base.css";

export const TableFoot: FunctionComponent<TTableFoot> = observer((props) => {
    return conditionFacade(props) ?
        <tfoot className={ props.style }
               style={CoreStyle.getStyle("TableFoot",props.template,':root > Style')}
               children={props.children}/>
        :
        null
});

export default TableFoot;