export type TTableFoot = {
    style?: string;
    conditionType?: string;
    conditionValue?: string;
    template: string;
}