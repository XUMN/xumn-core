export type TTableRow = {
    style?: string;
    conditionType?: string;
    conditionValue?: string;
    template: string,
}