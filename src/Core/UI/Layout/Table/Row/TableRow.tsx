import React,{FunctionComponent} from "react";
import {observer} from "mobx-react-lite";
import {conditionFacade} from "../../../../CoreCondition";
import {CoreStyle} from "../../../../CoreStyle";
import {TTableRow} from "./TTableRow";
import "./base.css";

export const TableRow: FunctionComponent<TTableRow> = observer((props) => {
    return conditionFacade(props) ?
        <tr role="row"
            className={ props.style }
            style={CoreStyle.getStyle("Row",props.template,':root > Style')}
            children={props.children}/>
        : null
});

export default TableRow;