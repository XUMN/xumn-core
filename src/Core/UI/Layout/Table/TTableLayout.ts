export type TTableLayout = {
    style?: string;
    conditionType?: string;
    conditionValue?: string;
    template: string;
}