import React, {FunctionComponent} from "react";
import {observer} from "mobx-react-lite";
import {CoreStyle} from "../../../../CoreStyle";
import {conditionFacade} from "../../../../CoreCondition";
import {TTableBody} from "./TTableBody";
import "./base.css";

export const TableBody: FunctionComponent<TTableBody> = observer((props) => {
    return conditionFacade(props) ?
        <tbody className={props.style}
               style={CoreStyle.getStyle("TableBody",props.template, ':root > Style')}
               children={props.children}/>
        : null
});

export default TableBody;