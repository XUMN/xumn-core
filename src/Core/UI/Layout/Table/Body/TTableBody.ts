export type TTableBody= {
    style?: string;
    valueType?: string;
    path?: string;
    conditionType?: string;
    conditionValue?: string;
    template: string;
}