export type TTableBodyLink= {
    style?: string,
    valueType?:string,
    to:string,
    template:string,
}