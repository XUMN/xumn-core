import React,{FunctionComponent} from "react";
import {observer} from "mobx-react-lite";
import {CoreStyle} from "../../../CoreStyle";
import {conditionFacade} from "../../../CoreCondition";
import {TTableLayout} from "./TTableLayout";
import "./base.css";

export const TableLayout: FunctionComponent<TTableLayout> = observer((props) => {
    return conditionFacade(props) ?
        <table role="table"
               className={props.style}
               style={CoreStyle.getStyle("Table",props.template, ':root > Style')}
               children={props.children}/>
        :
        null;
});

export default TableLayout;