import React,{FunctionComponent} from "react";
import {observer} from "mobx-react-lite";
import {CoreStyle} from "../../../../CoreStyle";
import {conditionFacade} from "../../../../CoreCondition";
import {TTableCell} from "./TTableCell";
import "./base.css";

export const TableCell: FunctionComponent<TTableCell> = observer((props) => {
    return conditionFacade(props) ?
        <td role="cell"
            className={props.style}
            style={CoreStyle.getStyle("Cell",props.template, ':root > Style')}
            width={props.width}
            align={props.align}
            colSpan={props.colSpan ? Number(props.colSpan) : undefined}
            rowSpan={props.colSpan ? Number(props.rowSpan) : undefined}
            children={props.children}/>
        :
        null
});

export default TableCell;