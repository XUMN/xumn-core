export type TTableCell = {
    style?: string;
    width?: string;
    colSpan?: string;
    rowSpan?: string;
    align?: "left"| "center" | "right";
    conditionType?: string;
    conditionValue?: string;
    template: string;
}