import React, {FunctionComponent} from "react";
import {observer} from "mobx-react-lite";
import {conditionFacade} from "../../../CoreCondition";
import {TGroup} from "./TGroup";
import {CoreStyle} from "../../../CoreStyle";

export const Group: FunctionComponent<TGroup> = observer((props) => {
    return conditionFacade(props) ?
        <div role="group"
             className={props.style}
             style={CoreStyle.getStyle("Group",props.template,':root > Group\\.Style')}
             children={props.children}/>
        : null
});

export default Group;