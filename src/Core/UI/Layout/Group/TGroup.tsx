export type TGroup = {
    style?: string;
    conditionType?: string;
    conditionValue?: string;
    template: string;
}