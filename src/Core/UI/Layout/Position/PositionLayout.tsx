import React, {FunctionComponent} from 'react';
import {TPositionLayout} from "./TPositionLayout";

export const PositionLayout: FunctionComponent<TPositionLayout> = (props) =>
    <div role="presentation" data-component="PositionLayout" data-style={props.style} children={props.children}/>;

export default PositionLayout;