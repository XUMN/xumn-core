import React, {FunctionComponent} from 'react';
import {TPage} from "./TPage";

const Page: FunctionComponent<TPage> = (props) =>
    <article role="region"
             data-style={props.style}
             children={props.children}/>;

export default Page;