import React, {FunctionComponent} from 'react';
import {CoreStyle} from "../../../CoreStyle";
import {THeaderLayout} from "./THeaderLayout";
import "./base.css";

const HeaderLayout: FunctionComponent<THeaderLayout> = (props) => {
    return <header role="complementary"
                   className={props.style}
                   style={CoreStyle.getStyle("Header",props.template,':root > Header\\.Style')}
                   children={props.children}/>
}

export default HeaderLayout;