export type THeaderLayout = {
    style?: string;
    template: string;
}