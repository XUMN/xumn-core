import React, {FunctionComponent} from 'react';
import {TStackLayout} from "./TStackLayout";

export const StackLayout: FunctionComponent<TStackLayout> = (props) =>
    <div role="presentation" data-component="StackLayout" data-style={props.style} children={props.children}/>;

export default StackLayout;