export type TNavigationLayout = {
    style?: string;
    conditionType?: string;
    conditionValue?: string;
    template: string;
}