import React, {FunctionComponent} from 'react';
import {observer} from "mobx-react-lite";
import {conditionFacade} from "../../../CoreCondition";
import {CoreStyle} from "../../../CoreStyle";
import {TNavigationLayout} from "./TNavigationLayout";
import "./base.css";

export const NavigationLayout: FunctionComponent<TNavigationLayout> = observer((props) => {
    return conditionFacade(props) ?
        <nav role="navigation"
             className={props.style}
             style={CoreStyle.getStyle("Navigation",props.template,':root > Navigation\\.Style')}
             children={props.children}/>
        : null
});

export default NavigationLayout;