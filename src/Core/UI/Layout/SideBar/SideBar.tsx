import React, {FunctionComponent} from 'react';
import {CoreStyle} from "../../../CoreStyle";
import {TSideBar} from "./TSideBar";
import "./base.css";

const SideBar: FunctionComponent<TSideBar> = (props) =>
    <aside role="directory"
           className={props.style}
           style={CoreStyle.getStyle("SideBar",props.template,':root > SideBar\\.Style')}
           children={props.children}/>;

export default SideBar;
