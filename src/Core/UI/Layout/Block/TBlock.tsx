export type TBlock = {
    style?: string;
    conditionType?: string;
    conditionValue?: string;
    template: string;
}