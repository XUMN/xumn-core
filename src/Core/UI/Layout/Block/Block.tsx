import React, {FunctionComponent} from "react";
import {observer} from "mobx-react-lite";
import {conditionFacade} from "../../../CoreCondition";
import {TBlock} from "./TBlock";
import {CoreStyle} from "../../../CoreStyle";

export const Block: FunctionComponent<TBlock> = observer((props) => {
    return conditionFacade(props) ?
        <div role="presentation"
             className={props.style}
             style={CoreStyle.getStyle("Block",props.template,':root > Block\\.Style')}
             children={props.children}/>
        : null
});

export default Block;