export type TTextArea = {
    style?: string,
    autoComplete?:string;
    placeholder?:string;
    required?:"true";
    value:string
}