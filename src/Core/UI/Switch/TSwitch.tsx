export type TSwitch = {
    style?: string,
    value: any;
    conditionType?: string;
    conditionValue?: string;
    template: string;
}