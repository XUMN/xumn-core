import {FunctionComponent,createElement} from 'react';
import {observer} from "mobx-react-lite";
import {ComponentFactory} from "../../Factory/Component/ComponentFactory";
import {TSwitch} from "./TSwitch";

class SwitchTemplateBuilder{
    static implementCase(element:Element,switchValue:string){
        const list = element.querySelectorAll("Case");
        for(const item of list){
            const value = item.getAttribute("Value");
            if(!value){continue}
            const fragment = new DOMParser().parseFromString("<Fragment><Condition><Insert/></Condition></Fragment>", "text/xml");
            fragment.querySelector("Condition Insert")?.replaceWith(...item.children);
            fragment.querySelector("Condition")?.setAttribute("ConditionType","IsEqual")
            fragment.querySelector("Condition")?.setAttribute("ConditionValue",switchValue + value);
            item.replaceWith(...fragment.documentElement.children);
        }
    }
    static getDefaultCaseValue = (element:Element,switchValue:string)=>{
        const list = element.querySelectorAll("Case");
        let value = switchValue;
        for(const item of list){
            const caseValue = item.getAttribute("Value");
            if(!caseValue){continue}
            value += caseValue;
        }
        return value;
    }
    static implementDefaultCase(element:Element,value:string){
        const item = element.querySelector("Default");
        if(item){
            const fragment = new DOMParser().parseFromString("<Fragment><Condition><Insert/></Condition></Fragment>", "text/xml");
            fragment.querySelector("Condition Insert")?.replaceWith(...item.children);
            fragment.querySelector("Condition")?.setAttribute("ConditionType","NotEqualAny")
            fragment.querySelector("Condition")?.setAttribute("ConditionValue",value)
            item.replaceWith(...fragment.documentElement.children);
        }
    }
    static implement (template:string){
        const switchElement = new DOMParser().parseFromString(template, "text/xml").documentElement;
        const switchValue = switchElement.getAttribute("Value");
        if(!switchValue) return String();
        const value = SwitchTemplateBuilder.getDefaultCaseValue(switchElement,switchValue);
        SwitchTemplateBuilder.implementCase(switchElement,switchValue);
        SwitchTemplateBuilder.implementDefaultCase(switchElement,value);
        const result = new DOMParser().parseFromString("<Template><Slot/></Template>", "text/xml");
        result.documentElement.querySelector("Slot")?.replaceWith(...switchElement.childNodes);
        return new XMLSerializer().serializeToString(result);
    }
}


export const SwitchComponent: FunctionComponent<TSwitch> = observer((props) => {
    const template = SwitchTemplateBuilder.implement(props.template);
    return createElement( ComponentFactory, {template: template});
});

export default SwitchComponent;