import React,{FunctionComponent} from "react";
import {TStatus} from "./TStatus";
import {CoreBinding} from "../../CoreBinding";
import {useObserver} from "mobx-react-lite";

const Status: FunctionComponent<TStatus> = (props) =>
    useObserver(() =>
        <div role="presentation"
             data-component="Status"
             data-style={props.style}
             data-status-code={new CoreBinding().obtainValue(props.value).bindingValue}
             children={props.children}/>
    );

export default Status;