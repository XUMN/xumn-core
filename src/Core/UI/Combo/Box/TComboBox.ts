export type TComboBox = {
    style?: string,
    placeholder: string;
    loop:string;
    visibleItemValue:string;
    installItemValue:string;
    into:string;
    value:string;
    template:string;
}