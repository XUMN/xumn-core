import React, {FunctionComponent} from 'react';
import {ResourceFactory} from "../../Factory/ResourceFactory";
import {TResourceDictionary} from "./TResourceDictionary";

const ResourceDictionary: FunctionComponent<TResourceDictionary> = (props) =>
    <style type="text/css" children={ResourceFactory.launch({template:props.template})}/>;

export default ResourceDictionary;