import React from "react";

const Icon = () =>
    <svg role="img" width="16" height="16" viewBox="0 0 16 16">
        <g strokeWidth="2" strokeLinecap="round">
            <line x1="2" y1="8" x2="6" y2="12" strokeLinecap="round"/>
            <line x1="6" y1="12" x2="16" y2="0" strokeLinecap="round"/>
        </g>
    </svg>;

export default Icon;