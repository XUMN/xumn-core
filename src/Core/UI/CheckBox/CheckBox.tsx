import React, {FunctionComponent} from "react";
import {observer} from "mobx-react-lite";
import {CoreBinding} from "../../CoreBinding";
import {BindingParser} from "../../Util/BindingParser";
import {getChildrenAttribute} from "../../Util/NodeXML";
import {conditionCatalog} from "../../CoreCondition";
import Icon from "./Icon";
import {TCheckBox} from "./TCheckBox";
import "./data-checkbox.css";


const bindingToggleValue = (template: string) => {
    let exist:boolean = true;
    getChildrenAttribute(template, ":root > Setter").map( (e:any) => {
        if(BindingParser.bindingParsingValue(e.into).recordProperty === "[]" ){
            if(conditionCatalog["NotEmptyAll"]({value:e.into})){
                exist = new CoreBinding().obtainValue(e.into).bindingValue.some( (item:any) => item === e.value ) ? exist : false;
            } else {
                exist = false;
            }
        } else {
            exist = new CoreBinding().obtainValue(e.into).bindingValue === undefined ? false : exist;
        }
        return exist;
    });
    return exist;
};

const CheckBox:FunctionComponent<TCheckBox> = observer((props) => {
    const handleClick = () => {
        if(bindingToggleValue( props.template )){
            getChildrenAttribute(props.template, ":root > Setter").map( (e:any) => new CoreBinding().removeValue(e.into,e.value));
        } else {
            getChildrenAttribute(props.template, ":root > Setter").map( (e:any) => new CoreBinding().recordValue(e.into,e.value));
        }
    };
    return (
        <button type={"button"}
                role={"checkbox"}
                data-component={"CheckBox"}
                aria-checked={bindingToggleValue(props.template)}
                onClick={handleClick}>
            <div role={"presentation"}><Icon /></div>
            {props.children}
        </button>
    )
});

export default CheckBox;