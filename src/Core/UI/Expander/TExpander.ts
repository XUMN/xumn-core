export type TExpander = {
    style?: string,
    value: string;
    expanded?: string;
    conditionType?: string;
    conditionValue?: string;
}