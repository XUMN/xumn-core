import React, {FunctionComponent} from 'react';

type ExpanderType = {
    open:boolean
}

const ExpanderGroup: FunctionComponent<ExpanderType> = (props) =>
    props.open ? <div role="group" children={props.children}/> : null;

export default ExpanderGroup;
