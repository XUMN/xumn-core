import React, {FunctionComponent} from 'react';
import {useLocalStore, useObserver} from "mobx-react-lite";
import ExpanderGroup from "./ExpanderGroup";
import {conditionFacade} from "../../CoreCondition";
import {TExpander} from "./TExpander";
import "./expander.css";
import {CoreBinding} from "../../CoreBinding";

const Expander: FunctionComponent<TExpander> = (props) => {
    const expanded = useLocalStore(() => ({ state: props.expanded === "true" }));
    return useObserver(() => conditionFacade(props) ?
        <div role="tablist"
             data-component="Expander"
             data-style={props.style}
             className="expander"
             aria-expanded={expanded.state}>
            <dl role="tab" onClick={() => (expanded.state = !expanded.state)}>
                <dt role="term">{new CoreBinding().outputValue(props.value).bindingValue}</dt>
                <dd role="presentation">
                    <svg role="img" width="12" height="16" viewBox="0 0 12 16">
                        <g id="arrow" strokeWidth="1" strokeLinecap="round" fill="none">
                            <line x1="4" y1="4" x2="8" y2="8"/>
                            <line x1="4" y1="12" x2="8" y2="8"/>
                        </g>
                    </svg>
                </dd>
            </dl>
            <ExpanderGroup open={expanded.state} children={props.children}/>
        </div>
        : null
    )
};

export default Expander;