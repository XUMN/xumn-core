import React,{FunctionComponent} from "react";

type TUse={
    href?:string;
    height?:string;
    width?:string;
}
const Use:FunctionComponent<TUse> = (props) =>
    <use href={props.href}
         height={props.height}
         width={props.width}
         children={props.children}/>;

export default Use;