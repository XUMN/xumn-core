import React,{FunctionComponent} from "react";

type TRect={
    rx?:string;
    ry?:string;
    stroke?:string;
    strokeWidth?:string;
    fill?:string;
    width?:string;
    height?:string;
}

const Rect:FunctionComponent<TRect> = (props) =>
    <rect width={props.width}
            height={props.height}
            rx={props.rx}
            ry={props.ry}
            stroke={props.stroke}
            strokeWidth={props.strokeWidth}
            fill={props.fill}
            children={props.children}/>;

export default Rect;