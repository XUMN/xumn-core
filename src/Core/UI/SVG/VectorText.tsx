import React,{FunctionComponent} from "react";

type TVectorText ={
    x?:string;
    y?:string;
    fontFamily?:string;
    fontSize?:string;
    dominantBaseline?:string;
    fontWeight?:string;
    fill?:string;
    value?:string;
}
const VectorText:FunctionComponent<TVectorText> = (props) =>
    <text fontFamily={props.fontFamily}
          fontSize={props.fontSize}
          dominantBaseline={props.dominantBaseline}
          fontWeight={props.fontWeight}
          x={props.x}
          y={props.y}
          fill={props.fill}
          children={props.value}/>;

export default VectorText;