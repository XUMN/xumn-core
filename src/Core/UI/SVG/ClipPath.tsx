import React,{FunctionComponent} from "react";

type TClipPath ={
    id?:string
}
const ClipPath:FunctionComponent<TClipPath> = (props) =>
    <clipPath id={props.id} children={props.children}/>;

export default ClipPath;