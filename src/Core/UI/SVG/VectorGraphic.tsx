import React,{FunctionComponent} from "react";

type TVectorGraphic={
    style?: string,
    width:string;
    height:string;
    viewBox:string;
    hidden?:"false" | "true";
}
const VectorGraphic:FunctionComponent<TVectorGraphic> = (props) =>
    <svg role="img"
         data-component="VectorGraphic"
         data-style={props.style}
         aria-hidden={props.hidden}
         width={props.width}
         height={props.height}
         viewBox={props.viewBox}
         children={props.children}/>;

export default VectorGraphic;