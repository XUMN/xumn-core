import React,{FunctionComponent} from "react";

type TLine={
    x1?:string;
    x2?:string;
    y1?:string;
    y2?:string;
    mask?:string;
    stroke?:string;
    strokeWidth?:string;
    strokeDasharray?:string;
    strokeLinecap?:"butt" | "round" | "square";
}
const Line:FunctionComponent<TLine> = (props) =>
    <line x1={props.x1}
          x2={props.x2}
          y1={props.y1}
          y2={props.y2}
          mask={props.mask}
          stroke={props.stroke}
          strokeWidth={props.strokeWidth}
          strokeLinecap={props.strokeLinecap}
          strokeDasharray={props.strokeDasharray}
          children={props.children}/>;

export default Line;