import React,{FunctionComponent} from "react";

type TMask={
    id?:string;
    x?:string;
    y?:string;
    width?:string;
    height?:string;
}
const Mask:FunctionComponent<TMask> = (props) =>
    <mask id={props.id}
          x={props.x}
          y={props.y}
          width={props.width}
          height={props.height}
          children={props.children}/>;

export default Mask;

