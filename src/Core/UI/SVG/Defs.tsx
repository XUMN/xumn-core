import React,{FunctionComponent} from "react";

const Defs:FunctionComponent = (props) =>
    <defs children={props.children}/>;

export default Defs;