import React,{FunctionComponent} from "react";

type TGroup ={
    strokeWidth?:string;
    fill?:string;
    strokeLinecap?:"butt" | "round" | "square";
    strokeDasharray?:string;
    stroke?:string
    clipPath?:string
}

const Group:FunctionComponent<TGroup> = (props) =>
    <g strokeWidth={props.strokeWidth}
       fill={props.fill}
       stroke={props.stroke}
       strokeLinecap={props.strokeLinecap}
       strokeDasharray={props.strokeDasharray}
       clipPath={props.clipPath}
       children={props.children}
    />;

export default Group;