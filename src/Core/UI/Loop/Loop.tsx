import {createElement, FunctionComponent, useEffect, useState} from "react";
import {observer} from "mobx-react-lite";
import {ComponentFactory} from "../../Factory/Component/ComponentFactory";
import {CoreBinding} from "../../CoreBinding";

import {
    parseXML,
    replaceRootElement,
    recordPartBindingAttribute
} from "../../Util/NodeXML";

import {TLoop} from "./TLoop";

const getTemplate = (template:string,binding:any) => {
    let pattern: string | null = null;
    const bindingSource = binding.bindingSource;
    const bindingPath = binding.bindingPath;
    const data = binding.bindingValue;
    if(bindingSource) {
        if (bindingPath) {
            if (Array.isArray(data)) {
                const root = parseXML("<Template/>");
                for(let i = 0; i < data.length; i++){
                    const fragment = replaceRootElement(template).documentElement;
                    const property = {
                        bindingSource: bindingSource,
                        bindingPath: bindingPath,
                        bindingIndex: i,
                        element: fragment,
                        attribute: "",
                    }
                    property.attribute = "Value";
                    recordPartBindingAttribute(property);
                    property.attribute = "Label";
                    recordPartBindingAttribute(property);
                    property.attribute = "ConditionValue";
                    recordPartBindingAttribute(property);
                    property.attribute = "into";
                    recordPartBindingAttribute(property);
                    property.attribute = "To";
                    recordPartBindingAttribute(property);
                    root.documentElement.append(fragment);
                }
                pattern = new XMLSerializer().serializeToString(root);
            }
        }
    }
    return pattern;
};

export const Loop:FunctionComponent<TLoop> = observer((props) => {
    const {mixinProps} = props;
    const binding = new CoreBinding().obtainValue(props.value);
    const [value, setValue] = useState(binding);

    useEffect(() => setValue(binding), [binding]);

    const template = getTemplate(props.template,value);

    return template ? createElement( ComponentFactory, {template: template,mixinProps:mixinProps}) : null;

});


export default Loop;