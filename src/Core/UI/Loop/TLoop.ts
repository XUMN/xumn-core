export type TLoop = {
    value: string;
    template:string;
    conditionType?: string;
    conditionValue?: string;
    mixinProps?: any;
}