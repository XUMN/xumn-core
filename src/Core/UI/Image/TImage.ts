export type TImage = {
    style?: string,
    source?: string;
    useImg?: string;
    alt?: string;
    width?: string;
    height?: string;
    template: string;
}