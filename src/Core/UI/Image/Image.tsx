import React,{FunctionComponent} from "react";
import {observer} from "mobx-react-lite";
import {conditionFacade} from "../../CoreCondition";
import {TImage} from "./TImage";
import {CoreStyle} from "../../CoreStyle";
import "./base.css";

export const Image: FunctionComponent<TImage> = observer((props) => {
    return conditionFacade(props) ?
        <img data-role="image"
             className={props.style}
             style={CoreStyle.getStyle("Image",props.template,':root > Image\\.Style')}
             alt={props.alt}
             src={props.source}
             width={props.width}
             height={props.height}/>
        : null
});


export default Image;