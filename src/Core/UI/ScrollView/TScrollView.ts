export type TScrollView = {
    style:string;
    orientation: "vertical" | "horizontal"
}