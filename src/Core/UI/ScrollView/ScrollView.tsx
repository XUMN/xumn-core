import React, {FunctionComponent} from 'react';
import {TScrollView} from "./TScrollView";

const ScrollView: FunctionComponent<TScrollView> = (props) =>
    <div role="presentation" data-component="ScrollView" data-orientation={props.orientation} children={props.children}/>;

export default ScrollView;
