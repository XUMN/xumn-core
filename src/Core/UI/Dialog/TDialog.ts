export type TDialog = {
    style?: string,
    open: string,
    conditionType?: string;
    conditionValue?: string;
    template:string;
}