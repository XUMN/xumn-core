import React, {FunctionComponent} from 'react';
import {observer} from "mobx-react-lite";
import {TDialog} from "./TDialog";
import {CoreStyle} from "../../CoreStyle";
import {CoreBinding} from "../../CoreBinding";
import "./base.css";

export const Dialog: FunctionComponent<TDialog> = observer((props) => {
    return (
        <dialog open={new CoreBinding().obtainValue(props.open).bindingValue}
                className={props.style}
                style={CoreStyle.getStyle("Dialog",props.template,':root > Dialog\\.Style')}
                children={props.children}/>
    )
});

export default Dialog;