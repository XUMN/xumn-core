import React from "react";

const Separator = () =>
    <svg role="img" width="24" height="24" viewBox="0 0 24 24">
        <g id="arrow" strokeWidth="2" strokeLinecap="round" fill="#346" stroke="none">
            <circle cx="8" cy="12" r="1"/>
            <circle cx="12" cy="12" r="1"/>
            <circle cx="16" cy="12" r="1"/>
        </g>
    </svg>;

export default Separator;