import React, {FunctionComponent} from "react";
import {useObserver} from "mobx-react-lite";
import {CoreBinding} from "../../CoreBinding";
import Separator from "./Separator";
import PageButton from "./PageButton";
import "./paginator.css";

type TPaginator ={
    step:string;
    count: string;
    rows: string;
    page: string;
    param:string;
    command:string;
}

export const Paginator: FunctionComponent<TPaginator> = (props) =>{
    return useObserver(() => {
            const step = Number(props.step);
            const count = Number(new CoreBinding().obtainValue(props.count).bindingValue);
            const rows = Number(new CoreBinding().obtainValue(props.rows).bindingValue);
            const page = Number(new CoreBinding().obtainValue(props.page).bindingValue);

            let total = Math.ceil(count/rows);
            --total;

            const pageBtn =[];

            let n = step;
            let i = page - step;
            let key = 0;

            // сохранение изначального диапазона
            if(page + step > total ){
                n += page + step - total;
                i -= page + step - total;
            }

            // перыдущие текущей страницы
            while(n--){
                if(i >= 0){
                    pageBtn.push(<PageButton key={key++} {...props} value={i}/>);
                }
                ++i;
            }

            // текущая страницы
            pageBtn.push(<PageButton key={key++} {...props} value={page} current="page"/>);

            n = step;
            i = page;

            // сохранение изначального диапазона
            if(page - step < 0 ){
                n += Math.abs(page - step  );
            }

            // последующие текущей страницы
            while (n--){
                ++i;
                if( i <= total ){
                    pageBtn.push(<PageButton key={key++} {...props} value={i}/>);
                }
            }

            return (
                <div data-component="Paginator">
                    {page > step &&  step + step < total ? <PageButton {...props} value={0}/> : null}
                    {page > step + 1 &&  step + step < total ? <Separator/> : null}
                    {pageBtn}
                    {page + step + 1 < total && step + step < total ? <Separator/>  : null}
                    {page + step < total && step + step < total ? <PageButton {...props} value={total}/> : null}
                </div>
            )
        }
    );
};

export default Paginator;