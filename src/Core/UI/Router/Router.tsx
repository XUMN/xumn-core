import React, {FunctionComponent} from 'react';
import {RouterController} from "./RouterController";
import {observer} from "mobx-react-lite";
import {store} from "../../CoreStore";
import {TRouter} from "./TRouter";

export const Router:FunctionComponent<TRouter> = observer((props) => {
    new RouterController( props.authentication, props.authenticationSso, props.unknown, props.template );
    return store.failure.method.status() ? null : <>{props.children}</>;
});

export default Router;