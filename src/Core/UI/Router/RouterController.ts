import {mutationHistory, replaceHistory} from "./RouterCommand";
import {getChildrenAttribute} from "../../Util/NodeXML";
import {store} from "../../CoreStore";

export class RouterController {
    protected readonly authentication: string;
    protected readonly authenticationSso: string;
    protected readonly unknown: string;
    protected readonly route: Array <any>;
    protected readonly redirect: Array <any>;
    protected readonly overlook: Array <any>;
    protected readonly protection: Array <any>;


    constructor(authentication: string, authenticationSso: string, unknown: string, template: string) {
        this.authentication = authentication;
        this.authenticationSso = authenticationSso;
        this.unknown = unknown;
        this.route = getChildrenAttribute(template, "Route" );
        this.redirect = getChildrenAttribute(template, "Redirect" );
        this.overlook= getChildrenAttribute(template, "Overlook");
        this.protection = getChildrenAttribute(template, "Protection Route");

        this.historyUpdate();
        window.addEventListener("MutationHistory", () => this.historyUpdate());
        window.addEventListener("SessionMutation", () => this.historyUpdate());
        window.addEventListener("popstate", () => mutationHistory());
    }
    protected historyUpdate() {
        const redirect = this.redirect.find(e => e.pathname === window.location.pathname);
        // Проверка на редирект
        if(store.failure.method.status()){
            let gotoPath = window.location.protocol + "//" + window.location.host + "/failure/" + store.failure.props.code;
            store.failure.method.finish()

            replaceHistory(gotoPath);
            //window.location.replace(gotoPath );

        }
        if(redirect){
            let gotoPath = window.location.protocol + "//" + window.location.host;
            window.location.replace(gotoPath + redirect.to);
        } else {
            // Агригация всех маршрутов из шаблона
            if (this.route.some(e => e.pathname === window.location.pathname)) {
                // Проверка авторизации
                if ( store.session.method.status()) {
                    // Замена пути если произошла авторизация и текущий путь соответсвует авторизации
                    if (window.location.pathname === this.authenticationSso || window.location.pathname === this.authentication) {
                        let gotoPath = window.location.protocol + "//" + window.location.host;
                        window.location.replace(gotoPath + "/");
                    }
                } else {
                    if (window.location.pathname !== this.authentication) {
                        // Замена пути если пользователь не авторизован и находится на защищёном маршрует
                        if (this.protection.some(e => e.pathname === window.location.pathname)) {
                            let gotoPath = window.location.protocol + "//" + window.location.host;
                            window.location.replace(gotoPath + "/auth/login");
                        }
                    }
                }
            } else {
                // Замена пути если маршрут не существует и не игнорируемый
                if(!this.overlook.some(e => window.location.pathname.includes(e.pathname ))){
                    window.location.replace(this.unknown);
                }
            }
        }
    }
}