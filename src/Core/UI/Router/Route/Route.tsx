import {createElement,useEffect,useState} from 'react';
import {TemplateProvider} from "../../Provider/Template/TemplateProvider";
import {TRoute} from "./TRoute";

export const Route = (props:TRoute) =>{
    const [pathname, setPathname] = useState(window.location.pathname);
    const [renovate, setRenovate] = useState(window.location.href);
    useEffect(() => {
        const update = () => {
            setPathname(window.location.pathname);
            setRenovate(window.location.search);
        };
        window.addEventListener("MutationHistory", update,false);
        return () =>  window.removeEventListener("MutationHistory", update,false);
    }, []);
    return props.pathname === pathname ? createElement(TemplateProvider, {...props, renovate: renovate}) : null
};

export default Route;