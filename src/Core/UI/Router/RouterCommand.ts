
export const mutationHistory = () => {
    const event = new Event("MutationHistory", { "bubbles": true, "cancelable": false });
    document.dispatchEvent( event );
};

export const installHistory = (URI:string) => {
    //console.log("install history: " + URI);
    window.history.pushState("","",URI);
    mutationHistory();
};

export const replaceHistory = (URI:string) => {
    //console.log("replace history: " + URI);
    window.history.replaceState("","",URI);
    mutationHistory();
};