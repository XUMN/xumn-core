import React, {FunctionComponent} from 'react';
import {observer} from "mobx-react-lite";
import {store} from "../../CoreStore";

export const Protection: FunctionComponent = observer((props) => {
    return store.session.method.status() ? <>{props.children}</> : null
});

export default Protection;