import React, {FunctionComponent, Fragment, useEffect, useState} from "react";
import {TemplateContent} from "../../Service/Content/ContentService";
import {observer} from "mobx-react-lite";
import {useLocation} from "react-router-dom";

type TTemplate = {
    template:string;
}

export const Template:FunctionComponent<TTemplate> = observer((props) => {
    const {template} = props;
    const {pathname,search} = useLocation();
    const trigger = pathname + search;

    const [status, setStatus] = useState<"process" | "success" | "failure">("process");

    useEffect(() => {
        (async () => {
            const service = new TemplateContent();
            await service.launch(template);
            setStatus(service.status);
        })()
    },[template,trigger]);

    return status === "success" ? <Fragment children={props.children}/> : null
});


export default Template;