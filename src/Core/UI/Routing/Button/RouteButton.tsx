import React, {FunctionComponent, useEffect, useState} from "react";
import {observer} from "mobx-react-lite";
import {useHistory} from "react-router-dom";
import {linkGenerator} from "../../../Util/LinkGenerator";
import {conditionFacade} from "../../../CoreCondition";
import {CoreAction, CorePropCondition} from "../../../CoreAction";
import {TRouteButton} from "./TRouteButton";
import {CoreStyle} from "../../../CoreStyle";
import "./base.css";

export const RouteButton:FunctionComponent<TRouteButton> = observer((props) => {
    const disableStatus = CorePropCondition.getCondition(props.template,':root > Disable Condition');
    const history = useHistory();

    const [disable, setDisable] = useState<boolean|undefined>(disableStatus);

    useEffect(() => {
        setDisable(disableStatus)
    }, [disableStatus]);

    const handleClick = () => {
        if(!disable){
            CoreAction.implement(props.template,':root > Trigger[TriggerName="OnClick"] > Command');
            history.push(linkGenerator(props.template, props.to) )
        }
    }

    return conditionFacade(props) ?
        <button data-role="route-button"
                type="button"
                className={props.style}
                style={CoreStyle.getStyle("Button",props.template,':root > RouteButton\\.Style')}
                disabled={disable}
                onClick={ handleClick }
                children={props.children}/>
        : null

});

export default RouteButton;