export type TRouteButton = {
    style?: string,
    to:string,
    command:string;
    template:string;
}