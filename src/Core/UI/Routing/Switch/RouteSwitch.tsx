import React, {FunctionComponent} from 'react';
import {Switch} from "react-router-dom";
import {observer} from "mobx-react-lite";
import Condition from "../../Condition/Condition";
import {TRouteSwitch} from "./TRouteSwitch";

export const RouteSwitch: FunctionComponent<TRouteSwitch> = observer((props) => {
        return <Condition conditionType={props.conditionName} conditionValue={props.conditionValue}><Switch children={props.children}/></Condition>
    }
);

export default RouteSwitch;