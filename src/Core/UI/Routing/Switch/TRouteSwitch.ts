export type TRouteSwitch ={
    conditionName?: string;
    conditionValue?: string;
    template:string;
}
