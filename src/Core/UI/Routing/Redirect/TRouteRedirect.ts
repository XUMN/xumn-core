export type TRouteRedirect ={
    to: string;
    from?: string;
    path?: string;
    exact?: boolean;
    strict?: boolean;
    template: string;
}