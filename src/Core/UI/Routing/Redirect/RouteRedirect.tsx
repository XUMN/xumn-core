import React, {FunctionComponent} from 'react';
import {observer} from "mobx-react-lite";
import {Redirect} from "react-router-dom";
import {conditionFacade} from "../../../CoreCondition";
import {TRouteRedirect} from "./TRouteRedirect";
import {linkGenerator} from "../../../Util/LinkGenerator";

export const RouteRedirect:FunctionComponent<TRouteRedirect> = observer((props) => {
    return conditionFacade(props) ?
        <Redirect path={props.path}
                  exact={!!props.exact}
                  to={linkGenerator(props.template, props.to)}
                  from={props.from}/>
        : null
});

export default RouteRedirect;