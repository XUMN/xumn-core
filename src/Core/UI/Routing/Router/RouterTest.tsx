import React, {FunctionComponent} from 'react';
import {BrowserRouter,RouterProps} from "react-router-dom";
import {observer} from "mobx-react-lite";

export const RouterTest:FunctionComponent<RouterProps> = observer((props) => {
    return <BrowserRouter children={props.children}/>
});

export default RouterTest;