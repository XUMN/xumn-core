import React, {FunctionComponent} from 'react';
import {Link} from "react-router-dom";
import {linkGenerator} from "../../../Util/LinkGenerator";
import {TRouteLink} from "./TRouteLink";
import {observer} from "mobx-react-lite";
import {CoreStyle} from "../../../CoreStyle";
import {CoreAction} from "../../../CoreAction";
import {CoreBinding} from "../../../CoreBinding";
import "./base.css";

export const RouteLink:FunctionComponent<TRouteLink> = observer((props) => {

    const binding = {bindingPath: "location.href", bindingSource: "Routing"}
    const current = new CoreBinding().sourceValue(binding.bindingSource).method.obtain( binding );
    const link = linkGenerator(props.template, props.to);

    const handleClick = () => {
        CoreAction.implement(props.template,':root > Trigger[TriggerName="OnClick"] > Command');
    }

    return <Link data-role="link"
                 role="link"
                 onClick={handleClick}
                 className={props.style}
                 style={CoreStyle.getStyle("Link",props.template,':root > Link\\.Style')}
                 to={link}
                 aria-current={current?.includes(link) ? "page" : undefined}
                 children={props.children}/>
});

export default RouteLink;