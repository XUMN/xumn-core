export type TRouteLink = {
    style?: string,
    to: string;
    replace?: boolean;
    template:string;
    conditionName?: string;
    conditionValue?: string;
}