export type TRouterRoute ={
    path?: string;
    exact?: boolean;
    sensitive?: boolean;
    strict?: boolean;
}