import {FunctionComponent,useEffect} from 'react';
import {observer} from "mobx-react-lite";
import {Route, useLocation} from "react-router-dom";
import {updateLocation} from "../../../Util/UpdateLocation";
import {TRouterRoute} from "./TRouterRoute";

export const RouterRoute:FunctionComponent<TRouterRoute> = observer((props) => {
        const location = useLocation();
        useEffect(() => updateLocation(location),[location]);
        return <Route path={props.path} exact={!!props.exact} children={props.children} />;
    }
);

export default RouterRoute;