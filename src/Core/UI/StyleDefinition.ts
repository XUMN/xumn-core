type TStyleDictionary = {
    [key: string]: {
        [key: string]: string;
    };
};
type TTriggerDefinition = {
    [key: string]: string;
};

export const triggerDefinition: TTriggerDefinition = {
    Hover: ":hover",
    Open: "[open]",
    Summary: ">summary",
    SummaryOpen: "[open]>summary",
    Current: "[aria-current='true']",
    Disabled: "[disabled]",
    DisabledHover: "[disabled]:hover",
    CurrentPage: "[aria-current='page']",
    LastChild: ":last-child",
    NotLastChild: ":not(:last-child)",
    Odd:":nth-child(odd)",
    Even:":nth-child(even)",
    OddHover:":nth-child(odd):hover",
    EvenHover:":nth-child(even):hover",
    TypeOdd:":nth-of-type(odd)",
    TypeEven:":nth-of-type(even)",
    TypeOddHover:":nth-of-type(odd):hover",
    TypeEvenHover:":nth-of-type(even):hover"


};


export const styleProperty: TTriggerDefinition = {
    Display: "display",
    Transition: "transition",

    GridTemplateAreas: "grid-template-areas",
    GridArea: "grid-area",

    GridTemplateColumns: "grid-template-columns",
    GridTemplateRows: "grid-template-rows",
    GridAutoFlow: "grid-auto-flow",
    Gap: "gap",
    RowGap: "row-gap",
    ColumnGap: "column-gap",
    GridColumn: "grid-column",


    FlexWrap: "flex-wrap",
    AlignItems: "align-items",
    JustifyContent: "justify-content",
    FlexGrow: "flex-grow",

    Position: "position",
    Left: "left",
    Right: "right",
    Top: "top",
    Bottom: "bottom",
    ZIndex: "z-index",

    Width: "width",
    MinWidth: "min-width",
    MaxWidth: "max-width",

    Height: "height",
    MinHeight: "min-height",
    MaxHeight: "max-height",

    Padding: "padding",
    PaddingLeft: "padding-left",
    PaddingRight: "padding-right",
    PaddingTop: "padding-top",
    PaddingBottom: "padding-bottom",


    Margin: "margin",
    MarginLeft: "margin-left",
    MarginRight: "margin-right",
    MarginTop: "margin-top",
    MarginBottom: "margin-bottom",

    BackgroundColor: "background-color",
    Background: "background",

    BackgroundImage: "background-image",
    BackgroundRepeat: "background-repeat",
    BackgroundPosition: "background-position",

    Border: "border",
    BorderLeft: "border-left",
    BorderRight: "border-right",
    BorderTop: "border-top",
    BorderBottom: "border-bottom",
    BackdropFilter: "backdrop-filter",
    WebkitBackdropFilter: "-webkit-backdrop-filter",


    BorderWidth: "border-width",
    BorderStyle: "border-style",
    BorderColor: "border-color",
    BorderRadius: "border-radius",

    Outline: "outline",
    BoxShadow: "box-shadow",

    FontFamily: "font-family",
    FontSize: "font-size",
    LineHeight: "line-height",
    FontWeight: "font-weight",
    TextAlign: "text-align",
    TextTransform: "text-transform",
    LetterSpacing: "letter-spacing",
    Color: "color",

    UserSelect: "user-select",
    PointerEvents: "pointer-events",

    Stroke: "stroke",
    Fill: "fill",
    StrokeDasharray: "stroke-dasharray",
    StrokeAlignment: "stroke-alignment",


    Overflow: "overflow",
    OverflowX:"overflow-x",
    OverflowY:"overflow-y",
    TextOverflow: "text-overflow",
    WhiteSpace: "white-space",
    Cursor: "cursor",
    Resize: "resize",
    "TableLayout":"table-layout",
    "VerticalAlign": "vertical-align",
    "Columns":"columns",
    "ColumnCount":"column-count",
    "ColumnWidth":"column-width",

    "ColumnFill":"column-fill",
    "ColumnRule":"column-rule",
    "ColumnSpan":"column-span",
    "BreakInside":"break-inside",
    "Filter":"filter",
};

export const styleDictionary: TStyleDictionary = {
    "Dialog": {
        AlignItems: styleProperty.AlignItems,
        JustifyContent: styleProperty.JustifyContent,
        BackgroundColor: styleProperty.BackgroundColor,
        Transition: styleProperty.Transition,
    },
    "FieldSet": {
        "Display": styleProperty.Display,

        "GridTemplateColumns": styleProperty.GridTemplateColumns,
        "GridTemplateRows": styleProperty.GridTemplateRows,
        "GridAutoFlow": styleProperty.GridAutoFlow,

        "AlignItems": styleProperty.AlignItems,
        "JustifyContent": styleProperty.JustifyContent,

        "Gap": styleProperty.Gap,
        "RowGap": styleProperty.RowGap,
        "ColumnGap": styleProperty.ColumnGap,

        "Width": styleProperty.Width,
        "MinWidth": styleProperty.MinWidth,
        "MaxWidth": styleProperty.MaxWidth,

        "PointerEvents": styleProperty.PointerEvents,
    },

"Group": {
    "Position": styleProperty.Position,
    "Left": styleProperty.Left,
    "Right": styleProperty.Right,
    "Top": styleProperty.Top,
    "Bottom": styleProperty.Bottom,

    "ZIndex": styleProperty.ZIndex,

    "Display": styleProperty.Display,

    "FontFamily": styleProperty.FontFamily,
    "FontSize": styleProperty.FontSize,
    "LineHeight": styleProperty.LineHeight,
    "TextAlign": styleProperty.TextAlign,
    "FontWeight": styleProperty.FontWeight,
    "Color": styleProperty.Color,
    "WordSpacing": styleProperty.WordSpacing,
    "WhiteSpace": styleProperty.WhiteSpace,
    "TextOverflow": styleProperty.TextOverflow,


    "Width": styleProperty.Width,
    "MinWidth": styleProperty.MinWidth,
    "MaxWidth": styleProperty.MaxWidth,
    "Height": styleProperty.Height,
    "MinHeight": styleProperty.MinHeight,
    "MaxHeight": styleProperty.MaxHeight,

    "Padding": styleProperty.Padding,
    "PaddingLeft": styleProperty.PaddingLeft,
    "PaddingRight": styleProperty.PaddingRight,
    "PaddingTop": styleProperty.PaddingTop,
    "PaddingBottom": styleProperty.PaddingBottom,

    "Margin": styleProperty.Margin,
    "MarginLeft": styleProperty.MarginLeft,
    "MarginRight": styleProperty.MarginLeft,
    "MarginTop": styleProperty.MarginTop,
    "MarginBottom": styleProperty.MarginBottom,

    "BackgroundColor": styleProperty.BackgroundColor,
    "Background": styleProperty.Background,

    "Border": styleProperty.Border,
    "BorderLeft": styleProperty.BorderLeft,
    "BorderRight": styleProperty.BorderRight,
    "BorderTop": styleProperty.BorderTop,
    "BorderBottom": styleProperty.BorderBottom,
    "BorderRadius": styleProperty.BorderRadius,
    "BorderColor": styleProperty.BorderColor,

    "BoxShadow": styleProperty.BoxShadow,

    "Outline": styleProperty.Outline,

    "BackdropFilter": styleProperty.BackdropFilter,
    "WebkitBackdropFilter": styleProperty.WebkitBackdropFilter,
    "Overflow": styleProperty.Overflow,

    "OverflowX": styleProperty.OverflowX,
    "OverflowY": styleProperty.OverflowY,

    "FlexGrow": styleProperty.FlexGrow,

    "Transition": styleProperty.Transition,

},

    "Block": {
        "FontFamily": styleProperty.FontFamily,
        "FontSize": styleProperty.FontSize,
        "LineHeight": styleProperty.LineHeight,
        "TextAlign": styleProperty.TextAlign,
        "FontWeight": styleProperty.FontWeight,
        "Color": styleProperty.Color,
        "WordSpacing": styleProperty.WordSpacing,
        "WhiteSpace": styleProperty.WhiteSpace,
        "TextOverflow": styleProperty.TextOverflow,


        "BoxShadow": styleProperty.BoxShadow,


        "BackdropFilter": styleProperty.BackdropFilter,
        "WebkitBackdropFilter": styleProperty.WebkitBackdropFilter,

        "Overflow": styleProperty.Overflow,
        "OverflowX": styleProperty.OverflowX,
        "OverflowY": styleProperty.OverflowY,

        "FlexGrow": styleProperty.FlexGrow,

        "Transition": styleProperty.Transition,

    },

    "Expander": {
        "Padding": styleProperty.Padding,
        "BorderTop": styleProperty.BorderTop,
        "BorderBottom": styleProperty.BorderBottom,
    },
    "Padding": {
        "Padding": styleProperty.Padding,
    },
    "Margin": {
        "Margin": styleProperty.Margin,
    },
    "Border": {
        "Border": styleProperty.Border,
        "BorderTop": styleProperty.BorderTop,
        "BorderBottom": styleProperty.BorderBottom,
        "BorderWidth": styleProperty.BorderWidth,
        "BorderStyle": styleProperty.BorderStyle,
        "BorderColor": styleProperty.BorderColor,
        "BorderRadius": styleProperty.BorderRadius,
    },
    "Page": {
        "Display": styleProperty.Display,
        "Width": styleProperty.Width,
        "MinWidth": styleProperty.MinWidth,
        "MaxWidth": styleProperty.MaxWidth,
        "AlignItems": styleProperty.AlignItems,
        "JustifyContent": styleProperty.JustifyContent,
        "Outline": styleProperty.Outline,
        "Border": styleProperty.Border,
        "BorderLeft": styleProperty.BorderLeft,
        "BorderRight": styleProperty.BorderRight,
        "BorderTop": styleProperty.BorderTop,
        "BorderBottom": styleProperty.BorderBottom,
        "BorderWidth": styleProperty.BorderWidth,
        "BorderStyle": styleProperty.BorderStyle,
        "BorderColor": styleProperty.BorderColor,
        "BorderRadius": styleProperty.BorderRadius,
        "BackgroundColor": styleProperty.BackgroundColor,
        "Margin": styleProperty.Margin,
    },
    "Table": {
        "Width": styleProperty.Width,
        "MinWidth": styleProperty.MinWidth,
        "MaxWidth": styleProperty.MaxWidth,
        "TableLayout": styleProperty.TableLayout,
        "Overflow": styleProperty.Overflow,
        "BorderRadius": styleProperty.BorderRadius,
        "BackgroundColor": styleProperty.BackgroundColor,
    },
    "TableHead": {
        "BackgroundColor": styleProperty.BackgroundColor,
        "BorderTop": styleProperty.BorderTop,
        "BorderBottom": styleProperty.BorderBottom,
    },
    "TableBody": {
        "BackgroundColor": styleProperty.BackgroundColor,
        "BorderTop": styleProperty.BorderTop,
        "BorderBottom": styleProperty.BorderBottom,
    },
    "TableFoot": {
        "BackgroundColor": styleProperty.BackgroundColor,
        "BorderTop": styleProperty.BorderTop,
        "BorderBottom": styleProperty.BorderBottom,
    },
    "TableBodyLink": {
        "BackgroundColor": styleProperty.BackgroundColor,
        "BorderTop": styleProperty.BorderTop,
        "BorderBottom": styleProperty.BorderBottom,
        "Cursor": styleProperty.Cursor,
    },
    "Row": {
        "Border": styleProperty.Border,
        "BorderLeft": styleProperty.BorderLeft,
        "BorderRight": styleProperty.BorderRight,
        "BorderTop": styleProperty.BorderTop,
        "BorderBottom": styleProperty.BorderBottom,
        "Background": styleProperty.Background,
        "BackgroundColor": styleProperty.BackgroundColor,
    },
    "ColumnHeader": {
        "VerticalAlign": styleProperty.VerticalAlign,
        "Padding": styleProperty.Padding,
        "BackgroundColor": styleProperty.BackgroundColor,
    },
    "Cell": {
        "VerticalAlign": styleProperty.VerticalAlign,
        "Padding": styleProperty.Padding,
        "BackgroundColor": styleProperty.BackgroundColor,
    },

    "PositionLayout": {
        "Width": styleProperty.Width,
        "Height": styleProperty.Height,
        "Margin": styleProperty.Margin,
        "Position": styleProperty.Position,
        "Left": styleProperty.Left,
        "Right": styleProperty.Right,
        "Top": styleProperty.Top,
        "Bottom": styleProperty.Bottom,
        "ZIndex": styleProperty.ZIndex,
    },

    "Complementary": {
        "Display": styleProperty.Display,
        "FlexWrap": styleProperty.FlexWrap,
        "AlignItems": styleProperty.AlignItems,
        "JustifyContent": styleProperty.JustifyContent,
        "FlexGrow": styleProperty.FlexGrow,
        "Gap": styleProperty.Gap,
        "RowGap": styleProperty.RowGap,
        "ColumnGap": styleProperty.ColumnGap,
        "Width": styleProperty.Width,
        "MinWidth": styleProperty.MinWidth,
        "MaxWidth": styleProperty.MaxWidth,
        "Height": styleProperty.Height,
        "MinHeight": styleProperty.MinHeight,
        "MaxHeight": styleProperty.MaxHeight,
        "Position": styleProperty.Position,
        "Left": styleProperty.Left,
        "Right": styleProperty.Right,
        "Top": styleProperty.Top,
        "Bottom": styleProperty.Bottom,
        "ZIndex": styleProperty.ZIndex,
        "Overflow": styleProperty.Overflow,

        "Padding": styleProperty.Padding,

        "BoxShadow": styleProperty.BoxShadow,

        "Border": styleProperty.Border,
        "BorderLeft": styleProperty.BorderLeft,
        "BorderRight": styleProperty.BorderRight,
        "BorderTop": styleProperty.BorderTop,
        "BorderBottom": styleProperty.BorderBottom,
        "Outline": styleProperty.Outline,

        "Background": styleProperty.Background,
        "BackgroundColor": styleProperty.BackgroundColor,
    },
    "StackLayout": {
        "GridTemplateColumns": styleProperty.GridTemplateColumns,
        "GridTemplateRows": styleProperty.GridTemplateRows,
        "GridAutoFlow": styleProperty.GridAutoFlow,
        "AlignItems": styleProperty.AlignItems,
        "Gap": styleProperty.Gap,
        "RowGap": styleProperty.RowGap,
        "GridColumnGap": styleProperty.GridColumnGap,
        "Width": styleProperty.Width,
        "Height": styleProperty.Height,
        "MaxHeight": styleProperty.MaxHeight
    },
    "Flex": {
        "FontFamily": styleProperty.FontFamily,
        "FontSize": styleProperty.FontSize,
        "LineHeight": styleProperty.LineHeight,
        "FontWeight": styleProperty.FontWeight,
        "TextAlign": styleProperty.TextAlign,
        "TextTransform": styleProperty.TextTransform,
        "Color": styleProperty.Color,
        "UserSelect": styleProperty.UserSelect,
        "PointerEvents": styleProperty.PointerEvents,
        "Overflow": styleProperty.Overflow,
        "TextOverflow": styleProperty.TextOverflow,
        "WhiteSpace": styleProperty.WhiteSpace,

    },
    "InputText": {
        "Height": styleProperty.Height,
        "Width": styleProperty.Width,
        "MaxWidth": styleProperty.MaxWidth,

        "BackgroundColor": styleProperty.BackgroundColor,
        "Padding": styleProperty.Padding,
        "Border": styleProperty.Border,
        "BorderColor": styleProperty.BorderColor,
        "BorderRadius": styleProperty.BorderRadius,

        "FontFamily": styleProperty.FontFamily,
        "FontSize": styleProperty.FontSize,
        "LineHeight": styleProperty.LineHeight,
        "FontWeight": styleProperty.FontWeight,
        "Color": styleProperty.Color,

        "PointerEvents": styleProperty.PointerEvents,
    },
    "Password": {
        "Height": styleProperty.Height,
        "BackgroundColor": styleProperty.BackgroundColor,
        "Padding": styleProperty.Padding,
        "Border": styleProperty.Border,
        "BorderColor": styleProperty.BorderColor,
        "BorderRadius": styleProperty.BorderRadius,
        "FontFamily": styleProperty.FontFamily,
        "FontSize": styleProperty.FontSize,
        "LineHeight": styleProperty.LineHeight,
        "FontWeight": styleProperty.FontWeight,
        "Color": styleProperty.Color,
        "PointerEvents": styleProperty.PointerEvents,
    },

    "Time": {
        "Display": styleProperty.Display,

        "Width": styleProperty.Width,
        "MaxWidth": styleProperty.MaxWidth,

        "Padding": styleProperty.Padding,
        "PaddingLeft": styleProperty.PaddingLeft,
        "PaddingRight": styleProperty.PaddingRight,
        "PaddingTop": styleProperty.PaddingTop,
        "PaddingBottom": styleProperty.PaddingBottom,

        "Margin": styleProperty.Margin,
        "MarginLeft": styleProperty.MarginLeft,
        "MarginRight": styleProperty.MarginLeft,
        "MarginTop": styleProperty.MarginTop,
        "MarginBottom": styleProperty.MarginBottom,

        "FontFamily": styleProperty.FontFamily,
        "FontSize": styleProperty.FontSize,
        "LineHeight": styleProperty.LineHeight,
        "FontWeight": styleProperty.FontWeight,
        "TextAlign": styleProperty.TextAlign,
        "TextTransform": styleProperty.TextTransform,
        "LetterSpacing": styleProperty.LetterSpacing,


        "Color": styleProperty.Color,
        "UserSelect": styleProperty.UserSelect,
        "PointerEvents": styleProperty.PointerEvents,
        "Overflow": styleProperty.Overflow,
        "TextOverflow": styleProperty.TextOverflow,
        "WhiteSpace": styleProperty.WhiteSpace,

        "BackgroundColor": styleProperty.BackgroundColor,

        "BorderRadius": styleProperty.BorderRadius,
    },
    "Text": {
        "UserSelect": styleProperty.UserSelect,
        "PointerEvents": styleProperty.PointerEvents,
        "Overflow": styleProperty.Overflow,
        "TextOverflow": styleProperty.TextOverflow,
        "WhiteSpace": styleProperty.WhiteSpace,
    },

    "Span": {
        "Width": styleProperty.Width,
        "MaxWidth": styleProperty.MaxWidth,
        "Padding": styleProperty.Padding,
        "PaddingLeft": styleProperty.PaddingLeft,
        "PaddingRight": styleProperty.PaddingRight,
        "PaddingTop": styleProperty.PaddingTop,
        "PaddingBottom": styleProperty.PaddingBottom,
        "Margin": styleProperty.Margin,
        "FontFamily": styleProperty.FontFamily,
        "FontSize": styleProperty.FontSize,
        "LineHeight": styleProperty.LineHeight,
        "FontWeight": styleProperty.FontWeight,
        "TextAlign": styleProperty.TextAlign,
        "TextTransform": styleProperty.TextTransform,
        "Color": styleProperty.Color,
        "UserSelect": styleProperty.UserSelect,
        "PointerEvents": styleProperty.PointerEvents,
        "Overflow": styleProperty.Overflow,
        "TextOverflow": styleProperty.TextOverflow,
        "WhiteSpace": styleProperty.WhiteSpace,
        "BackgroundColor": styleProperty.BackgroundColor,
        "BorderRadius": styleProperty.BorderRadius,
    },
    "DateTime": {
        "FontFamily": styleProperty.FontFamily,
        "FontSize": styleProperty.FontSize,
        "LineHeight": styleProperty.LineHeight,
        "FontWeight": styleProperty.FontWeight,
        "TextAlign": styleProperty.TextAlign,
        "TextTransform": styleProperty.TextTransform,
        "Color": styleProperty.Color,
        "UserSelect": styleProperty.UserSelect,
        "PointerEvents": styleProperty.PointerEvents,
        "Overflow": styleProperty.Overflow,
        "TextOverflow": styleProperty.TextOverflow,
        "WhiteSpace": styleProperty.WhiteSpace,
    },
    "VectorGraphic": {
        "Width": styleProperty.Width,
        "MinWidth": styleProperty.MinWidth,
        "MaxWidth": styleProperty.MaxWidth,
        "Height": styleProperty.Height,
        "MinHeight": styleProperty.MinHeight,
        "MaxHeight": styleProperty.MaxHeight,
        "Margin": styleProperty.Margin,
        "Padding": styleProperty.Padding,
        "Stroke": styleProperty.Stroke,
        "Fill": styleProperty.Fill,
        "StrokeDasharray": styleProperty.StrokeDasharray,
        "StrokeAlignment": styleProperty.StrokeAlignment,
    },
    "TextArea": {
        "Width": styleProperty.Width,
        "MinWidth": styleProperty.MinWidth,
        "MaxWidth": styleProperty.MaxWidth,
        "Height": styleProperty.Height,
        "MinHeight": styleProperty.MinHeight,
        "MaxHeight": styleProperty.MaxHeight,
        "Resize": styleProperty.Resize,
    },
    "TextBlock": {
        "Display": styleProperty.Display,
        "FlexWrap": styleProperty.FlexWrap,
        "UserSelect": styleProperty.UserSelect,
        "PointerEvents": styleProperty.PointerEvents,
        "Overflow": styleProperty.Overflow,
        "TextOverflow": styleProperty.TextOverflow,
        "WhiteSpace": styleProperty.WhiteSpace,
    },

    "SideBar": {
        "Overflow": styleProperty.Overflow,
    },

    "Summary": {
        "Display": styleProperty.Display,
        "Gap": styleProperty.Gap,
        "RowGap": styleProperty.RowGap,
        "ColumnGap": styleProperty.ColumnGap,

        "GridTemplateColumns": styleProperty.GridTemplateColumns,
        "GridTemplateRows": styleProperty.GridTemplateRows,
        "AlignItems": styleProperty.AlignItems,
        "JustifyContent": styleProperty.JustifyContent,
        "FlexWrap": styleProperty.FlexWrap,

        "FontFamily": styleProperty.FontFamily,
        "FontSize": styleProperty.FontSize,
        "LineHeight": styleProperty.LineHeight,
        "FontWeight": styleProperty.FontWeight,
        "TextAlign": styleProperty.TextAlign,
        "TextTransform": styleProperty.TextTransform,
        "Color": styleProperty.Color,

        "BackgroundImage": styleProperty.BackgroundImage,

        "BackgroundRepeat": styleProperty.BackgroundRepeat,
        "BackgroundPosition": styleProperty.BackgroundPosition,



        "UserSelect": styleProperty.UserSelect,
        "Overflow": styleProperty.Overflow,
        "TextOverflow": styleProperty.TextOverflow,
        "WhiteSpace": styleProperty.WhiteSpace,
        "PointerEvents": styleProperty.PointerEvents,
    },
    "Column": {
        "Columns":styleProperty.Columns,
        "ColumnCount":styleProperty.ColumnsCount,
        "ColumnWidth":styleProperty.ColumnWidth,
        "ColumnGap":styleProperty.ColumnGap,
        "ColumnFill":styleProperty.ColumnFill,
        "ColumnRule":styleProperty.ColumnRule,
        "ColumnSpan":styleProperty.ColumnSpan,
        "BreakInside":styleProperty.BreakInside,
        "Margin": styleProperty.Margin,
        "Padding": styleProperty.Padding,
    },
    "Select": {
        "Width": styleProperty.Width,
        "MinWidth": styleProperty.MinWidth,
        "MaxWidth": styleProperty.MaxWidth,
    },
};

const group = {
    "Display": {
        "Display": "display",
    },
    "Width": {
        "Width": "width",
        "MinWidth": "min-width",
        "MaxWidth": "max-width",
    },
    "Height": {
        "Height": "height",
        "MinHeight": "min-height",
        "MaxHeight": "max-height",
    },
    "Margin": {
        "Margin": "margin",
        "MarginLeft": "margin-left",
        "MarginRight": "margin-right",
        "MarginTop": "margin-top",
        "MarginBottom": "margin-bottom",
    },
    "Padding":{
        "Padding": "padding",
        "PaddingLeft": "padding-left",
        "PaddingRight": "padding-right",
        "PaddingTop": "padding-top",
        "PaddingBottom": "padding-bottom",
    },
    "Position":{
        "Position": "position",
        "Left": "left",
        "Right": "right",
        "Top": "top",
        "Bottom": "bottom",
        "ZIndex": "z-index"
    },
    "Border":{
        "Border": "border",
        "BorderLeft": "border-left",
        "BorderRight": "border-right",
        "BorderTop": "border-top",
        "BorderBottom": "border-bottom",
        "BorderColor": "border-color",
        "BorderRadius": "border-radius",
        "BorderWidth": "border-width",
        "BorderStyle": "border-style",
    },
    "Outline":{
        "Outline": "outline",
    },
    "Background": {
        "Background": "background",
        "BackgroundColor": "background-color",
        "BackgroundImage": "background-image",
        "BackgroundRepeat": "background-repeat",
        "BackgroundPosition": "background-position",
    },
    "Flex":{
        "FlexWrap": "flex-wrap",
        "AlignItems": "align-items",
        "JustifyContent": "justify-content",
        "FlexGrow": "flex-grow",
        "Gap": "gap",
        "RowGap": "row-gap",
        "ColumnGap": "column-gap",
    },
    "Grid":{
        "GridTemplateAreas": "grid-template-areas",
        "GridArea": "grid-area",
        "GridTemplateColumns": "grid-template-columns",
        "GridTemplateRows": "grid-template-rows",
        "GridAutoFlow": "grid-auto-flow",
        "AlignItems": "align-items",
        "JustifyContent": "justify-content",
        "Gap": "gap",
        "RowGap": "row-gap",
        "ColumnGap": "column-gap",
        "GridRow": "grid-row",
        "GridRowStart": "grid-row-start",
        "GridRowEnd": "grid-row-end",
        "GridColumn": "grid-column",
        "GridColumnStart": "grid-column-start",
        "GridColumnEnd": "grid-column-end",
    },
    "Text":{
        "FontFamily": "font-family",
        "FontSize": "font-size",
        "LineHeight": "line-height",
        "FontWeight": "font-weight",
        "TextAlign": "text-align",
        "TextTransform": "text-transform",
        "TextOverflow":"text-overflow",
        "LetterSpacing": "letter-spacing",
        "Color": "color",
        "WhiteSpace": "white-space",
    },
    "Overflow":{
        "Overflow": "overflow",
        "OverflowX": "overflow-x",
        "OverflowY": "overflow-y",
    },
    "Transition":{
        "Transition":"transition"
    },
    "Cursor": {
        "Cursor":"cursor"
    },
    "PointerEvents":{
        "PointerEvents":"pointer-events"
    },
    "UserSelect":{
        "UserSelect":"user-select"
    },
    "Filter": {
        "Filter":"filter"
    },
    "BoxShadow":{
        "BoxShadow":"box-shadow"
    },


}

function setGroup(name:Array<string>,group:{ [key:string]:string }){
    for(const key of name){
        if(!styleDictionary.hasOwnProperty(key)){
            styleDictionary[key] = {}
        }
        Object.assign( styleDictionary[key], {...group});
    }
}

function setDecoration(name:Array<string> ){
    setGroup(name,group["Width"]);
    setGroup(name,group["Height"]);
    setGroup(name,group["Margin"]);
    setGroup(name,group["Padding"]);
    setGroup(name,group["Border"]);
    setGroup(name,group["Outline"]);
    setGroup(name,group["Background"]);
    setGroup(name,group["Position"]);
    setGroup(name,group["BoxShadow"]);
}

// Основные лэйауты приложения
void function(){
    const name = ["Navigation","Main","Header","Footer","SideBar"];
    setGroup(name,group["Display"]);
    setGroup(name,group["Flex"]);
    setGroup(name,group["Grid"]);
    setGroup(name,group["Overflow"]);
    setDecoration(name);
}();


void function(){
    const name = ["Grid","GridCell","GridRow"];
    setGroup(name,group["Grid"]);
    setGroup(name,group["Overflow"]);
    setGroup(name,group["PointerEvents"]);
    setDecoration(name);
}();

void function(){
    const name = ["Flex"];
    setGroup(name,group["Flex"]);
    setGroup(name,group["Overflow"]);
    setGroup(name,group["PointerEvents"]);
    setDecoration(name);
}();

void function(){
    const name = ["Block","Menu","Group","MenuGroup"];
    setGroup(name,group["Text"]);
    setGroup(name,group["Overflow"]);
    setGroup(name,group["PointerEvents"]);
    setGroup(name,group["UserSelect"]);
    setDecoration(name);
}();

// Контролы
void function(){
    const name = ["Link","Button","InputButton","InputImage","MenuToggle","MenuGroup","MenuItem","AutoComplete","AutoCompleteGroup"];
    setGroup(name,group["Display"]);
    setGroup(name,group["Flex"]);
    setGroup(name,group["Grid"]);
    setGroup(name,group["Text"]);
    setGroup(name,group["Overflow"]);
    setGroup(name,group["Cursor"]);
    setGroup(name,group["PointerEvents"]);
    setGroup(name,group["UserSelect"]);
    setGroup(name,group["Filter"]);
    setGroup(name,group["Transition"]);
    setDecoration(name);
}();

void function(){
    const name = ["InputText","InputPassword","InputDate"];
    setGroup(name,group["Text"]);
    setGroup(name,group["Overflow"]);
    setGroup(name,group["Cursor"]);
    setGroup(name,group["PointerEvents"]);
    setGroup(name,group["UserSelect"]);
    setGroup(name,group["Filter"]);
    setGroup(name,group["Transition"]);
    setDecoration(name);
}();

void function(){
    const name = ["Text"];
    setGroup(name,group["Text"]);
    setGroup(name,group["Overflow"]);
    setGroup(name,group["PointerEvents"]);
    setDecoration(name);
}();

void function(){
    const name = ["Image"];
    setGroup(name,group["Filter"]);
    setGroup(name,group["Transition"]);
    setGroup(name,group["PointerEvents"]);
    setDecoration(name);
}();

void function(){
    const name = ["Details"];
    setGroup(name,group["Text"]);
    setDecoration(name);
}();

void function(){
    const name = ["Summary"];
    setGroup(name,group["PointerEvents"]);
    setGroup(name,group["Transition"]);
    setDecoration(name);
}();

void function(){
    const name = ["Dialog"];
    setGroup(name,group["Flex"]);
    setGroup(name,group["Overflow"]);
    setGroup(name,group["Transition"]);
    setDecoration(name);
}();