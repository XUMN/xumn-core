import {TOutsideClickMixinProps} from "../../Observer/OutsideClick/TOutsideClickMixinProps";

export type TMenuGroup = {
    style?: string,
    conditionType?: string;
    conditionValue?: string;
    template:string;
    mixinProps: TOutsideClickMixinProps;
}