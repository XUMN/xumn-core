import React, {FunctionComponent, Children, isValidElement, cloneElement, useState, useEffect} from "react";
import {observer} from "mobx-react-lite";
import {TMenuGroup} from "./TMenuGroup";
import {CoreStyle} from "../../../CoreStyle";
import "./base.css";

export const MenuGroup: FunctionComponent<TMenuGroup> = observer((props) => {
        const {mixinProps} = props;
        const {currentStatus} = mixinProps;

        const [open, setOpen] = useState<boolean>(currentStatus);

        useEffect(() => setOpen(currentStatus), [currentStatus]);

        const childrenWidthMixin = Children.map(props.children, item => {
            return isValidElement(item) ? cloneElement(item, {mixinProps}) : item;
        });

        return open ?
            <menu data-role="menu-group"
                  role="group"
                  aria-hidden={!open}
                  className={props.style}
                  style={CoreStyle.getStyle("MenuGroup",props.template,':root > MenuGroup\\.Style')}
                  children={childrenWidthMixin}/>
            :
            null

    }
);

export default MenuGroup;