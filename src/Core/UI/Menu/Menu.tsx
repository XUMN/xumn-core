import React, {FunctionComponent,useRef,useState,Children,isValidElement,cloneElement} from "react";
import {observer} from "mobx-react-lite";
import {TMenu} from "./TMenu";
import {CoreStyle} from "../../CoreStyle";
import {TOutsideClickMixinProps} from "../Observer/OutsideClick/TOutsideClickMixinProps";

export const Menu: FunctionComponent<TMenu> = observer((props) => {
        const [open, setOpen] = useState<boolean>(!!props.open);
        const ref = useRef(null)

        const mixinProps:TOutsideClickMixinProps = {
            currentStatus: open,
            reference: ref,
            setStatus: (open:boolean) => setOpen(open),
        }

        const childrenWidthMixin = Children.map(props.children, item => {
            return isValidElement(item) ? cloneElement(item, {mixinProps}) : item;
        });

        return <menu data-role="menu"
                     role="menu"
                     ref={ref}
                     className={props.style}
                     style={CoreStyle.getStyle("Menu",props.template,':root > Menu\\.Style')}
                     children={childrenWidthMixin}/>
    }
);

export default Menu;