import React, {FunctionComponent} from 'react';
import {observer} from "mobx-react-lite";
import {CoreAction} from "../../../CoreAction";
import {TMenuItem} from "./TMenuItem";
import {conditionFacade} from "../../../CoreCondition";
import {CoreStyle} from "../../../CoreStyle";

export const MenuItem:FunctionComponent<TMenuItem> = observer((props) => {
    const handleClick = () => {
        CoreAction.implement(props.template,':root > Trigger[TriggerName="OnClick"] > Command');
        if(props.mixinProps){
            const {setStatus} = props.mixinProps;
            setStatus(false);
        }
    }

    return conditionFacade(props) ?
        <li role="menuitem"
            className={props.style}
            style={CoreStyle.getStyle("MenuItem",props.template,':root > Style')}
            onClick={ handleClick }
            children={props.children}/>
        : null
});

export default MenuItem;