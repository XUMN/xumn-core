import {TOutsideClickMixinProps} from "../../Observer/OutsideClick/TOutsideClickMixinProps";

export type TMenuItem = {
    value: string;
    style?: string;
    disabled?: string;
    template: string;
    onClick: Function;
    mixinProps?: TOutsideClickMixinProps;
}