import {TOutsideClickMixinProps} from "../../Observer/OutsideClick/TOutsideClickMixinProps";

export type TMenuToggle = {
    value: string;
    style?: string;
    disabled?: string;
    template: string;
    mixinProps: TOutsideClickMixinProps;
}