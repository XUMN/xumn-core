import React, {FunctionComponent, useState, useEffect} from 'react';
import {observer} from "mobx-react-lite";
import {CoreAction} from "../../../CoreAction";
import {TMenuToggle} from "./TMenuToggle";
import {conditionFacade} from "../../../CoreCondition";
import {CoreStyle} from "../../../CoreStyle";
import "./base.css";

export const MenuToggle:FunctionComponent<TMenuToggle> = observer((props) => {
    const {setStatus,currentStatus} = props.mixinProps;

    const [pressed, setPressed] = useState<boolean>(currentStatus);

    useEffect(() => setPressed(currentStatus), [currentStatus]);

    const handleClick = () => {
        setStatus(!currentStatus);
        CoreAction.implement(props.template,':root > Trigger[TriggerName="OnClick"] > Command' );
    }

    return conditionFacade(props) ?
        <li data-role="menu-toggle"
            role="button"
            aria-pressed={pressed}
            className={props.style}
            style={CoreStyle.getStyle("MenuToggle",props.template,':root > MenuToggle\\.Style')}
            onClick={ handleClick }
            children={props.children}/>
        : null
});

export default MenuToggle;