export type TMenu = {
    style?: string,
    open: string;
    conditionType?: string;
    conditionValue?: string;
    template:string;
}