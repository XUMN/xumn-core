export type TLink = {
    style?: string,
    to: string;
    replace?: boolean;
    template:string;
    conditionName?: string;
    conditionValue?: string;
    valueType?: string;
}