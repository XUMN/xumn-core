import React, {FunctionComponent, useEffect, useState} from 'react'
import {observer} from "mobx-react-lite";
import {installHistory} from "../Router/RouterCommand";
import {linkGenerator} from "../../Util/LinkGenerator";
import {CoreStyle} from "../../CoreStyle";
import {TLink} from "./TLink";
import "./base.css";
import {CoreBinding} from "../../CoreBinding";

export const Link: FunctionComponent<TLink>  = observer((props) => {
    const generator = linkGenerator(props.template, props.to);
    const [link, setLink] = useState<string>(generator);
    useEffect(() => setLink(generator), [generator]);

    const href = linkGenerator(props.template, props.to);

    return <button data-role="link"
                   type="button"
                   role="link"
                   className={props.style}
                   style={CoreStyle.getStyle("Link",props.template,':root > Link\\.Style')}
                   aria-current={window.location.href.includes(href) ? "page" : false}
                   onClick={()=>installHistory(href)}
                   children={props.children}/>
});

export default Link;
