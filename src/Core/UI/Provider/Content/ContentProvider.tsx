import  {FunctionComponent, createElement, useEffect, useState, Fragment} from "react";
import {observer} from "mobx-react-lite";
import {ContentService} from "../../../Service/Content/ContentService";
import {TContentProvider} from "./TContentProvider";

export const ContentProvider:FunctionComponent<TContentProvider> = observer((props) => {
    const [status, setStatus] = useState<"process" | "success" | "failure">("process");
    useEffect(() => {
        const contentService = new ContentService()
        contentService.supplyContent(props).then(() => {setStatus(contentService.status)})
    }, [props]);
    return status === "success" ? createElement(Fragment, {children:props.children}) : null;
});

export default ContentProvider;