export type TContentProvider = {
    template:string;
    into:string;
    providerType:string;
    processNotice: string;
    successNotice: string;
    failureNotice: string;
}