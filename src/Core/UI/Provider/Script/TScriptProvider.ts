export type TScriptProvider = {
    template:string;
    into:string;
    providerType:string;
    processNotice: string;
    successNotice: string;
    failureNotice: string;
}