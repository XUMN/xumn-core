import  {FunctionComponent, createElement, useEffect, useState, Fragment} from "react";
import {ContentService} from "../../../Service/Content/ContentService";
import {TScriptProvider} from "./TScriptProvider";

export const ScriptProvider:FunctionComponent<TScriptProvider> = (props) => {
/*
    async function getComponent() {
        const {default} = await import('./my-module');
        return React.createElement(default.view)
    })

 */

    const [status, setStatus] = useState<"process" | "success" | "failure">("process");
    useEffect(() => {
        const contentService = new ContentService()
        contentService.supplyContent(props).then(() => {setStatus(contentService.status)})
    }, [props]);
    return status === "success" ? createElement(Fragment, {children:props.children}) : null;
};

export default ScriptProvider;