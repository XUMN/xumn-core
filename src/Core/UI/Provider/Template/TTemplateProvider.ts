export type TTemplateProvider = {
    source:string;
    renovate?:string;
    processNotice?:string;
}