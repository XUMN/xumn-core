import {createElement, useEffect, useState} from "react";
import {observer} from "mobx-react-lite";
import {store} from "../../../CoreStore";
import {ComponentFactory} from "../../../Factory/Component/ComponentFactory";
import {TemplateService} from "../../../Service/Template/TemplateService";
import {TTemplateProvider} from "./TTemplateProvider";

export const TemplateProvider = observer((props: TTemplateProvider) => {
    const [status, setStatus] = useState<"process" | "success" | "failure">("process");
    useEffect(() => {
        const service = new TemplateService();
        service.launch(props).then( () => setStatus(service.status));
    }, [ props ]);
    if(status === "success"){
        const template = store.template.method.obtain(props.source);
        return template ? createElement( ComponentFactory, {template: template }) : null;
    } else {
        return null
    }
});