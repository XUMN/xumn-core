import React, {FunctionComponent} from "react";
import {observer} from "mobx-react-lite";
import {conditionFacade} from "../../CoreCondition";
import {TDetails} from "./TDetails";
import {CoreStyle} from "../../CoreStyle";
import "./base.css";

export const Details: FunctionComponent<TDetails> = observer((props) => {
        return conditionFacade(props) ?
            <details data-role="details"
                     open={!!props.open}
                     className={props.style}
                     style={CoreStyle.getStyle("Details",props.template,':root > Details\\.Style')}
                     children={props.children}/>
            : null
    }
);

export default Details;