export type TDetails = {
    style?: string,
    open?: string;
    conditionType?: string;
    conditionValue?: string;
    template: string,
}