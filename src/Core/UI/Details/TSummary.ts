export type TSummary = {
    style?: string,
    conditionType?: string;
    conditionValue?: string;
    template: string;
}