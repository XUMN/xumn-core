import React, {FunctionComponent} from "react";
import {observer} from "mobx-react-lite";
import {conditionFacade} from "../../CoreCondition";
import {TSummary} from "./TSummary";
import {CoreStyle} from "../../CoreStyle";

export const Summary: FunctionComponent<TSummary> = observer((props) => {
        return conditionFacade(props) ?
            <summary className={props.style}
                     style={CoreStyle.getStyle("Summary",props.template,':root > Style')}
                     children={props.children}/> : null
    }
);

export default Summary;