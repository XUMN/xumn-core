import {createElement,useEffect,useState} from 'react';
import {observer} from "mobx-react-lite";
import {useLocation} from "react-router-dom";
import {Source} from "../../Util/TemplateIdGenerator";
import {conditionFacade} from "../../CoreCondition";
import {TemplateProvider} from "../Provider/Template/TemplateProvider";
import {TSlot} from "./TSlot";

export const Slot = observer((props:TSlot) => {
    const id = Source.getSource(props.template,props.routePattern,useLocation().pathname,props.source);
    const [source, setSource] = useState<string>(id);
    useEffect(() => setSource(id), [id]);
    return conditionFacade(props) ? createElement(TemplateProvider, {source:source}) : null
});

export default Slot;
