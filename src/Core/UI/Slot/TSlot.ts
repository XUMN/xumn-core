export type TSlot = {
    template: string;
    source: string;
    conditionType?: string;
    conditionValue?: string;
    routePattern?: string;
}