import React, {FunctionComponent} from "react";
import {BrowserRouter} from "react-router-dom";

export const Application:FunctionComponent = (props) => {
    return <BrowserRouter children={props.children}/>
};

export default Application;