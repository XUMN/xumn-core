export type TCondition = {
    conditionType?: string;
    conditionValue?: string;
    template?: string
}