import React, {FunctionComponent,useState,useEffect,Fragment} from "react";
import {observer} from "mobx-react-lite";
import {conditionFacade} from "../../CoreCondition";
import {TCondition} from "./TCondition";

export const Condition:FunctionComponent<TCondition> = observer((props) => {
    const condition = conditionFacade(props);
    const [state, setState] = useState(condition);
    useEffect(() => setState(condition), [condition]);
    return (
        state
            ? <Fragment children={props.children}/>
            : null
    )
});

export default Condition;