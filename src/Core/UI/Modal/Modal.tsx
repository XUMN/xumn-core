import React, {FunctionComponent} from "react";
import {observer} from "mobx-react-lite";
import {store} from "../../CoreStore";
import {TemplateProvider} from "../Provider/Template/TemplateProvider";
import {conditionFacade} from "../../CoreCondition";
import {TModal} from "./TModal";
import "./base.css";

export const Modal:FunctionComponent<TModal> = observer((props) => {
    return conditionFacade(props) ?
        <dialog data-role="modal" open={store.modal.method.status()}>
            <TemplateProvider source={store.modal.method.templateIdentifier()}/>
        </dialog>
        : null
});

export default Modal;
