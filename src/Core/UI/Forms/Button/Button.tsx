import React, {FunctionComponent, useEffect, useState} from 'react';
import {observer} from "mobx-react-lite";
import {conditionFacade} from "../../../CoreCondition";
import {CoreAction, CorePropCondition} from "../../../CoreAction";
import {CoreStyle} from "../../../CoreStyle";
import {TButton} from "./TButton";
import "./base.css";

export const Button:FunctionComponent<TButton> = observer((props) => {
    const disableStatus = CorePropCondition.getCondition(props.template,':root > Disable Condition');

    const [disable, setDisable] = useState<boolean|undefined>(disableStatus);

    useEffect(() => {
        setDisable(disableStatus)
    }, [disableStatus]);

    const handleClick = () => {
        if(!disable){
            CoreAction.implement(props.template,':root > Trigger[TriggerName="OnClick"] > Command');
        }
        //CoreAction.implement(props.template,":root > Command")
    }

    return conditionFacade(props) ?
        <button data-role="button"
                type="button"
                className={props.style}
                style={CoreStyle.getStyle("Button",props.template,':root > Button\\.Style')}
                disabled={disable}
                onClick={ handleClick }
                children={props.children}/>
        : null

});

export default Button;