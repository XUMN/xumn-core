export type TButton = {
    style?: string,
    command:string;
    templateIdentifier?:string;
    template:string;
    disabled?:string;
    pageNumber?:string;
}