export type TAutoComplete = {
    style?: string,
    open: string;
    conditionType?: string;
    conditionValue?: string;
    template:string;
}