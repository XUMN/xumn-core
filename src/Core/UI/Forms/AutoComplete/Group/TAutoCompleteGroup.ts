import {TOutsideClickMixinProps} from "../../../Observer/OutsideClick/TOutsideClickMixinProps";

export type TAutoCompleteGroup = {
    style?: string;
    value: string;
    conditionType?: string;
    conditionValue?: string;
    template:string;
    mixinProps: TOutsideClickMixinProps;
}