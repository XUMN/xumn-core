import React, {FunctionComponent, useState, useEffect} from "react";
import {observer} from "mobx-react-lite";
import {CoreStyle} from "../../../../CoreStyle";
import {CoreBinding} from "../../../../CoreBinding";
import {TAutoCompleteGroup} from "./TAutoCompleteGroup";

export const AutoCompleteGroup: FunctionComponent<TAutoCompleteGroup> = observer((props) => {
        const {value,mixinProps} = props;
        const {currentStatus} = mixinProps;

        const bindingValue = new CoreBinding().obtainValue(value).bindingValue;

        const condition = Array.isArray(bindingValue) ? bindingValue.length !== 0 : false;

        const [open, setOpen] = useState<boolean>(condition);

        useEffect(() => {
            currentStatus ? setOpen(condition) : setOpen(false)
        }, [currentStatus,condition]);

        return open ?
            <menu data-role="autocompletegroup"
                  role="group"
                  aria-hidden={!open}
                  className={props.style}
                  style={CoreStyle.getStyle("AutoCompleteGroup",props.template,':root > Style')}
                  children={props.children}/>
            :
            null
    }
);

export default AutoCompleteGroup;