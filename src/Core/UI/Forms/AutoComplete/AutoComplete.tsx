import React, {FunctionComponent, useRef, useState} from "react";
import {observer} from "mobx-react-lite";
import {CoreAction} from "../../../CoreAction";
import {CoreStyle} from "../../../CoreStyle";
import OutsideClickObserver from "../../Observer/OutsideClick/OutsideClickObserver";
import {TAutoComplete} from "./TAutoComplete";
import {TOutsideClickMixinProps} from "../../Observer/OutsideClick/TOutsideClickMixinProps";
import "./base.css";

export const AutoComplete: FunctionComponent<TAutoComplete> = observer((props) => {
        const [open, setOpen] = useState<boolean>(false);
        const ref = useRef(null)

        const mixinProps:TOutsideClickMixinProps = {
            currentStatus: open,
            reference: ref,
            setStatus: (open:boolean) => setOpen(open),
        }

        const handleClick = () => {
            setOpen(true);
            CoreAction.implement(props.template,':root > AutoComplete[TriggerName="OnClick"] > Command' );
        }

        return <fieldset data-role="autocomplete"
                         ref={ref}
                         aria-expanded={open}
                         className={props.style}
                         style={CoreStyle.getStyle("AutoComplete",props.template,':root > Style')}
                         onClick={ handleClick }>
            <OutsideClickObserver mixinProps={mixinProps} children={props.children}/></fieldset>
    }
);

export default AutoComplete;