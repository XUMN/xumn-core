import React, {ChangeEvent, FunctionComponent} from 'react';
import {observer} from "mobx-react-lite";
import {conditionFacade} from "../../../CoreCondition";
import {CoreBinding} from "../../../CoreBinding";
import {CoreAction} from "../../../CoreAction";

export type TDataList = {
    style?: string,
    id: string;
    conditionType?: string;
    conditionValue?: string;
    template: string;
}

export const DataList: FunctionComponent<TDataList> = observer((props) => {

    const handleChange = (e: ChangeEvent<HTMLDataListElement>) => {
console.log( "----" )
    }

    return conditionFacade(props) ?
        <datalist id={props.id}
                  onChange={(e: ChangeEvent<HTMLDataListElement>)=>handleChange(e)}
                  data-style={props.style}
                  children={props.children}/>
        : null
});

export default DataList;