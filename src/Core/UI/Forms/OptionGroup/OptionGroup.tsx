import React, {FunctionComponent} from 'react';
import {observer} from "mobx-react-lite";
import {conditionFacade} from "../../../CoreCondition";
import {CoreBinding} from "../../../CoreBinding";
import {TOptionGroup} from "./TOptionGroup";

export const OptionGroup: FunctionComponent<TOptionGroup> = observer((props) => {
        return(
            conditionFacade(props) ?
                <optgroup disabled={!!props.disabled}
                          label={new CoreBinding().outputValue(props.value).bindingValue}
                          children={props.children}/>
                : null
        )
    }
);

export default OptionGroup;