export type TOptionGroup = {
    value: string;
    conditionName?: string;
    conditionValue?: string;
    disabled?: string;
}