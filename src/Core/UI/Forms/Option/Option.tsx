import React, {FunctionComponent} from 'react';
import {observer} from "mobx-react-lite";
import {conditionFacade} from "../../../CoreCondition";
import {CoreBinding} from "../../../CoreBinding";
import {TOption} from "./TOption";


export const Option: FunctionComponent<TOption> = observer((props) => {
    return (
        conditionFacade(props) ?
            <option hidden={!!props.hidden}
                    disabled={!!props.disabled}
                    value={new CoreBinding().outputValue(props.value).bindingValue}
                    label={props?.label ? new CoreBinding().outputValue(props.label).bindingValue : undefined}/>
            : null
    )
});

export default Option;