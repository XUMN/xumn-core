export type TOption = {
    value: string;
    label?: string;
    conditionName?: string;
    conditionValue?: string;
    hidden?: string;
    disabled?: string;
    template: string;
}