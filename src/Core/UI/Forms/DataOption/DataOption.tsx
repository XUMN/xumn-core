import React, {FunctionComponent} from 'react';
import {observer} from "mobx-react-lite";
import {conditionFacade} from "../../../CoreCondition";
import {CoreBinding} from "../../../CoreBinding";
import {TDataOption} from "./TDataOption";


export const DataOption: FunctionComponent<TDataOption> = observer((props) => {
    return (
        conditionFacade(props) ?
            <option hidden={!!props.hidden}
                    disabled={!!props.disabled}
                    value={props.value}
                    label={props?.label ? new CoreBinding().outputValue(props.label).bindingValue : undefined}/>
            : null
    )
});

export default DataOption;