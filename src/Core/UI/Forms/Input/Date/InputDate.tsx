import React, {FormEvent, FunctionComponent} from "react";
import {observer} from "mobx-react-lite";
import {conditionFacade} from "../../../../CoreCondition";
import {CoreBinding} from "../../../../CoreBinding";
import {CoreAction, CorePropCondition} from "../../../../CoreAction";
import {CoreStyle} from "../../../../CoreStyle";
import "./base.css";

export type TInputDate = {
    style?: string,
    value: string;
    required?: "true";
    template: string;
    defaultValue?: string;
}

export const InputDate:FunctionComponent<TInputDate> = observer((props) => {

    const handleChange = (e: FormEvent<HTMLInputElement>) => {
        new CoreBinding().recordValue(props.value, e.currentTarget.value)
        CoreAction.implement(props.template,':root > Trigger[TriggerName="OnChange"] > Command');
    }

    return conditionFacade(props) ?
        <input data-role="input-date"
               type="date"
               className={props.style}
               style={CoreStyle.getStyle("InputDate",props.template,':root > InputDate\\.Style')}
               disabled={CorePropCondition.getCondition(props.template,':root > Disable Condition')}
               required={!!props.required}
               value={new CoreBinding().outputValue(props.value).bindingValue}
               onChange={(e: FormEvent<HTMLInputElement>) => handleChange(e)}
        />
        : null
});

export default InputDate;