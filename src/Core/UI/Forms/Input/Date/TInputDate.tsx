export type TInputDate = {
    style?: string,
    placeholder?: string;
    srcInput: string;
    required?: "true";
    value: string

}