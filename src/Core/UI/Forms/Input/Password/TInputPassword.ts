export type TInputPassword = {
    value: string;
    source: string;
    style?: string;
    width?: string;
    height?: string;
    autoComplete?: string;
    placeholder?: string;
    required?: "true";
    maxLength?: string | undefined;
    pattern?: string | undefined;
    template: string;
    alt?: string;
}