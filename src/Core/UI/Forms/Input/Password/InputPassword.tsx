import React, {FormEvent, FunctionComponent, useState} from 'react';
import {observer} from "mobx-react-lite";
import {CoreBinding} from "../../../../CoreBinding";
import {CoreStyle} from "../../../../CoreStyle";
import {TInputPassword} from "./TInputPassword";
import "./base.css";

export const InputPassword:FunctionComponent<TInputPassword> = observer((props) => {
    const [type, setState] = useState<"password" |"text">("password");

    return (
        <fieldset data-role="password-fieldset">
            <input data-role="input-password"
                   type={type}
                   className={props.style}
                   autoComplete={props.autoComplete}
                   placeholder={props.placeholder}
                   required={!!props.required}
                   maxLength={props.maxLength ? Number(props.maxLength) : undefined}
                   pattern={props.pattern}
                   value={new CoreBinding().outputValue(props.value).bindingValue}
                   style={CoreStyle.getStyle("InputPassword",props.template,':root > InputPassword\\.Style')}
                   onChange={(e: FormEvent<HTMLInputElement>) => new CoreBinding().recordValue(props.value, e.currentTarget.value)}/>
            <input data-role="input-image-password"
                   type="image"
                   width={props.width}
                   height={props.height}
                   src={props.source}
                   alt={props.alt}
                   style={CoreStyle.getStyle("InputImage",props.template,':root > InputImage\\.Style')}
                   onClick={()=>(setState(type === "password" ? "text" : "password"))}/>
        </fieldset>
    )
});


export default InputPassword;