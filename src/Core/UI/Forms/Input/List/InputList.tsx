import React, {ChangeEvent, FunctionComponent, useEffect, useState} from 'react';
import {observer} from "mobx-react-lite";
import {CoreBinding} from "../../../../CoreBinding";
import {conditionFacade} from "../../../../CoreCondition";
import {CoreAction} from "../../../../CoreAction";

export type TInputList = {
    id: string;
    list: string;
    name: string;
    style?: string,
    placeholder?: string;
    required?: "true";
    value: string;
    maxLength?: string | undefined;
    pattern?: string | undefined;
    template: string;
    autoComplete?: string;

    bindingLoop?: string;
    bindingV?: string;

}

export const InputList:FunctionComponent<TInputList> = observer((props) => {
    const [value, setValue] = useState("");

    const bindingValue = new CoreBinding().outputValue(props.value).bindingValue;

    useEffect(() => {
        setValue(bindingValue)
    }, [ bindingValue ]);

    CoreAction.implement(props.template,':root > Trigger[TriggerName="OnLaunch"] > Command');


    const handleChange = (e: ChangeEvent<HTMLInputElement>) => {


        new CoreBinding().recordValue(props.value, e.currentTarget.value);
        CoreAction.implement(props.template,':root > Trigger[TriggerName="OnChange"] > Command');




    }

    return conditionFacade(props) ?
        <input id={props.id}
               type="text"
               list={props.list}
               name={props.name}
               data-style={props.style}
               placeholder={props.placeholder}
               autoComplete={props.autoComplete}
               required={!!props.required}
               maxLength={props.maxLength ? Number(props.maxLength) : undefined}
               pattern={props.pattern}
               value={value}
               onChange={(e: ChangeEvent<HTMLInputElement>)=>handleChange(e)}
        />
        : null
});

export default InputList;