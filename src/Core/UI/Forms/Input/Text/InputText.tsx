import React, {FunctionComponent,ChangeEvent} from 'react';
import {observer} from "mobx-react-lite";
import {CoreBinding} from "../../../../CoreBinding";
import {CoreAction} from "../../../../CoreAction";
import {conditionFacade} from "../../../../CoreCondition";
import {CoreStyle} from "../../../../CoreStyle";
import {TInputText} from "./TInputText";
import "./base.css";

export const InputText:FunctionComponent<TInputText> = observer((props) => {
    const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
        new CoreBinding().recordValue(props.value, e.currentTarget.value);
        CoreAction.implement(props.template,':root > Trigger[TriggerName="OnChange"] > Command');
    }

    return conditionFacade(props) ?
        <input data-role="input-text"
               type="text"
               className={props.style}
               style={CoreStyle.getStyle("InputText",props.template,':root > InputText\\.Style')}
               autoComplete={props.autoComplete}
               placeholder={props.placeholder}
               required={!!props.required}
               maxLength={props.maxLength ? Number(props.maxLength) : undefined}
               pattern={props.pattern}
               value={new CoreBinding().outputValue(props.value).bindingValue}
               onChange={(e: ChangeEvent<HTMLInputElement>) => handleChange(e)}
        />
        : null
});

export default InputText;