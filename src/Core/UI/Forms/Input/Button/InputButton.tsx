import React, {FunctionComponent, useEffect, useState} from 'react';
import {observer} from "mobx-react-lite";
import {CoreAction, CorePropCondition} from "../../../../CoreAction";
import {TInputButton} from "./TInputButton";
import {conditionFacade} from "../../../../CoreCondition";
import {CoreBinding} from "../../../../CoreBinding";
import {CoreStyle} from "../../../../CoreStyle";
import "./base.css";

export const InputButton:FunctionComponent<TInputButton> = observer((props) => {
    const disableStatus = CorePropCondition.getCondition(props.template,':root > Disable Condition');
    const [disable, setDisable] = useState<boolean|undefined>(disableStatus);

    useEffect(() => {
        setDisable(disableStatus)
    }, [disableStatus]);

    const handleClick = () => {
        if(!disable){
            CoreAction.implement(props.template,':root > Trigger[TriggerName="OnClick"] > Command');
        }
    }

    return conditionFacade(props) ?
        <input data-role="input-button"
               type="button"
               className={props.style}
               style={CoreStyle.getStyle("InputButton",props.template,":root > InputButton\\.Style")}
               disabled={disable}
               data-style={props.style}
               onClick={ handleClick }
               value={new CoreBinding().outputValue(props.value).bindingValue}/>

        : null
});

export default InputButton;