export type TInputButton = {
    value: string,
    style?: string,
    className?: string,
    disabled?: string;
    template: string;
}