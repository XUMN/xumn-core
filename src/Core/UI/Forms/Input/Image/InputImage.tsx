import React, {FunctionComponent,useEffect,useState,useRef} from 'react';
import {observer} from "mobx-react-lite";
import {CoreAction, CorePropCondition} from "../../../../CoreAction";
import {TInputImage} from "./TInputImage";
import {conditionFacade} from "../../../../CoreCondition";
import {CoreStyle} from "../../../../CoreStyle";
import "./base.css";

export const InputImage:FunctionComponent<TInputImage> = observer((props) => {
    const disableStatus = CorePropCondition.getCondition(props.template,':root > Disable Condition');
    const [disable, setDisable] = useState<boolean|undefined>(disableStatus);

    const ref = useRef(null);

    useEffect(() => {
        setDisable(disableStatus)
    }, [disableStatus]);

    const handleClick = () => {
        if(!disable){
            CoreAction.implement(props.template,':root > Trigger[TriggerName="OnClick"] > Command');
        }
    }

    return conditionFacade(props) ?
        <input data-role="input-image"
               ref={ref}
               type="image"
               alt={props.alt}
               width={props.width}
               height={props.height}
               disabled={disable}
               className={props.style}
               style={CoreStyle.getStyle( "InputImage",props.template,":root > InputImage\\.Style")}
               onClick={handleClick}
               src={props.source}/>
        : null

});


export default InputImage;