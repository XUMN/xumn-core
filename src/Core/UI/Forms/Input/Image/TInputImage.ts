export type TInputImage = {
    source: string;
    width?: string;
    height?: string;
    style?: string;
    disabled?: string;
    alt?: string;
    template: string;
}