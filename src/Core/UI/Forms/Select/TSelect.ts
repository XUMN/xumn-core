export type TSelect = {
    style?: string,
    value: string;
    defaultValue: string;
    conditionName?: string;
    conditionValue?: string;
    converter?: string;
    template: string;
    id?: string;
    size?: string;
    hidden?: string;
    multiple?: string;

    label?: string;
}