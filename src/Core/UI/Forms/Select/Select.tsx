import React, {ChangeEvent, FunctionComponent, useEffect, useState} from 'react';
import {observer} from "mobx-react-lite";
import {conditionFacade} from "../../../CoreCondition";
import {CoreBinding} from "../../../CoreBinding";
import {TSelect} from "./TSelect";
import {CoreAction} from "../../../CoreAction";

export const Select: FunctionComponent<TSelect> = observer((props) =>{
        const [value, setValue] = useState<string>("")
        const bindingValue = new CoreBinding().outputValue(props.value).bindingValue;

        useEffect(() => {
            setValue(bindingValue !== "" ? bindingValue : props.defaultValue)
        }, [ props,bindingValue ]);

        const handleChange = (e: ChangeEvent<HTMLSelectElement>) => {
            new CoreBinding().recordValue(props.value,e.target.value);
            CoreAction.implement(props.template,':root > Trigger[TriggerName="OnChange"] > Command');
        }

        return (
            conditionFacade(props) ?
                <select id={props.id}
                        data-style={props.style}
                        hidden={!!props.hidden}
                        value={value}
                        size={props.size ? Number(props.size) : undefined}
                        multiple={!!props.multiple}
                        onChange={(e: ChangeEvent<HTMLSelectElement>)=>handleChange(e)}
                        children={props.children}/>
                : null
        )
    }
);

export default Select;