import React, {FunctionComponent, useEffect, useState} from 'react';
import {TAutoRun} from "./TAutoRun"
import {observer} from "mobx-react-lite";
import {CoreAction} from "../../CoreAction";
import {CoreBinding} from "../../CoreBinding";

export const Auto:FunctionComponent<TAutoRun> = observer((props) => {
    const [mount, setMount] = useState(true);

    const binding = {bindingPath: "location.href", bindingSource: "Routing",}
    const trigger = new CoreBinding().sourceValue(binding.bindingSource).method.obtain( binding );

    if(mount){
        CoreAction.implement(props.template,':root > Command');
    }

    useEffect(() => {
        setMount(true);
        return setMount(false);
    },[trigger]);


    return <>{props.children}</>
});

export default Auto;