import React, {FunctionComponent, useEffect, useState} from 'react';
import {TAutoRun} from "./TAutoRun"
import {obtainCoreCommand} from "../../CoreCommand";
import {observer} from "mobx-react-lite";

export const AutoRun:FunctionComponent<TAutoRun> = observer((props) => {
    const [state, setState] = useState(false);
    const [mount, setMount] = useState(true);

    useEffect(() => {
        if(mount){
            setState(false);
            const command = obtainCoreCommand(props.commandName);
            if(command){
                const perform = command.perform;
                perform(props, window.location);
                setState(true);
            }
        }
        setMount(false);
    },[props,mount]);
    return state ? <>{props.children}</> : null
});

export default AutoRun;