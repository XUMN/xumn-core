import React, {FunctionComponent, useEffect} from 'react';
import {TAutoRun} from "./TAutoRun"
import {observer} from "mobx-react-lite";
import {useLocation} from "react-router-dom";
import {CoreAction} from "../../CoreAction";

export const AutoRun:FunctionComponent<TAutoRun> = observer((props) => {
    const {pathname,search} = useLocation();
    const trigger = pathname + search;

    useEffect(() => {
        CoreAction.implement(props.template,':root > Command');
    },[props,trigger]);

    return <>{props.children}</>
});

export default AutoRun;