import React, {FunctionComponent, useEffect, useState} from 'react';
import {TAutoRunMulti} from "./TAutoRunMulti"
import {observer} from "mobx-react-lite";
import {CoreAction} from "../../CoreAction";

export const AutoRunMulti:FunctionComponent<TAutoRunMulti> = observer((props) => {
    const [state, setState] = useState(false);
    const [mount, setMount] = useState(true);
    useEffect(() => {
        if(mount){
            setState(false);
            CoreAction.implement(props.template,':root > Command');
            setState(true);
        }
        setMount(false);
    },[props,mount]);
    return state ? <>{props.children}</> : null
});

export default AutoRunMulti;