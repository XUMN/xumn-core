import React, {FunctionComponent, useRef} from "react";
import {observer, useLocalStore} from "mobx-react-lite";
import {CoreBinding} from "../../CoreBinding";
import ComboBoxGroup from "./ComboBoxGroup";
import {useClickOutside} from "../Use/UseClickOutside";
import {TComboBox} from "./TComboBox";
import "./data-combobox.css";

export const ComboBox: FunctionComponent<TComboBox> = observer((props) => {
    const state = useLocalStore(() => ({expanded: false}));
    const toggleExpanded = () => state.expanded = false;
    const elementReference = useRef(null);
    useClickOutside({ref:elementReference,handler:toggleExpanded});
    return (
        <div data-component={"ComboBox"} aria-expanded={state.expanded} ref={elementReference}>
            <div role="presentation">
                <output data-component="Text"
                        data-style={props.style}
                        onClick={()=>state.expanded ? state.expanded = false : state.expanded = true}
                        children={
                            new CoreBinding().outputValue(props.value).bindingValue ?
                                new CoreBinding().outputValue(props.value).bindingValue
                                :
                                props.placeholder
                        }/>
                <div role="status">
                    <svg role="img" width="16" height="12" viewBox="0 0 16 12">
                        <g id="point" strokeWidth="1.28" strokeLinecap="round">
                            <line x1="4" y1="4" x2="8" y2="8"/>
                            <line x1="8" y1="8" x2="12" y2="4"/>
                        </g>
                    </svg>
                </div>
            </div>
            <ComboBoxGroup setExpanded={toggleExpanded} children={props.children}/>
        </div>
    )
});

export default ComboBox;