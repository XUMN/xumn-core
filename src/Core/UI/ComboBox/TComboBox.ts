export type TComboBox = {
    style?: string,
    placeholder: string;
    into:string;
    query:string;
    filter:string;
    value:string;
    providerType:string;
    template:string;
}