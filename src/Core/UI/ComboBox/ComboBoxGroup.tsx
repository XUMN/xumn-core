import React, {FunctionComponent} from "react";
import "./data-combobox.css";

type TAutoCompleteGroup = {
    setExpanded:Function;
}

export const ComboBoxGroup: FunctionComponent<TAutoCompleteGroup> = (props) =>
    <div role="group"
         data-component={"ComboBoxGroup"}
         onClick={()=>props.setExpanded(false)}
         children={props.children}/>;

export default ComboBoxGroup;