import React, {FunctionComponent, Children, isValidElement, cloneElement, useEffect, Fragment} from "react";
import {TOutsideClickObserver} from "./TOutsideClickObserver";

export const OutsideClickObserver: FunctionComponent<TOutsideClickObserver> = (props) => {
    const {mixinProps} = props;
    const {setStatus,reference} = mixinProps;

    useEffect(() => {
        const listener = (event: TouchEvent | MouseEvent) => {
            if (reference.current){
                if (reference.current.contains(event.target)) {
                    return;
                } else {
                    setStatus(false);
                }
            } else {
                return
            }
        }
        document.addEventListener("mousedown", listener);
        document.addEventListener("touchstart", listener);
        return () => {
            document.removeEventListener("mousedown", listener);
            document.removeEventListener("touchstart", listener);
        };
    }, [setStatus,reference]);

    const childrenWidthMixin = Children.map(props.children, item => {
        return isValidElement(item) ? cloneElement(item, {mixinProps}) : item;
    });

    return <Fragment children={childrenWidthMixin}/>
};

export default OutsideClickObserver;