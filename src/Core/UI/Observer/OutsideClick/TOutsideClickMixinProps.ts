export type TOutsideClickMixinProps = {
    currentStatus: boolean;
    reference: any;
    setStatus: Function;
}