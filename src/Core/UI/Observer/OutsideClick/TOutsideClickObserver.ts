import {TOutsideClickMixinProps} from "./TOutsideClickMixinProps";

export type TOutsideClickObserver = {
    mixinProps: TOutsideClickMixinProps;
}