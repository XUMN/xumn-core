import React, {FunctionComponent} from 'react';
import {observer} from "mobx-react-lite";
import {CoreBinding} from "../../../CoreBinding";
import {conditionFacade} from "../../../CoreCondition";
import {TTextTime} from "./TTextTime";
import {CoreStyle} from "../../../CoreStyle";

export const TextTime: FunctionComponent<TTextTime> = observer((props) => {
    let value = String();
    const datetime = new CoreBinding().outputValue( props.value ).bindingValue;
    const options = {
        "year": "numeric",
        "month": "numeric",
        "day": "numeric"
    } as { [key:string]:string };
    if (props.time){
        options.hour = "2-digit";
        options.minute = "2-digit";
    }

    if(datetime){
        if (!isNaN(Date.parse(datetime))) {
            value = new Date(datetime).toLocaleString(props.locale, options);
        }
    }

    return conditionFacade(props) ?
        <time dateTime={datetime}
              className={props.style}
              style={CoreStyle.getStyle("Span",props.template,':root > Style')}
              children={value}/>
        : null

});

export default TextTime;