export type TTextTime = {
    style?: string,
    value: string;
    locale: string;
    time?: "true";
    conditionType?: string;
    conditionValue?: string;
    template: string;
}