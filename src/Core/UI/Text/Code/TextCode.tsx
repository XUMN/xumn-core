import React, {FunctionComponent,Fragment} from 'react';
import {observer} from "mobx-react-lite";
import {conditionFacade} from "../../../CoreCondition";
import {TTextCode} from "./TTextCode";

export const TextCode: FunctionComponent<TTextCode> = observer((props) => {
    return conditionFacade(props) ? <Fragment children={props.value}/> : null
});

export default TextCode;