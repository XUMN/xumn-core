export type TTextCode = {
    value: any;
    conditionType?: string;
    conditionValue?: string;
    template: string;
}