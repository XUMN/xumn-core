export type TTextSpan = {
    style?: string,
    value: any;
    conditionType?: string;
    conditionValue?: string;
    template: string;
}