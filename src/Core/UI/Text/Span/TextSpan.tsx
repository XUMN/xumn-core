import React, {FunctionComponent} from 'react';
import {observer} from "mobx-react-lite";
import {CoreBinding} from "../../../CoreBinding";
import {conditionFacade} from "../../../CoreCondition";
import {TTextSpan} from "./TTextSpan";
import {CoreStyle} from "../../../CoreStyle";
import "./base.css";

export const TextSpan: FunctionComponent<TTextSpan> = observer((props) => {
    return conditionFacade(props) ?
        <span data-role="textspan"
              className={props.style}
              style={CoreStyle.getStyle("Span",props.template,':root > Span\\.Style')}
              children={new CoreBinding().outputValue(props.value).bindingValue}/>
        : null
});

export default TextSpan;