import React, {FunctionComponent} from 'react';
import {observer} from "mobx-react-lite";
import {CoreBinding} from "../../CoreBinding";
import {CoreStyle} from "../../CoreStyle";
import {conditionFacade} from "../../CoreCondition";
import {TText} from "./TText";
import "./base.css";

export const Text: FunctionComponent<TText> = observer((props) => {
    return conditionFacade(props) ?
        <output data-role="text"
                role="textbox"
                className={props.style}
                style={CoreStyle.getStyle("Text",props.template,':root > Text\\.Style')}
                children={props.value ? new CoreBinding().outputValue(props.value).bindingValue : props.children}/>
        : null
});

export default Text;