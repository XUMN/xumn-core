export type TText = {
    style?: string,
    value: any;
    conditionType?: string;
    conditionValue?: string;
    template: string;
}