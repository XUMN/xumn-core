import React, {FunctionComponent} from "react";
import {observer} from "mobx-react-lite";
import {store} from "../../CoreStore";
import {CoreStyle} from "../../CoreStyle";

export type TProcess = {
    style?: string,
    template: string;
}

export const Process:FunctionComponent<TProcess> = observer((props) => {
    return (
        <dialog open={store.process.method.status()}
                data-style={props.style}
                style={CoreStyle.getStyle("Dialog",props.template,':root > Dialog\\.Property')}
                children={props.children}/>
    )
});

export default Process;