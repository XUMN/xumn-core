import {useEffect} from "react";

interface IUseClickOutside {
    ref: any;
    handler: (data?: any) => void;
}

export const useClickOutside = ({ ref, handler }: IUseClickOutside) => {
    useEffect(() => {
        const listener = (event: any) => {
            if (ref.current){
                if (ref.current.contains(event.target)) {
                    return;
                } else {
                    handler(event);
                }
            } else {
                return
            }
        };
        document.addEventListener("mousedown", listener);
        document.addEventListener("touchstart", listener);
        return () => {
            document.removeEventListener("mousedown", listener);
            document.removeEventListener("touchstart", listener);
        }
    }, [ref, handler]);
};


