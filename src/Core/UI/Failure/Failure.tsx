import React, {FunctionComponent} from 'react';
import {observer} from "mobx-react-lite";
import {store} from "../../CoreStore";

export const Failure:FunctionComponent = observer((props) => {
    return (
        store.failure.method.status() ? <>{props.children}</> : null
    )
});

export default Failure;