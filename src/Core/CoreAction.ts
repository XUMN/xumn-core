import {getChildrenAttribute, parseXML} from "./Util/NodeXML";
import {obtainCoreCommand} from "./CoreCommand";
import {conditionFacade} from "./CoreCondition";

export class CoreAction {
    static implement = (template:string, selector:string) => {
        const element = parseXML(template).querySelectorAll( selector );



        getChildrenAttribute(template,selector,"lowercase").map((command,i)=>{
            if(obtainCoreCommand(command.commandName)){
                command.template = new XMLSerializer().serializeToString(element[i]);
                return obtainCoreCommand(command.commandName).perform(command);
            } else {
                return console.warn("Unknown command Name:", command.commandName);
            }
        });
    };
}

export class CorePropCondition {
    static getCondition(template:string, selector:string){
        const condition = {status: undefined } as { [key:string]:undefined | boolean };
        for(const item of getChildrenAttribute(template,selector,"lowercase")){
            condition.status = conditionFacade(item) ? true : condition.status;
        }
        return condition.status;
    }
}


/*
export class CoreAction {
    static implement = (template:string, selector:string) => {
        const element = new DOMParser().parseFromString(template, "text/xml").querySelectorAll( selector );
        for(const item of element ) {
            const command = getElementAttr(item,"lowercase");
            if (obtainCoreCommand(command.commandName)) {
                command.template = new XMLSerializer().serializeToString(item);
                return obtainCoreCommand(command.commandName).perform(command);
            }
        }
    };
}

export class CorePropCondition {
    static getCondition(template:string, selector:string){
        const condition = {status: undefined } as { [key:string]:undefined | boolean };
        for(const item of getChildrenAttribute(template,selector,"lowercase")){
            condition.status = conditionFacade(item) ? true : condition.status;
        }
        return condition.status;
    }
}
 */