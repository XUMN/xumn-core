import {styleDictionary} from "./UI/StyleDefinition";

export class CoreStyle {
    static setLowercase = (value: string) => value.replace(/^./, (match) => match.toLowerCase());
    static getStyle = (key: string, template: string, selector: string) => {
        const style: { [key: string]: string } = {}
        const element = new DOMParser().parseFromString(template, "text/xml").documentElement.querySelector(selector);
        if (element) {
            const {attributes} = element;
            for (const attribute of attributes) {
                if(attribute.name.includes(".")){
                    const name = "--" + attribute.name.replace(".","");
                    style[name] = attribute.value;
                    continue;
                }
                if (styleDictionary[key]) {
                    if (styleDictionary[key][attribute.name]) {
                        style[CoreStyle.setLowercase(attribute.name)] = attribute.value;
                    } else {
                        console.log("Not correct attribute name in style:", element.tagName, attribute.name)
                    }
                } else {
                    console.log("Not correct style tag name:", element.tagName)
                }
            }
        }
        return style;
    }
}
    /*
    static getStyle = (template: string, selector: string,) => {
        const style: { [key: string]: string } = {}
        const root = new DOMParser().parseFromString(template, "text/xml");
        const name = root.documentElement.tagName;
        const collection = root.querySelectorAll(selector);
        for (const element of collection) {
            const styleName = element.getAttribute("StyleName");
            if (!styleName) {
                continue
            }
            const property = styleDictionary[name][styleName];
            if (!property) {
                console.log("Incorrect style name:", styleName)
            }
            if (!property) {
                continue
            }
            const value = element.getAttribute("Value");
            if (!value) {
                continue
            }
            style[CoreStyle.setLowercase(styleName)] = value;
        }
        return style;
    }



    static setMixin(template:string,target:{},style:{},type:string,selector:string){
        Object.assign(
            target,
            style,
            CoreStyle.getStyle(type,template,selector)
        );
    }
    static getMixin(template:string,type:string){
        const style = {
            regular: {},
            onHover: {},
            onClick: {},
            onFocus: {},
            disable: {},
        }
        style.regular = CoreStyle.getStyle(type,template,':root > Style');
        CoreStyle.setMixin( template, style.onHover, style.regular, type,':root > Style > Style\\.OnHover');
        CoreStyle.setMixin( template, style.onClick, style.regular, type,':root > Style > Style\\.OnClick');
        CoreStyle.setMixin( template, style.onFocus, style.regular, type,':root > Style > Style\\.OnFocus');
        CoreStyle.setMixin( template, style.disable, style.regular, type,':root > Style > Style\\.Disable');
        return style;
    }
}
     */