import {TemplateAssembly} from "./TemplateAssembly";

export class Reference {
    static setIncludes (template:Document,reference:Element,insertion:Document) {
        const collection = reference.querySelectorAll('Reference Includes');
        for (const includes of collection) {
            const key = includes.getAttribute("Key");
            if (!key) {continue}
            const list = insertion.querySelectorAll(`Reference[KeyName="${key}"]`);
            for(const item of list){
                item.replaceWith(...includes.childNodes);
            }
        }
    }
    static setResource (template:Document,reference:Element,insertion:Document) {
        const resourceElement = reference.querySelectorAll('Reference\\.Resource Resource');
        for (const resource of resourceElement) {
            const key = resource.getAttribute("Key");
            if (!key) {continue}
            const keyName = resource.getAttribute("KeyName");
            if (!keyName) {continue}
            const fragment = template.querySelector(`Template\\.Resource Resource[Key="${keyName}"]`);
            if (!fragment) {continue}
            fragment.setAttribute("Key",key);
            const clone = fragment.cloneNode();
            insertion.querySelector(`Template\\.Resource Resource\\.Reference[Key="${key}"]`)?.replaceWith(clone);
        }
    }
    static setVariable (template:Document,reference:Element,insertion:Document) {
        const collection = reference.querySelectorAll('Reference Variable');
        for (const variable of collection) {
            const key = variable.getAttribute("Key");
            if (!key) {continue}
            const keyName = variable.getAttribute("KeyName");
            if(keyName){
                const target = template.querySelector(`Template\\.Variable Variable[Key="${keyName}"]`);
                if (!target) {continue}
                const value = target.getAttribute("Value");
                if (!value) {continue}
                variable.setAttribute("Value",value);
            }
            insertion.querySelector(`Template\\.Variable Variable[Key="${key}"]`)?.replaceWith(variable);
        }
    }
    static implement(template:Document,reference:Element,resource:any) {
        const insertion = new DOMParser().parseFromString(resource, "text/xml");
        Reference.setIncludes(template, reference, insertion);
        Reference.setResource(template, reference, insertion);
        Reference.setVariable(template, reference, insertion);
        TemplateAssembly.installResource(insertion);
        TemplateAssembly.installFragment(insertion);
        TemplateAssembly.installVariable(insertion);
        reference.replaceWith(...insertion.documentElement.childNodes);
    }
}