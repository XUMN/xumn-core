export class Fragment {
    static setIncludes (template:Document,reference:Element,insertion:Document) {
        const collection = reference.querySelectorAll('Reference Includes');
        for (const includes of collection) {
            const key = includes.getAttribute("Key");
            if (!key) {continue}
            const list = insertion.querySelectorAll(`Reference[KeyName="${key}"]`);
            for(const item of list){
                item.replaceWith(...includes.childNodes);
            }
        }
    }
    static setResource (template:Document,reference:Element,insertion:Document) {
        const resourceElement = reference.querySelectorAll('Reference\\.Resource Resource');
        for (const resource of resourceElement) {
            const key = resource.getAttribute("Key");
            if (!key) {continue}
            const keyName = resource.getAttribute("KeyName");
            if (!keyName) {continue}
            const fragment = template.querySelector(`Template\\.Resource Resource[Key="${keyName}"]`);
            if (!fragment) {continue}
            fragment.setAttribute("Key",key);
            const clone = fragment.cloneNode();
            insertion.querySelector(`Template\\.Resource Resource\\.Reference[Key="${key}"]`)?.replaceWith(clone);
        }
    }
    static setVariable (reference:Element,insertion:Document) {
        const collection = reference.querySelectorAll('Reference Variable');
        for (const variable of collection) {
            const key = variable.getAttribute("Key");
            if (!key) {continue}
            const value = variable.getAttribute("Value");
            if (!value) {continue}
            const list = insertion.querySelectorAll('[Reference="Variable"]');
            for(const item of list){
                for(const attribute of item.attributes) {
                    if(attribute.value === `{Variable KeyName=${key}}` ){
                        item.setAttribute(attribute.name,value)

                    }
                }
            }
        }
    }
    static implement(reference:Element,fragment:Element) {
        const insertion = new DOMParser().parseFromString(new XMLSerializer().serializeToString(fragment), "text/xml");
        Fragment.setVariable(reference, insertion);
        reference.replaceWith(...insertion.documentElement.childNodes);
    }
}