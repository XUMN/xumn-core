import {store} from "../../CoreStore";
import {Reference} from "./Reference";
import {Fragment} from "./Fragment";

export class TemplateAssembly {
    static installReference(template:Document){
        for (const reference of template.querySelectorAll('Reference[ReferenceType="Resource"]')) {
            const source = reference.getAttribute("Source");
            if (!source) continue;
            const resource = store.template.method.obtain(source);
            if (!resource) continue;
            Reference.implement(template,reference,resource);
        }
    }
    static installFragment(template:Document){
        const collection = template.querySelectorAll('Reference[ReferenceType="Fragment"]');
        for (const reference of collection){
            const keyName = reference.getAttribute("KeyName");
            if (!keyName) continue;
            const fragment = template.querySelector(`Template\\.Fragment Fragment[Key="${keyName}"]`);
            if (!fragment) continue;
            Fragment.implement(reference,fragment)
        }
    }
    static installResource(template:Document){
        const collection = template.querySelectorAll('Reference[ReferenceType="Resource"]');
        for (const reference of collection){
            const keyName = reference.getAttribute("KeyName");
            if (!keyName) continue;
            const source = template.querySelector(`Template\\.Resource Resource[Key="${keyName}"]`)?.getAttribute("Source");
            if (!source) continue;
            const resource = store.template.method.obtain(source);
            if (!resource) continue;
            const insertion = new DOMParser().parseFromString(resource, "text/xml");
            reference.replaceWith(...insertion.documentElement.childNodes);
        }
    }
    static installVariable(template:Document){
        const collection = template.querySelectorAll('[Reference="Variable"]');
        for (const reference of collection) {
            for(const attribute of reference.attributes){
                if(!attribute.value.includes("Variable KeyName")) continue;
                const propertyName = attribute.name;
                let result: string = String();
                for(const item of Array.from(attribute.value.split(/[{}]/))) {
                    if(item){
                        result = item
                    }
                }
                const keyName = result.replace("Variable KeyName=","");
                const variable = template.querySelector(`Template\\.Variable Variable[Key="${keyName}"]`);
                if(!variable) continue;
                const propertyValue = variable.getAttribute("Value");
                if(!propertyValue) continue;
                reference.setAttribute(propertyName,propertyValue);
            }
        }
    }
}