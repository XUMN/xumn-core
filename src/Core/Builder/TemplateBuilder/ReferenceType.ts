export class SetReferenceType{

    // Установка тип Reference и Source: Reference будет заменена шаблоном из стора по id === Source
    static setResourceType(template:Document){
        const collection = template.querySelectorAll('Template\\.Resource Resource');
        for(const resource of collection){
            const key = resource.getAttribute("Key");
            if(!key) {continue}
            const source = resource.getAttribute("Source");
            if(!source) {continue}
            // Поск всех Reference на Template.Resource
            const list = template.querySelectorAll(`Reference[KeyName="${key}"]`);
            for(const item of list){
                item.setAttribute("ReferenceType","Resource");
                item.setAttribute("Source",source);
            }
        }
    }

    // Установка тип Fragment: Reference будет заменена содержимым Fragment из Template.Fragment Fragment
    static setFragmentType(template:Document){
        const collection = template.querySelectorAll('Template\\.Fragment Fragment');
        for(const fragment of collection){
            const key = fragment.getAttribute("Key");
            if(!key) {continue}
            // Поск всех Reference на Template.Fragment
            const list = template.querySelectorAll(`Reference[KeyName="${key}"]`);
            for(const item of list){
                item.setAttribute("ReferenceType","Fragment");
            }
        }
    }

    // Установка тип Includes: Reference будет заменена внешней вставкой Includes из Reference.Includes Includes
    static setIncludesType(template:Document){
        const collection = template.querySelectorAll('Template\\.Includes Includes');
        for(const fragment of collection){
            const key = fragment.getAttribute("Key");
            if(!key) {continue}
            // Поск всех Reference на Template.Includes
            const list = template.querySelectorAll(`Reference[KeyName="${key}"]`);
            for(const item of list){
                item.setAttribute("ReferenceType","Includes");
            }
        }
    }
}