import {SetReferenceType} from "./ReferenceType";
import {TemplateAssembly} from "./TemplateAssembly";

export function templateBuilder(template:Document) {
    SetReferenceType.setResourceType(template);
    SetReferenceType.setFragmentType(template);
    SetReferenceType.setIncludesType(template);
    TemplateAssembly.installFragment(template);
    TemplateAssembly.installReference(template);
    TemplateAssembly.installResource(template);
    TemplateAssembly.installVariable(template);
}