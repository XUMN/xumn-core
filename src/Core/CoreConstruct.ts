export class CoreConstruct {

    currentDateTime = (props:any) => props.bindingValue = new Date().toLocaleString( props.constructProperty, {year:'numeric', month:'numeric', day:'numeric', hour:'numeric', minute:'numeric', second:'numeric'});

    currentDate = (props:any) => {
        if(props.constructProperty === "YYYY-MM-DD"){
            props.bindingValue = new Date().toISOString().split("T").shift();
        } else {
            props.bindingValue = new Date().toLocaleString( props.constructProperty, {year:'numeric', month:'numeric', day:'numeric'});
        }
    }

    currentTime = (props:any) => props.bindingValue = new Date().toLocaleString( props.constructProperty, {hour:'numeric', minute:'numeric', second:'numeric'});

    newArray = (props:any) => props.bindingValue = [];

    newObject = (props:any) => props.bindingValue = {};

    newNull= (props:any) => props.bindingValue = null;

    public method = {
        "NewNull": (props:any) => this.newNull(props),
        "NewArray": (props:any) => this.newArray(props),
        "NewObject": (props:any) => this.newObject(props),
        "CurrentDateTime": (props:any) => this.currentDateTime(props),

        "CurrentDate": (props:any) => this.currentDate(props),
        "CurrentTime": (props:any) => this.currentTime(props),
    } as { [key:string]:Function };

    launch = (props:any) =>{
        if(props.bindingConstruct){
            if(this.method[props.bindingConstruct]){
                this.method[props.bindingConstruct](props)
            }
        }
    }
}